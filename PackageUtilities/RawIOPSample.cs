﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Injectsense.PackageUtilities
{
    public class RawIOPSample
    {
        public readonly int PressureValueLength = 12;
        public readonly int TemperatureValueLength = 8;
        public readonly int BatteryVoltageLength = 10;

        public UInt16 Pressure { get; set; }
        public UInt16 Temperature { get; set; }
        public UInt16 BatteryVoltage { get; set; }
    }
}

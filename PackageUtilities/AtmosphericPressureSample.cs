﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Injectsense.PackageUtilities
{
    public class AtmosphericPressureSample
    {
        public float Pressure { get; set; }
        public string LocalTimestampString { get; set; }  // TODO: Should this be UTC? And put all reference in display in clinician's view?
        public DateTime LocalTimestamp { get; set; }
    }
}

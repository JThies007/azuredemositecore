﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Injectsense.PackageUtilities
{
    public class AtmosphericPressureSection : Section
    {
        public string HardwareModel { get; set; }
        public string OSVersion { get; set; }
        public string AppName { get; set; }
        public string AppVersion { get; set; }

        public UInt16 NumberOfSamples { get; set; }
        public List<AtmosphericPressureSample> Samples { get; set; }

        public AtmosphericPressureSection()
        {
            this.Identifier = (UInt16)IdentifierConstants.AtmosphericPressureData;
            this.Version = 0x001;
            Samples = new List<AtmosphericPressureSample>();
        }
    }
}

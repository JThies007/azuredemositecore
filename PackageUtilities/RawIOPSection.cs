﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Injectsense.PackageUtilities
{
    public class RawIOPSection : Section
    {
        public readonly int UniqueSensorIdValueLength = 21;

        public UInt64 UniqueSensorId { get; set; }
        public string DataReadTimestamp { get; set; }
        public UInt16 NumberOfSamples { get; set; }
        public List<RawIOPSample> Samples { get; set; }
        public Byte IntervalPeriod { get; set; }
        public Byte LastIntervalTime { get; set; }
        public string DataEnableTimestamp { get; set; }

        public RawIOPSection()
        {
            this.Identifier = (UInt16)IdentifierConstants.RawIOPData;
            this.Version = 0x001;

            Samples = new List<RawIOPSample>();
        }
    }
}

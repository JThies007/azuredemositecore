﻿
using System;


namespace Injectsense.PackageUtilities
{
    public abstract class Section
    {
        public UInt16 Identifier;
        public UInt16 Version;
    }
}

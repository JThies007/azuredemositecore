﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Injectsense.PackageUtilities
{
    public enum IdentifierConstants
    {
        RawIOPData = 0xFF11,
        SensorReaderData = 0xFF21,
        NetworkData = 0xFF31,
        AtmosphericPressureData = 0xFF41
    }
}

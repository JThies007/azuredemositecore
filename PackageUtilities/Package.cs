﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Injectsense.PackageUtilities
{
    public class Package : IPackage
    {
        public const int HASHLength = 20;

        public UInt16 Identifier { get; set; }
        public UInt16 Version { get; set; }
        public string HardwareModel { get; set; }
        public string OSVersion { get; set; }
        public string AppName { get; set; }
        public string AppVersion { get; set; }

        // Time Of Interrogation
        public string TimeOfInterrogationUTC { get; set; }

        // Sensor Reader Information
        public string SensorReaderSerialNumber { get; set; }

        public string SensorReaderFirmwareVersion { get; set; }

        public byte NumberOfSections { get; set; }

        public List<Section> Sections;

        public byte[] Hash { get; set; }

        public Package()
        {
            this.Identifier = (UInt16)IdentifierConstants.NetworkData;
            this.Version = 0x001;

            this.Sections = new List<Section>();
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Injectsense.PackageUtilities
{
    public static class PackageManager
    {
        /// <summary>
        /// Gets the sensor ids from package.
        /// </summary>
        /// <param name="package">The package.</param>
        /// <returns>strings are empty if no sensor exists</returns>
        public static (string SensorId1, string SensorId2) GetSensorIdsFromPackage(Package package)
        {
            string sensorId1 = string.Empty;
            string sensorId2 = string.Empty;

            // Give the Raw IOP sections in the package,
            // find the sensor serial numbers.
            bool foundFirstId = false;
            foreach (Section section in package.Sections)
            {
                if (section.Identifier == (ushort)IdentifierConstants.RawIOPData)
                {
                    if (!foundFirstId)
                    {
                        sensorId1 = ((section as RawIOPSection).UniqueSensorId).ToString();
                        foundFirstId = true;
                    }
                    else
                    {
                        sensorId2 = ((section as RawIOPSection).UniqueSensorId).ToString();
                    }
                }
            }

            return (sensorId1, sensorId2);
        }

        // ******************************************************************
        // The following methods parse a byte array and populate a Package.
        // ******************************************************************

        /// <summary>
        /// Parses the byte array to package.
        /// </summary>
        /// <param name="byteArray">The byte array.</param>
        /// <returns>Package</returns>
        public static Package ParseByteArrayToPackage(byte[] byteArray)
        {
            Package package = new Package();
            int arrayIndex = 0;

            arrayIndex = ReadPackageHeader(package, byteArray, arrayIndex);

            arrayIndex = ReadSections(package, byteArray, arrayIndex);

            // TODO: Need to read and validate hash value.

            return (package);
        }

        /// <summary>
        /// Reads the sections.
        /// </summary>
        /// <param name="package">The package.</param>
        /// <param name="byteArray">The byte array.</param>
        /// <param name="arrayIndex">Index of the array.</param>
        /// <returns></returns>
        /// <exception cref="Exception"></exception>
        private static int ReadSections(Package package, byte[] byteArray, int arrayIndex)
        {
            int sectionCounter = 0;
            while (sectionCounter < package.NumberOfSections)
            {
                // Read the section identifer from the byteArray.
                ushort identifier = ReadWord(byteArray, arrayIndex);

                // Process the specific section based on identifer.
                switch (identifier)
                {
                    case (ushort)IdentifierConstants.RawIOPData:
                        arrayIndex = ReadRawIOPDataSection(package, byteArray, arrayIndex);
                        break;

                    case (ushort)IdentifierConstants.AtmosphericPressureData:
                        arrayIndex = ReadAtmospherPressureSection(package, byteArray, arrayIndex);
                        break;

                    default:
                        throw new Exception(string.Format("Invalid section identifier encountered: {0:X}", identifier));
                        // No break listed to satisfy compiler.
                }

                sectionCounter++;
            }

            return (arrayIndex);
        }

        /// <summary>
        /// Reads the raw iop data section.
        /// </summary>
        /// <param name="package">The package.</param>
        /// <param name="byteArray">The byte array.</param>
        /// <param name="arrayIndex">Index of the array.</param>
        /// <returns></returns>
        private static int ReadRawIOPDataSection(Package package, byte[] byteArray, int arrayIndex)
        {
            RawIOPSection rawIOPSection = new RawIOPSection();

            // Read the Identifier.
            rawIOPSection.Identifier = ReadWord(byteArray, arrayIndex);
            arrayIndex = arrayIndex + 2;

            // Read the Version
            rawIOPSection.Version = ReadWord(byteArray, arrayIndex);
            arrayIndex = arrayIndex + 2;

            // Read the unique id/device serial number (6 bytes)
            rawIOPSection.UniqueSensorId = ((ulong)(((ulong)byteArray[arrayIndex] << 40) | ((ulong)byteArray[arrayIndex + 1] << 32)
                | ((ulong)byteArray[arrayIndex + 2] << 24) | ((ulong)byteArray[arrayIndex + 3] << 16)
                | ((ulong)byteArray[arrayIndex + 4] << 8) | (ulong)byteArray[arrayIndex + 5]));
            arrayIndex = arrayIndex + 6;

            // Read the Data Read Timestamp
            (string resultstring, int arrayIndex) results = ReadString(byteArray, arrayIndex);
            rawIOPSection.DataReadTimestamp = results.resultstring;
            arrayIndex = results.arrayIndex;

            // Read the number of samples.
            rawIOPSection.NumberOfSamples = ReadWord(byteArray, arrayIndex);
            arrayIndex = arrayIndex + 2;

            // Read the samples.
            for (int i = 0; i < rawIOPSection.NumberOfSamples; i++)
            {
                RawIOPSample sample = new RawIOPSample();

                // Read Pressure
                sample.Pressure = ReadWord(byteArray, arrayIndex);
                arrayIndex = arrayIndex + 2;

                // Temperature
                sample.Temperature = ReadWord(byteArray, arrayIndex);
                arrayIndex = arrayIndex + 2;

                // Battery Voltage
                sample.BatteryVoltage = ReadWord(byteArray, arrayIndex);
                arrayIndex = arrayIndex + 2;
            }

            // Read the Interval Period
            rawIOPSection.IntervalPeriod = byteArray[arrayIndex++];

            // Read the Last Interval Time
            rawIOPSection.LastIntervalTime = byteArray[arrayIndex++];

            // Read Data Enable Timestamp
            results = ReadString(byteArray, arrayIndex);
            rawIOPSection.DataEnableTimestamp = results.resultstring;
            arrayIndex = results.arrayIndex;

            // Add the section to the package.
            package.Sections.Add(rawIOPSection);

            return (arrayIndex);
        }

        /// <summary>
        /// Reads the atmospher pressure section.
        /// </summary>
        /// <param name="package">The package.</param>
        /// <param name="byteArray">The byte array.</param>
        /// <param name="arrayIndex">Index of the array.</param>
        /// <returns></returns>
        private static int ReadAtmospherPressureSection(Package package, byte[] byteArray, int arrayIndex)
        {
            AtmosphericPressureSection atmPressureSection = new AtmosphericPressureSection();

            // Read the Identifier.
            atmPressureSection.Identifier = ReadWord(byteArray, arrayIndex);
            arrayIndex = arrayIndex + 2;

            // Read the Version
            atmPressureSection.Version = ReadWord(byteArray, arrayIndex);
            arrayIndex = arrayIndex + 2;

            // Read the Hardware Model
            (string resultstring, int arrayIndex) results = ReadString(byteArray, arrayIndex);
            atmPressureSection.HardwareModel = results.resultstring;
            arrayIndex = results.arrayIndex;

            // Read the OS Version
            results = ReadString(byteArray, arrayIndex);
            atmPressureSection.OSVersion = results.resultstring;
            arrayIndex = results.arrayIndex;

            // Read the App Name
            results = ReadString(byteArray, arrayIndex);
            atmPressureSection.AppName = results.resultstring;
            arrayIndex = results.arrayIndex;

            // Read the App Version
            results = ReadString(byteArray, arrayIndex);
            atmPressureSection.AppVersion = results.resultstring;
            arrayIndex = results.arrayIndex;

            // Read number of samples
            atmPressureSection.NumberOfSamples = ReadWord(byteArray, arrayIndex);
            arrayIndex = arrayIndex + 2;

            // Read the samples
            for (int i = 0; i < atmPressureSection.NumberOfSamples; i++)
            {
                AtmosphericPressureSample sample = new AtmosphericPressureSample();

                // Read Pressure
                sample.Pressure = ((ushort)(((ushort)byteArray[arrayIndex] << 24) | ((ushort)byteArray[arrayIndex + 1] << 16)
                                    | ((ushort)byteArray[arrayIndex + 2] << 8) | (ushort)byteArray[arrayIndex + 3]));
                arrayIndex = arrayIndex + 4;

                // Local Timestamp
                results = ReadString(byteArray, arrayIndex);
                sample.LocalTimestampString = results.resultstring;
                sample.LocalTimestamp = Convert.ToDateTime(results.resultstring);
                arrayIndex = results.arrayIndex;
            }

            package.Sections.Add(atmPressureSection);

            return (arrayIndex);
        }

        /// <summary>
        /// Reads the package header.
        /// </summary>
        /// <param name="package">The package.</param>
        /// <param name="byteArray">The byte array.</param>
        /// <param name="arrayIndex">Index of the array.</param>
        /// <returns></returns>
        public static int ReadPackageHeader(Package package, byte[] byteArray, int arrayIndex)
        {
            // Retrieve and validate data.

            // Package Identifier
            package.Identifier = ReadWord(byteArray, arrayIndex);
            arrayIndex = arrayIndex + 2;

            // Package Version
            package.Version = ReadWord(byteArray, arrayIndex);
            arrayIndex = arrayIndex + 2;

            // Mobile App - Hardware Model
            (string resultstring, int arrayIndex) results = ReadString(byteArray, arrayIndex);
            package.HardwareModel = results.resultstring;
            arrayIndex = results.arrayIndex;

            // Mobile App - OS Version
            results = ReadString(byteArray, arrayIndex);
            package.OSVersion = results.resultstring;
            arrayIndex = results.arrayIndex;

            // Mobile App - App Name
            results = ReadString(byteArray, arrayIndex);
            package.AppName = results.resultstring;
            arrayIndex = results.arrayIndex;

            // Mobile App - App Version
            results = ReadString(byteArray, arrayIndex);
            package.AppVersion = results.resultstring;
            arrayIndex = results.arrayIndex;

            // Mobile App - Sensor Reader Serial Number
            results = ReadString(byteArray, arrayIndex);
            package.SensorReaderSerialNumber = results.resultstring;
            arrayIndex = results.arrayIndex;

            // Mobile App - Sensor Reader Firmware Version
            results = ReadString(byteArray, arrayIndex);
            package.SensorReaderFirmwareVersion = results.resultstring;
            arrayIndex = results.arrayIndex;

            // Time of Interrogation UTC
            results = ReadString(byteArray, arrayIndex);
            package.TimeOfInterrogationUTC = results.resultstring;
            arrayIndex = results.arrayIndex;

            // Number of Sections
            package.NumberOfSections = byteArray[arrayIndex++];

            ValidatePackageHeader(package);

            return (arrayIndex);
        }

        /// <summary>
        /// Reads the word.
        /// </summary>
        /// <param name="byteArray">The byte array.</param>
        /// <param name="arrayIndex">Index of the array.</param>
        /// <returns></returns>
        public static ushort ReadWord(byte[] byteArray, int arrayIndex)
        {
            return ((ushort)(((ushort)byteArray[arrayIndex] << 8) | (ushort)byteArray[arrayIndex + 1]));
        }

        /// <summary>
        /// Validates the package header.
        /// </summary>
        /// <param name="package">The package.</param>
        public static void ValidatePackageHeader(Package package)
        {
            if (package.Identifier != (ushort)IdentifierConstants.NetworkData)
            {
                throw new Exception(string.Format("Invalid identifier in the header of the package file. Invalid value is: {0:X}", package.Identifier));
            }

            if (package.Version != 0x0001)
            {
                throw new Exception(string.Format("Invalid version in the hdeader of the package file. Invalid value is: {0:X}", package.Version));
            }
        }

        /// <summary>
        /// Reads the string.
        /// </summary>
        /// <param name="byteArray">The byte array.</param>
        /// <param name="arrayIndex">Index of the array.</param>
        /// <returns></returns>
        public static (string resultstring, int arrayIndex) ReadString(byte[] byteArray, int arrayIndex)
        {
            StringBuilder sb = new StringBuilder();

            int counter = 0;
            while ((byteArray[arrayIndex] != 0x00) && (counter < 100))
            {
                sb.Append((char)byteArray[arrayIndex++]);
                counter++;
            }

            return (sb.ToString(), ++arrayIndex);
        }

        // ******************************************************************
        // The following methods write the package to a file.
        // ******************************************************************

        /// <summary>
        /// Writes the package to file.
        /// </summary>
        /// <param name="package">The package.</param>
        /// <param name="outputFilename">The output filename.</param>
        public static void WritePackageToFile(Package package, string outputFilename)
        {
            // write the binary file section by section to to drive.
            using (System.IO.Stream stream = new FileStream(outputFilename, FileMode.Create))
            using (BinaryWriter bw = new BinaryWriter(stream))
            {
                // iOS Application to Network File Format
                bw.Write((byte)(package.Identifier >> 8));
                bw.Write((byte)(package.Identifier & 0x00FF));

                bw.Write((byte)(package.Version >> 8));
                bw.Write((byte)(package.Version & 0x00FF));

                // Have to do this for strings because binary writer inserts length of string at beginning of array.
                // Assuming not unicode as that adds 1-byte per character.
                byte[] textBytes = Encoding.ASCII.GetBytes(package.HardwareModel);
                bw.Write(textBytes);
                bw.Write((byte)0x00);

                textBytes = Encoding.ASCII.GetBytes(package.OSVersion);
                bw.Write(textBytes);
                bw.Write((byte)0x00);

                textBytes = Encoding.ASCII.GetBytes(package.AppName);
                bw.Write(textBytes);
                bw.Write((byte)0x00);

                textBytes = Encoding.ASCII.GetBytes(package.AppVersion);
                bw.Write(textBytes);
                bw.Write((byte)0x00);

                textBytes = Encoding.ASCII.GetBytes(package.SensorReaderSerialNumber);
                bw.Write(textBytes);
                bw.Write((byte)0x00);

                textBytes = Encoding.ASCII.GetBytes(package.SensorReaderFirmwareVersion);
                bw.Write(textBytes);
                bw.Write((byte)0x00);

                textBytes = Encoding.ASCII.GetBytes(package.TimeOfInterrogationUTC);
                bw.Write(textBytes);
                bw.Write((byte)0x00);

                // Number of Sections could be atmospheric + 1 sensor (total=2) or atmospheric + 2 sensor (total=3)
                bw.Write(package.NumberOfSections);

                // write the sections out.
                foreach (Section section in package.Sections)
                {
                    switch (section.Identifier)
                    {
                        case 0xFF11:
                            // Raw IOP Data Format Sequence
                            WriteRawIOPSection(bw, section as RawIOPSection);
                            break;

                        case 0xFF41:
                            // Atmospheric Pressure Sequence
                            WriteAtmosphericPressureSection(bw, section as AtmosphericPressureSection);
                            break;
                    }
                }

                // TODO: Add the hash value.
            }

        }

        /// <summary>
        /// Writes the atmospheric pressure section.
        /// </summary>
        /// <param name="bw">The bw.</param>
        /// <param name="section">The section.</param>
        private static void WriteAtmosphericPressureSection(BinaryWriter bw, AtmosphericPressureSection section)
        {

            bw.Write((byte)(section.Identifier >> 8));
            bw.Write((byte)(section.Identifier & 0x00FF));

            bw.Write((byte)(section.Version >> 8));
            bw.Write((byte)(section.Version & 0x00FF));

            // Have to do this for strings because binary writer inserts length of string at beginning of array.
            // Assuming not unicode as that adds 1-byte per character.
            byte[] textBytes = Encoding.ASCII.GetBytes(section.HardwareModel);
            bw.Write(textBytes);
            bw.Write((byte)0x00);

            textBytes = Encoding.ASCII.GetBytes(section.OSVersion);
            bw.Write(textBytes);
            bw.Write((byte)0x00);

            textBytes = Encoding.ASCII.GetBytes(section.AppName);
            bw.Write(textBytes);
            bw.Write((byte)0x00);

            textBytes = Encoding.ASCII.GetBytes(section.AppVersion);
            bw.Write(textBytes);
            bw.Write((byte)0x00);

            // Following the standard where MSByte comes first followed by LSByte
            bw.Write((byte)(section.NumberOfSamples >> 8));
            bw.Write((byte)(section.NumberOfSamples & 0x00FF));

            // Write the samples
            // Pressure Float (4 bytes)
            // Local Timestamp (string 26 bytes)
            for (int i = 0; i < section.NumberOfSamples; i++)
            {
                byte[] bytes = BitConverter.GetBytes(section.Samples[i].Pressure);
                bw.Write(bytes[3]);
                bw.Write(bytes[2]);
                bw.Write(bytes[1]);
                bw.Write(bytes[0]);

                // limit the string to 25 bytes + null terminator.
                // if string is less in length then need to pad with 0x00's.
                textBytes = Encoding.ASCII.GetBytes(section.Samples[i].LocalTimestampString);
                byte[] textBytes2 = new byte[25];
                if (textBytes.Length > 25)
                {
                    Array.Copy(textBytes, textBytes2, 25);
                    bw.Write(textBytes2);
                }
                else
                {
                    bw.Write(textBytes);
                }
                bw.Write((byte)0x00);
            }
        }

        /// <summary>
        /// Writes the raw iop section.
        /// </summary>
        /// <param name="bw">The bw.</param>
        /// <param name="section">The section.</param>
        private static void WriteRawIOPSection(BinaryWriter bw, RawIOPSection section)
        {
            bw.Write((byte)(section.Identifier >> 8));
            bw.Write((byte)(section.Identifier & 0x00FF));

            bw.Write((byte)(section.Version >> 8));
            bw.Write((byte)(section.Version & 0x00FF));

            // 3 words/6 bytes
            bw.Write((byte)((section.UniqueSensorId >> 40) & 0x00FF));
            bw.Write((byte)((section.UniqueSensorId >> 32) & 0x00FF));
            bw.Write((byte)((section.UniqueSensorId >> 24) & 0x00FF));
            bw.Write((byte)((section.UniqueSensorId >> 16) & 0x00FF));
            bw.Write((byte)((section.UniqueSensorId >> 8) & 0x00FF));
            bw.Write((byte)(section.UniqueSensorId & 0x00FF));

            byte[] textBytes = Encoding.ASCII.GetBytes(section.DataReadTimestamp);
            bw.Write(textBytes);
            bw.Write((byte)0x00);

            // Following the standard where MSByte comes first followed by LSByte
            bw.Write((byte)(section.NumberOfSamples >> 8));
            bw.Write((byte)(section.NumberOfSamples & 0x00FF));

            // Write the samples
            // Pressure Float (2 bytes)
            // Temperature (2 bytes)
            // Battery Voltage (2 bytes)
            for (int i = 0; i < section.NumberOfSamples; i++)
            {
                byte[] bytes = BitConverter.GetBytes(section.Samples[i].Pressure);
                bw.Write(bytes[1]);
                bw.Write(bytes[0]);

                bytes = BitConverter.GetBytes(section.Samples[i].Temperature);
                bw.Write(bytes[1]);
                bw.Write(bytes[0]);

                bytes = BitConverter.GetBytes(section.Samples[i].BatteryVoltage);
                bw.Write(bytes[1]);
                bw.Write(bytes[0]);
            }

            bw.Write(section.IntervalPeriod);

            bw.Write(section.LastIntervalTime);

            textBytes = Encoding.ASCII.GetBytes(section.DataEnableTimestamp.ToString());
            bw.Write(textBytes);
            bw.Write((byte)0x00);
        }
    }
}

﻿// ************************************************************
// Copyright (C) 2018 Injectsense, Inc. - All Rights Reserved
// ************************************************************
using System;
using Microsoft.EntityFrameworkCore;
using Injectsense.DataAccess.Repositories;

namespace Injectsense.DataAccess
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly MainContext _context;

        public IClinicRepository Clinics { get; private set; }
        public IPatientRepository Patients { get; private set; }
        public IDeviceRepository Devices { get; private set; }
        public IClinicStaffRepository ClinicStaffs { get; private set; }
        public ITransmissionRepository Transmissions { get; private set; }

        public UnitOfWork(MainContext context)
        {
            _context = context;

            Clinics = new ClinicRepository(_context);
            Patients  = new PatientRepository(_context);
            Devices = new DeviceRepository(_context);
            ClinicStaffs = new ClinicStaffRepository(_context);
            Transmissions = new TransmissionRepository(_context);
        }

        public int Commit()
        {
            return _context.SaveChanges();
        }

        public void Dispose()
        {
            _context.Dispose();
        }
    }
}

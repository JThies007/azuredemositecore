﻿// ************************************************************
// Copyright (C) 2018 Injectsense, Inc. - All Rights Reserved
// ************************************************************
using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using System.Linq;

using Injectsense.DataAccess.Entities;

namespace Injectsense.DataAccess.Repositories
{
    /// <summary>
    /// Patient repository.
    /// </summary>
    public class PatientRepository : Repository<Patient>, IPatientRepository
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="PatientRepository"/> class.
        /// </summary>
        /// <param name="context">The context.</param>
        public PatientRepository(MainContext context) : base(context)
        {
        }

        /// <summary>
        /// Gets the patient.
        /// </summary>
        /// <param name="PatientId">The patient identifier.</param>
        /// <returns>
        /// Patient
        /// </returns>
        public Patient GetPatient(Guid PatientId)
        {
            Patient patient = MainContext.Patients.Where(x => x.Id == PatientId).SingleOrDefault();

            return (patient);
        }

        /// <summary>
        /// Gets the implanted device information.
        /// </summary>
        /// <param name="patientId">The patient identifier.</param>
        /// <returns>List of device info.</returns>
        public List<(Guid DeviceId, string ImplantEye, DateTime? ImplantDate)> GetImplantedDeviceInfo(Guid patientId)
        {
            // returns list with left eye as first item if it exists.
            List<(Guid DeviceId, string ImplantEye, DateTime? ImplantDate)> devices =
                MainContext.PatientDeviceAssociations.Where(x => x.PatientId == patientId && x.DeviceStatusTypeId == 2)
                .Select(b => new { DeviceId = b.DeviceId, ImplantEye = b.ImplantEye, ImplantDate = b.ImplantDateNotUTC })
                .AsEnumerable()
                .Select(b => (DeviceId: b.DeviceId, ImplantEye: b.ImplantEye, ImplantDate: b.ImplantDate))
                .OrderBy(c => c.ImplantEye)
                .ToList();

            return (devices);
        }

        /// <summary>
        /// Gets the implanted device information.
        /// </summary>
        /// <param name="deviceSerialNumber">The device serial number.</param>
        /// <returns>device info</returns>
        public (Guid DeviceId, string ImplantEye, DateTime? ImplantDate) GetImplantedDeviceInfo(string deviceSerialNumber)
        {
            // deviceId will be empty guid if device not found AND device is implanted.
            (Guid DeviceId, string ImplantEye, DateTime? ImplantDate) device = (Guid.Empty, "", DateTime.UtcNow);

            // Get the device id.
            Guid deviceId = MainContext.Devices.Where(x => x.SerialNumber.Equals(deviceSerialNumber, StringComparison.InvariantCultureIgnoreCase))
                .Select(z => z.Id)
                .FirstOrDefault();

            if (deviceId != null)
            {
                // Find the device id in the patient-device association table where device is implanted (2).
                device =
                    MainContext.PatientDeviceAssociations.Where(x => x.DeviceId == deviceId && x.DeviceStatusTypeId == 2)
                    .Select(b => new { DeviceId = b.DeviceId, ImplantEye = b.ImplantEye, ImplantDate = b.ImplantDateNotUTC })
                    .AsEnumerable()
                    .Select(b => (DeviceId: b.DeviceId, ImplantEye: b.ImplantEye, ImplantDate: b.ImplantDate))
                    .FirstOrDefault();
            }

            return (device);
        }

        /// <summary>
        /// Gets the name of the physician.
        /// </summary>
        /// <param name="patientId">The patient identifier.</param>
        /// <returns>physician name</returns>
        public string GetPhysicianName(Guid patientId)
        {
            var clinicStaff = MainContext.ClinicPatientAssociations
            .Join(MainContext.Clinics,
                clinicPatientAssocation => clinicPatientAssocation.ClinicId,
                clinic => clinic.Id,
                (clinicPatientAssociation, clinic) => new { ClinicPatientAssociation = clinicPatientAssociation, Clinic = clinic })
            .Where(z => z.ClinicPatientAssociation.PatientId == patientId)
            .Join(MainContext.ClinicStaffs,
                x => x.ClinicPatientAssociation.Physician,
                y => y.Id,
                (x, y) => new { NamePrefix = y.NamePrefix, NameFirst = y.NameFirst, NameLast = y.NameLast })
            .Select(y => y).Single();

            return (string.Format(clinicStaff.NamePrefix + " " + clinicStaff.NameFirst + " " + clinicStaff.NameLast));
        }

        /// <summary>
        /// Gets the name of the device model.
        /// </summary>
        /// <param name="deviceId">The device identifier.</param>
        /// <returns>device model name</returns>
        public string GetDeviceModelName(Guid deviceId)
        {
            // Get the device type Id for the current device passed in.
            int deviceTypeId = MainContext.Devices.Where(x => x.Id == deviceId)
                .Select(y => y.DeviceTypeId)
                .Single();

            string deviceModelName = MainContext.DeviceTypes.Where(x => x.Id == deviceTypeId)
                .Select(y => y.Name)
                .Single();

            return (deviceModelName);
        }

        /// <summary>
        /// Associates the device to patient.
        /// </summary>
        /// <param name="patientId">The patient identifier.</param>
        /// <param name="serialNumber">The serial number.</param>
        /// <param name="implantEye">The implant eye.</param>
        /// <param name="implantDate">The implant date.</param>
        /// <param name="deviceStatus">The device status.</param>
        /// <param name="clinicStaffId">The clinic staff identifier.</param>
        public void AssociateDeviceToPatient(Guid patientId, string serialNumber, string implantEye, DateTime? implantDate, int deviceStatus, Guid clinicStaffId)
        {
            PatientDeviceAssociation pda = new PatientDeviceAssociation();
            pda.PatientId = patientId;
            pda.ImplantEye = implantEye;
            pda.ImplantDateNotUTC = implantDate;
            pda.DeviceStatusTypeId = deviceStatus;
            pda.LastUpdateTimeUTC = DateTime.UtcNow;
            pda.LastUpdatedBy = clinicStaffId;

            try
            {
                pda.DeviceId = MainContext.Devices.Where(x => x.SerialNumber == serialNumber).Select(y => y.Id).Single();
            }
            catch (Exception ex)
            {
                throw new Exception("Unable to find Device Serial Number in DeviceRepository.", ex);
            }

            MainContext.PatientDeviceAssociations.Add(pda);
        }

        /// <summary>
        /// Updates the patient device association.
        /// </summary>
        /// <param name="patientId">The patient identifier.</param>
        /// <param name="serialNumber">The serial number.</param>
        /// <param name="implantEye">The implant eye.</param>
        /// <param name="implantDate">The implant date.</param>
        /// <param name="deviceStatus">The device status.</param>
        /// <param name="clinicStaffId">The clinic staff identifier.</param>
        public void UpdatePatientDeviceAssociation(Guid patientId, string serialNumber, string implantEye, DateTime? implantDate, int deviceStatus, Guid clinicStaffId)
        {
            Guid deviceId = Guid.Empty;
            PatientDeviceAssociation pda;

            try
            {
                deviceId = MainContext.Devices.Where(x => x.SerialNumber == serialNumber).Select(y => y.Id).Single();
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("Unable to find Device Serial Number: {0} in DeviceRepository.", serialNumber), ex);
            }

            try
            {
                pda = MainContext.PatientDeviceAssociations.Where(x => x.PatientId == patientId && x.DeviceId == deviceId).Single();
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("Unable to find Patient Id: {0} or Device Id: {1} in Patient Device Associations.", patientId.ToString(), deviceId.ToString()), ex);
            }

            pda = MainContext.PatientDeviceAssociations.Where(x => x.PatientId == patientId && x.DeviceId == deviceId).Single();
            pda.ImplantEye = implantEye;
            pda.ImplantDateNotUTC = implantDate;
            pda.DeviceStatusTypeId = deviceStatus;
            pda.LastUpdateTimeUTC = DateTime.UtcNow;
            pda.LastUpdatedBy = clinicStaffId;

            MainContext.Entry(pda).State = EntityState.Modified;
        }

        /// <summary>
        /// Removes the device.
        /// </summary>
        /// <param name="patientId">The patient identifier.</param>
        /// <param name="serialNumber">The serial number.</param>
        /// <param name="deviceStatus">The device status.</param>
        /// <param name="clinicStaffId">The clinic staff identifier.</param>
        public void RemoveDevice(Guid patientId, string serialNumber, int deviceStatus, Guid clinicStaffId)
        {
            Guid deviceId = Guid.Empty;
            PatientDeviceAssociation pda;

            try
            {
                deviceId = MainContext.Devices.Where(x => x.SerialNumber == serialNumber).Select(y => y.Id).Single();
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("Unable to find Device Serial Number: {0} in DeviceRepository.", serialNumber), ex);
            }

            try
            {
                pda = MainContext.PatientDeviceAssociations.Where(x => x.PatientId == patientId && x.DeviceId == deviceId).Single();
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("Unable to find Patient Id: {0} or Device Id: {1} in Patient Device Associations.", patientId.ToString(), deviceId.ToString()), ex);
            }

            pda.DeviceStatusTypeId = deviceStatus;
            pda.LastUpdateTimeUTC = DateTime.UtcNow;
            pda.LastUpdatedBy = clinicStaffId;
        }

        /// <summary>
        /// Saves the plot data.
        /// </summary>
        /// <param name="transmissionId">The transmission identifier.</param>
        /// <param name="leftSensorPlotPoints">The left sensor plot points.</param>
        /// <param name="rightSensorPlotPoints">The right sensor plot points.</param>
        /// <param name="leftSensorBaselinePlotPoints">The left sensor baseline plot points.</param>
        /// <param name="rightSensorBaselinePlotPoints">The right sensor baseline plot points.</param>
        public void SavePlotData(long transmissionId,
                                        List<PlotPressureDataPoint> leftSensorPlotPoints,
                                        List<PlotPressureDataPoint> rightSensorPlotPoints,
                                        List<PlotPressureDataPoint> leftSensorBaselinePlotPoints,
                                        List<PlotPressureDataPoint> rightSensorBaselinePlotPoints)
        {

            // If there are no points to save the for loops do not get executed.

            // Save the left sensor plot data.
            if (leftSensorPlotPoints != null)
            {
                for (int i = 0; i < leftSensorPlotPoints.Count; i++)
                {
                    LeftSensorPlotData lData = new LeftSensorPlotData();
                    lData.TransmissionId = transmissionId;
                    lData.EventDateTime = leftSensorPlotPoints[i].DateValue;
                    lData.Value = leftSensorPlotPoints[i].PressureValue;
                    MainContext.LeftSensorPlotDatas.Add(lData);
                }
            }

            // Save the right sensor plot data.
            if (rightSensorPlotPoints != null)
            {
                for (int i = 0; i < rightSensorPlotPoints.Count; i++)
                {
                    RightSensorPlotData lData = new RightSensorPlotData();
                    lData.TransmissionId = transmissionId;
                    lData.EventDateTime = rightSensorPlotPoints[i].DateValue;
                    lData.Value = rightSensorPlotPoints[i].PressureValue;
                    MainContext.RightSensorPlotDatas.Add(lData);
                }
            }

            // Save the left baseline sensor plot data.
            if (leftSensorBaselinePlotPoints != null)
            {
                for (int i = 0; i < leftSensorBaselinePlotPoints.Count; i++)
                {
                    LeftSensorBaselinePlotData lData = new LeftSensorBaselinePlotData();
                    lData.TransmissionId = transmissionId;
                    lData.EventDateTime = leftSensorBaselinePlotPoints[i].DateValue;
                    lData.Value = leftSensorBaselinePlotPoints[i].PressureValue;
                    MainContext.LeftSensorBaselinePlotDatas.Add(lData);
                }
            }

            // Save the right baseline sensor plot data.
            if (rightSensorBaselinePlotPoints != null)
            {
                for (int i = 0; i < rightSensorBaselinePlotPoints.Count; i++)
                {
                    RightSensorBaselinePlotData lData = new RightSensorBaselinePlotData();
                    lData.TransmissionId = transmissionId;
                    lData.EventDateTime = rightSensorBaselinePlotPoints[i].DateValue;
                    lData.Value = rightSensorBaselinePlotPoints[i].PressureValue;
                    MainContext.RightSensorBaselinePlotDatas.Add(lData);
                }
            }
        }

        /// <summary>
        /// Gets the plot data.
        /// </summary>
        /// <param name="startDate">The start date.</param>
        /// <param name="stopDate">The stop date.</param>
        /// <param name="plotDataType">Type of the plot data.</param>
        /// <returns>List of plot data points</returns>
        public List<PlotPressureDataPoint> GetPlotData(Guid patientId, DateTime startDate, DateTime stopDate, PlotDataType plotDataType)
        {
            List<PlotPressureDataPoint> plotDataList = new List<PlotPressureDataPoint>();

            switch (plotDataType)
            {
                case PlotDataType.LeftSensorBaselinePlotData:
                    plotDataList = MainContext.Transmissions
                                    .Join(MainContext.LeftSensorBaselinePlotDatas, transmission => transmission.Id, leftSensorBaselinePlotData => leftSensorBaselinePlotData.TransmissionId, (transmission, leftSensorBaselinePlotData) => new { Transmission = transmission, LeftSensorBaselinePlotData = leftSensorBaselinePlotData })
                                    .Where(x => x.Transmission.PatientId.Equals(patientId) && (x.LeftSensorBaselinePlotData.EventDateTime >= startDate && x.LeftSensorBaselinePlotData.EventDateTime <= stopDate)).Select(x => new PlotPressureDataPoint() { DateValue = x.LeftSensorBaselinePlotData.EventDateTime, PressureValue = x.LeftSensorBaselinePlotData.Value }).ToList();
                    break;

                case PlotDataType.LeftSensorPlotData:
                    plotDataList = MainContext.Transmissions
                                    .Join(MainContext.LeftSensorPlotDatas, transmission => transmission.Id, leftSensorPlotData => leftSensorPlotData.TransmissionId, (transmission, leftSensorPlotData) => new { Transmission = transmission, LeftSensorPlotData = leftSensorPlotData })
                                    .Where(x => x.Transmission.PatientId.Equals(patientId) && (x.LeftSensorPlotData.EventDateTime >= startDate && x.LeftSensorPlotData.EventDateTime <= stopDate)).Select(x => new PlotPressureDataPoint() { DateValue = x.LeftSensorPlotData.EventDateTime, PressureValue = x.LeftSensorPlotData.Value }).ToList();
                    break;

                case PlotDataType.RightSensorBaselinePlotData:
                    plotDataList = MainContext.Transmissions
                                    .Join(MainContext.RightSensorBaselinePlotDatas, transmission => transmission.Id, rightSensorBaselinePlotData => rightSensorBaselinePlotData.TransmissionId, (transmission, rightSensorBaselinePlotData) => new { Transmission = transmission, RightSensorBaselinePlotData = rightSensorBaselinePlotData })
                                    .Where(x => x.Transmission.PatientId.Equals(patientId) && (x.RightSensorBaselinePlotData.EventDateTime >= startDate && x.RightSensorBaselinePlotData.EventDateTime <= stopDate)).Select(x => new PlotPressureDataPoint() { DateValue = x.RightSensorBaselinePlotData.EventDateTime, PressureValue = x.RightSensorBaselinePlotData.Value }).ToList();
                    break;

                case PlotDataType.RightSensorPlotData:
                    plotDataList = MainContext.Transmissions
                                    .Join(MainContext.RightSensorPlotDatas, transmission => transmission.Id, rightSensorPlotData => rightSensorPlotData.TransmissionId, (transmission, rightSensorPlotData) => new { Transmission = transmission, RightSensorPlotData = rightSensorPlotData })
                                    .Where(x => x.Transmission.PatientId.Equals(patientId) && (x.RightSensorPlotData.EventDateTime >= startDate && x.RightSensorPlotData.EventDateTime <= stopDate)).Select(x => new PlotPressureDataPoint() { DateValue = x.RightSensorPlotData.EventDateTime, PressureValue = x.RightSensorPlotData.Value }).ToList();
                    break;

                default:
                    throw new Exception(string.Format("Invalid PlotDataType entered: {0}, for PatientId: {1}", plotDataType.ToString(), patientId.ToString()));
                  //  break; Commented out to satisfy compiler warning.
            }

            return (plotDataList);
        }

        /// <summary>
        /// Updates the patient device association.
        /// </summary>
        /// <param name="patientId">The patient identifier.</param>
        /// <param name="nameFirst">The name first.</param>
        /// <param name="nameLast">The name last.</param>
        /// <param name="nameMiddle">The name middle.</param>
        /// <param name="dateOfBirthNotUTC">The date of birth not UTC.</param>
        /// <param name="identifier">The identifier.</param>
        /// <param name="phoneHome">The phone home.</param>
        /// <param name="gender">The gender.</param>
        public void UpdatePatientDeviceAssociation(Guid patientId, string nameFirst, string nameLast, string nameMiddle,
                                                    DateTime dateOfBirthNotUTC, string identifier, string phoneHome, string gender)
        {
            Patient patientEntity = null;

            try
            {
                // Find the patient entity and save the specific fields that were available on form.
                patientEntity = MainContext.Patients.Where(x => x.Id == patientId).Single();
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("Unable to find Patient Entity for Patient Id: {0}", patientId.ToString()), ex);
            }

            patientEntity.NameFirst = nameFirst;
            patientEntity.NameLast = nameLast;
            patientEntity.NameMiddle = nameMiddle;
            patientEntity.DateOfBirthNotUTC = dateOfBirthNotUTC;
            patientEntity.Identifier = identifier;
            patientEntity.PhoneHome = phoneHome;
            patientEntity.Gender = gender;

            MainContext.Entry(patientEntity).State = EntityState.Modified;
            MainContext.SaveChanges();
        }

        /// <summary>
        /// Gets the patient.
        /// </summary>
        /// <param name="deviceId">The device identifier.</param>
        /// <returns>
        /// Guid representing the patient id.
        /// </returns>
        public Guid GetPatientFromDeviceId(Guid deviceId)
        {
            Guid patientId = Guid.Empty;

            try
            {
                // There will be multiple rows for multiple sensors. Just need 1 to get patient id.
                patientId = MainContext.PatientDeviceAssociations.Where(x => (x.DeviceId == deviceId)).Select(y => y.PatientId).First();
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("Unable to find Patient Entity for DeviceId: {0}", deviceId), ex);
            }

            return (patientId);
        }

        /// <summary>
        /// Gets the main context.
        /// </summary>
        /// <value>
        /// The main context.
        /// </value>
        public MainContext MainContext
        {
            get { return Context as MainContext; }
        }
    }
}


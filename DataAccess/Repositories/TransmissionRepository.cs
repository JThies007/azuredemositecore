﻿// ************************************************************
// Copyright (C) 2018 Injectsense, Inc. - All Rights Reserved
// ************************************************************
using Injectsense.DataAccess.Entities;
using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using System.Linq;

namespace Injectsense.DataAccess.Repositories
{
    public class TransmissionRepository : Repository<Transmission>, ITransmissionRepository
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="TransmissionRepository"/> class.
        /// </summary>
        /// <param name="context">The context.</param>
        public TransmissionRepository(MainContext context) : base(context)
        {
        }

        /// <summary>
        /// Gets all transmissions.
        /// </summary>
        /// <param name="sensorSerialNumber">The sensor serial number.</param>
        /// <param name="receivedMinTimeUTC">The received minimum time UTC.</param>
        /// <param name="receivedMaxTimeUTC">The received maximum time UTC.</param>
        /// <returns>Transmission List Info for sensor serial number and date range</returns>
        public List<TransmissionListInfo> GetAllTransmissions(string sensorSerialNumber, DateTime? receivedMinTimeUTC, DateTime? receivedMaxTimeUTC)
        {
            List<TransmissionListInfo> txList = null;

            // handle scenario: serial number + both dates
            // handle scenario: serial number and not both dates
            // handle scenario: no serial number and both dates
            // handle scenario: nothing provided - just return a list of latest back 30 days
            // all other scenarios return an empty list (do nothing)

            // handle scenario: serial number + both dates
            if (!string.IsNullOrEmpty(sensorSerialNumber) && receivedMinTimeUTC.HasValue && receivedMaxTimeUTC.HasValue)
            {
                txList = MainContext.Transmissions.Where(x => (x.ReceivedTimeUTC > receivedMinTimeUTC)
                    && (x.ReceivedTimeUTC < receivedMaxTimeUTC) && ((x.DeviceSerialNumber1 == sensorSerialNumber) || (x.DeviceSerialNumber2 == sensorSerialNumber)))
                    .Select(y => new TransmissionListInfo
                    {
                        ReceivedTimeUTC = y.ReceivedTimeUTC.ToString(),
                        TransmissionId = y.Id,
                        TimeOfInterrogationUTC = y.TimeOfInterrogationUTC.ToString(),
                        ReaderSerialNumber = y.ReaderSerialNumber,
                        SensorSerialNumber1 = y.DeviceSerialNumber1,
                        SensorSerialNumber2 = y.DeviceSerialNumber2
                    }).ToList();
            }
            else if ((!string.IsNullOrEmpty(sensorSerialNumber) && (!receivedMinTimeUTC.HasValue || !receivedMaxTimeUTC.HasValue)))
            {
                // handle scenario: serial number and not both dates
                txList = MainContext.Transmissions.Where(x => ((x.DeviceSerialNumber1 == sensorSerialNumber) || (x.DeviceSerialNumber2 == sensorSerialNumber)))
                    .Select(y => new TransmissionListInfo
                    {
                        ReceivedTimeUTC = y.ReceivedTimeUTC.ToString(),
                        TransmissionId = y.Id,
                        TimeOfInterrogationUTC = y.TimeOfInterrogationUTC.ToString(),
                        ReaderSerialNumber = y.ReaderSerialNumber,
                        SensorSerialNumber1 = y.DeviceSerialNumber1,
                        SensorSerialNumber2 = y.DeviceSerialNumber2
                    }).ToList();
            }
            else if ((string.IsNullOrEmpty(sensorSerialNumber) && receivedMinTimeUTC.HasValue && receivedMaxTimeUTC.HasValue))
            {
                // handle scenario: no serial number and both dates
                txList = MainContext.Transmissions.Where(x => (x.ReceivedTimeUTC > receivedMinTimeUTC)
                    && (x.ReceivedTimeUTC < receivedMaxTimeUTC))
                    .Select(y => new TransmissionListInfo
                    {
                        ReceivedTimeUTC = y.ReceivedTimeUTC.ToString(),
                        TransmissionId = y.Id,
                        TimeOfInterrogationUTC = y.TimeOfInterrogationUTC.ToString(),
                        ReaderSerialNumber = y.ReaderSerialNumber,
                        SensorSerialNumber1 = y.DeviceSerialNumber1,
                        SensorSerialNumber2 = y.DeviceSerialNumber2
                    }).ToList();
            }
            else if ((string.IsNullOrEmpty(sensorSerialNumber) && !receivedMinTimeUTC.HasValue && !receivedMaxTimeUTC.HasValue))
            {
                DateTime startTime = DateTime.UtcNow.AddDays(-30);
                DateTime stopTime = DateTime.UtcNow;

                txList = MainContext.Transmissions.Where(x => (x.ReceivedTimeUTC > startTime)
                    && (x.ReceivedTimeUTC < stopTime))
                    .Select(y => new TransmissionListInfo
                    {
                        ReceivedTimeUTC = y.ReceivedTimeUTC.ToString(),
                        TransmissionId = y.Id,
                        TimeOfInterrogationUTC = y.TimeOfInterrogationUTC.ToString(),
                        ReaderSerialNumber = y.ReaderSerialNumber,
                        SensorSerialNumber1 = y.DeviceSerialNumber1,
                        SensorSerialNumber2 = y.DeviceSerialNumber2
                    }).ToList();
            }
            else
            {
                // all other scenarios return an empty list (do nothing)
            }

            return (txList);
        }

        /// <summary>
        /// Gets the transmission.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>Transmission</returns>
        public Transmission GetTransmission(long id)
        {
            Transmission transmission = null;

            try
            {
                transmission = MainContext.Transmissions.Where(x => x.Id == id).Single();
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("Unable to find transmission id requested. Transmission Id: {0}", id), ex);
            }

            return (transmission);
        }

        /// <summary>
        /// Saves the transmission data.
        /// </summary>
        /// <param name="patientId">The patient identifier.</param>
        /// <param name="leftDeviceSerialNumber">The left device serial number.</param>
        /// <param name="rightDeviceSerialNumber">The right device serial number.</param>
        /// <param name="receivedTimeUTC">The received time UTC.</param>
        /// <param name="timeOfInterrogationUTC">The time of interrogation UTC.</param>
        /// <param name="package">The package.</param>
        public void SaveTransmissionData(
                                    Guid patientId,
                                    string deviceSerialNumber1,
                                    string deviceSerialNumber2,
                                    DateTime receivedTimeUTC,
                                    DateTime timeOfInterrogationUTC,
                                    byte[] package)
        {
            Transmission transmission = new Transmission();
            transmission.PatientId = patientId;
            transmission.DeviceSerialNumber1 = deviceSerialNumber1;
            transmission.DeviceSerialNumber2 = deviceSerialNumber2;
            transmission.ReceivedTimeUTC = receivedTimeUTC;
            transmission.TimeOfInterrogationUTC = timeOfInterrogationUTC;
            transmission.Package = (package == null) ? new byte[] { 0x00 } : package; // if it's null then just drop a byte in. TODO: This should be removed when not using canned data.
            this.Add(transmission);
        }

        /// <summary>
        /// Determines whether [is duplicate transmission] [the specified time of interrogation UTC].
        /// </summary>
        /// <param name="timeOfInterrogationUTC">The time of interrogation UTC.</param>
        /// <param name="deviceSerialNumber1">The device serial number1.</param>
        /// <param name="deviceSerialNumber2">The device serial number2.</param>
        /// <returns>
        ///   <c>true</c> if [is duplicate transmission] [the specified time of interrogation UTC]; otherwise, <c>false</c>.
        /// </returns>
        public bool IsDuplicateTransmission(DateTime timeOfInterrogationUTC, string deviceSerialNumber1, string deviceSerialNumber2)
        {
            return(MainContext.Transmissions.Any(x => x.TimeOfInterrogationUTC.Equals(timeOfInterrogationUTC) 
                    && (x.DeviceSerialNumber1.Equals(deviceSerialNumber1,StringComparison.InvariantCultureIgnoreCase) || x.DeviceSerialNumber1.Equals(deviceSerialNumber2, StringComparison.InvariantCultureIgnoreCase))
                    && (x.DeviceSerialNumber2.Equals(deviceSerialNumber1,StringComparison.InvariantCultureIgnoreCase) || x.DeviceSerialNumber2.Equals(deviceSerialNumber2, StringComparison.InvariantCultureIgnoreCase))));
        }

        /// <summary>
        /// Gets the transmission identifier.
        /// </summary>
        /// <param name="DeviceSerialNumber1">The device serial number1.</param>
        /// <param name="DeviceSerialNumber2">The device serial number2.</param>
        /// <param name="timeOfInterrogationUTC">The time of interrogation UTC.</param>
        /// <returns></returns>
        /// <exception cref="Exception"></exception>
        public long GetTransmissionId(string deviceSerialNumber1, string deviceSerialNumber2, DateTime timeOfInterrogationUTC)
        {
            try
            {
                return (MainContext.Transmissions.Where(x => x.TimeOfInterrogationUTC.Equals(timeOfInterrogationUTC)
                            && (x.DeviceSerialNumber1.Equals(deviceSerialNumber1, StringComparison.InvariantCultureIgnoreCase) || x.DeviceSerialNumber1.Equals(deviceSerialNumber2, StringComparison.InvariantCultureIgnoreCase))
                            && (x.DeviceSerialNumber2.Equals(deviceSerialNumber1, StringComparison.InvariantCultureIgnoreCase) || x.DeviceSerialNumber2.Equals(deviceSerialNumber2, StringComparison.InvariantCultureIgnoreCase))))
                            .Select(t => t.Id).Single();
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("Unable to find transmission requested. Serial Number1: {0}, Serial Number2: {1}, Time Of Interrogation UTC: {2}", deviceSerialNumber1, deviceSerialNumber2, timeOfInterrogationUTC.ToString()), ex);
            }
        }

        /// <summary>
        /// Gets the main context.
        /// </summary>
        /// <value>
        /// The main context.
        /// </value>
        public MainContext MainContext
        {
            get { return Context as MainContext; }
        }
    }
}

﻿// ************************************************************
// Copyright (C) 2018 Injectsense, Inc. - All Rights Reserved
// ************************************************************
using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using Injectsense.DataAccess.Entities;

namespace Injectsense.DataAccess.Repositories
{
    /// <summary>
    /// Device Repository.
    /// </summary>
    public class DeviceRepository : Repository<Device>, IDeviceRepository
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="DeviceRepository"/> class.
        /// </summary>
        /// <param name="context">The context.</param>
        public DeviceRepository(MainContext context) : base(context)
        {
        }

        /// <summary>
        /// Gets the serial number.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>serial number in a string</returns>
        public string GetSerialNumber(Guid id)
        {
            string serialNumber = string.Empty;

            try
            {
                serialNumber = MainContext.Devices.Where(a => a.Id == id).Select(x => x.SerialNumber).Single();
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("Unable to find device requested. Device Id: {0}", id.ToString()), ex);
            }

            return (serialNumber);
        }

        /// <summary>
        /// Gets the serial number.
        /// </summary>
        /// <param name="sensorId">The sensor identifier.</param>
        /// <returns>Device Id and Serial Number string.</returns>
        public (Guid DeviceId, string SensorSerialNumber) GetSerialNumber(string sensorId)
        {
            Device device;

            try
            {
                device = MainContext.Devices.Where(a =>a.UniqueManufacturingIdentifier == sensorId).Single();
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("Unable to find device requested. Sensor Id: {0}", sensorId), ex);
            }

            return (device.Id, device.SerialNumber);
        }

        /// <summary>
        /// Gets the implant eye.
        /// </summary>
        /// <param name="deviceSerialNumber">The device serial number.</param>
        /// <returns>string representing implant eye location</returns>
        public string GetImplantEye(string deviceSerialNumber)
        {
            string implantEye = MainContext.Devices
                .Join(MainContext.PatientDeviceAssociations, device => device.Id, pda => pda.DeviceId, (device, pda) => new { Device = device, PatientDeviceAssociation = pda })
                .Where(x => x.Device.SerialNumber.ToLower() == deviceSerialNumber.ToLower()).Select(x => x.PatientDeviceAssociation.ImplantEye).Single();

            return (implantEye);
        }

        /// <summary>
        /// Gets the device model.
        /// </summary>
        /// <param name="deviceId">The device identifier.</param>
        /// <returns>string representing device model</returns>
        public string GetDeviceModel(Guid deviceId)
        {
            string modelName = MainContext.Devices
                .Join(MainContext.DeviceTypes, device => device.DeviceTypeId, deviceType => deviceType.Id, (device, deviceType) => new { Device = device, DeviceType = deviceType })
                .Where(x => x.Device.Id == deviceId).Select(x => x.DeviceType.Name).Single();

            return (modelName);
        }

        /// <summary>
        /// Gets the available device model list.
        /// </summary>
        /// <returns>id and name</returns>
        public List<(int Id, string Name)> GetAvailableDeviceModelList()
        {
            // TODO: At some point this will need to check:
            // a.) Availability of model.
            // b.) Availability in country of model.
            List<(int Id, string Name)> listOfModels = MainContext.DeviceTypes.Select(x => new { Id = x.Id, Name = x.Name })
                .AsEnumerable()
                .Select(b => (Id: b.Id, Name: b.Name))
                .ToList();

            return (listOfModels);
        }

        /// <summary>
        /// Devices the is available.
        /// </summary>
        /// <param name="serialNumber">The serial number.</param>
        /// <returns>true if device is available.</returns>
        public bool DeviceIsAvailable(string serialNumber)
        {
            // TODO: Currently availability is true if:
            // a.) Device does not exist.
            // b.) Device does exist and is available for use.
            // In production a fixed list of devices will be in database and you'll pick and choose from existing items.
            if (!MainContext.Devices.Any(x => x.SerialNumber == serialNumber))
            {
                return (true);
            }

            if (MainContext.Devices.Any(x => x.SerialNumber == serialNumber && x.IsAvailableForUse))
            {
                return (true);
            }

            return (false);
        }

        /// <summary>
        /// Gets the device identifier.
        /// </summary>
        /// <param name="serialNumber">The serial number.</param>
        /// <returns>device id in a guid</returns>
        public Guid GetDeviceId(string serialNumber)
        {
            Guid deviceId = MainContext.Devices.Where(x => x.SerialNumber == serialNumber).Select(y => y.Id).DefaultIfEmpty(Guid.Empty).FirstOrDefault();

            return (deviceId);
        }

        /// <summary>
        /// Gets the model serial number prefix.
        /// </summary>
        /// <param name="modelName">Name of the model.</param>
        /// <returns>device serial number prefix.</returns>
        public string GetModelSerialNumberPrefix(string modelName)
        {
            string devicePrefix = "";

            try
            {
                devicePrefix = MainContext.DeviceTypes.Where(x => x.Name == modelName).Select(y => y.SerialNumberPrefix).Single();
            }
            catch
            {
                throw new Exception("Unable to find device model.");
            }

            return (devicePrefix);
        }

        /// <summary>
        /// Gets the main context.
        /// </summary>
        /// <value>
        /// The main context.
        /// </value>
        public MainContext MainContext
        {
            get { return Context as MainContext; }
        }
    }
}

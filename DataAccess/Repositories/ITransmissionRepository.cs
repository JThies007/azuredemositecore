﻿// ************************************************************
// Copyright (C) 2018 Injectsense, Inc. - All Rights Reserved
// ************************************************************
using Injectsense.DataAccess.Entities;
using System;
using System.Collections.Generic;

namespace Injectsense.DataAccess.Repositories
{
    public class TransmissionListInfo
    {
        public long TransmissionId { get; set; }
        public string ReceivedTimeUTC { get; set; }
        public string TimeOfInterrogationUTC { get; set; }
        public string SensorSerialNumber1 { get; set; }
        public string SensorSerialNumber2 { get; set; }
        public string ReaderSerialNumber { get; set; }
    }

    public interface ITransmissionRepository : IRepository<Transmission>
    {
        Transmission GetTransmission(long id);

        void SaveTransmissionData(
            Guid patientId,
            string deviceSerialNumber1,
            string deviceSerialNumber2,
            DateTime receivedTimeUTC,
            DateTime timeOfInterrogationUTC,
            byte[] package);

        bool IsDuplicateTransmission(DateTime timeOfInterrogationUTC, string deviceSerialNumber1, string deviceSerialNumber2);

        long GetTransmissionId(string deviceSerialNumber1, string deviceSerialNumber2, DateTime timeOfInterrogationUTC);

        List<TransmissionListInfo> GetAllTransmissions(string sensorSerialNumber, DateTime? receivedMinTimeUTC, DateTime? receivedMaxTimeUTC);
    }
}

﻿// ************************************************************
// Copyright (C) 2018 Injectsense, Inc. - All Rights Reserved
// ************************************************************
using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using Injectsense.DataAccess.Entities;

namespace Injectsense.DataAccess.Repositories
{
    /// <summary>
    /// Clinic Repository.
    /// </summary>
    public class ClinicRepository : Repository<Clinic>, IClinicRepository
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ClinicRepository"/> class.
        /// </summary>
        /// <param name="context">The context.</param>
        public ClinicRepository(MainContext context) : base(context)
        {
        }

        /// <summary>
        /// Gets all patients with new transmissions for clinic.
        /// </summary>
        /// <param name="clinicId">The clinic identifier.</param>
        /// <returns>List<ClinicPatientAssociation></returns>
        public List<ClinicPatientAssociation> GetAllPatientsWithNewTransmissionsForClinic(Guid clinicId)
        {
            // TODO: Will need to clean this up when requirements created on what determines valid patient (not discontinued, etc.).
            List<ClinicPatientAssociation> patientTxList = MainContext.ClinicPatientAssociations.Where(a => (a.ClinicId == clinicId) && (a.PatientDiscontinueReasonTypeId == 0) && (a.NewTransmissionCount != 0))
                .OrderByDescending(b => b.MostRecentTransmissionReceivedDateUTC).ToList();

            return (patientTxList);
        }

        /// <summary>
        /// Gets the last office visit date.
        /// </summary>
        /// <param name="clinicId">The clinic identifier.</param>
        /// <param name="patientId">The patient identifier.</param>
        /// <returns>DateTime?</returns>
        public DateTime? GetLastOfficeVisitDate(Guid clinicId, Guid patientId)
        {
            DateTime? dateTime = MainContext.ClinicPatientAssociations.Where(a => a.ClinicId == clinicId && a.PatientId == patientId)
                .Select(x => x.LastOfficeVisitDateUTC).DefaultIfEmpty().FirstOrDefault();

            return (dateTime);

        }

        /// <summary>
        /// Gets all patients.
        /// </summary>
        /// <param name="clinicId">The clinic identifier.</param>
        /// <returns>Lost of Patients</returns>
        public List<Patient> GetAllPatients(Guid clinicId)
        {
            // Could be better but sing statement with Join was having problems.
            List<Guid> patientIdList = MainContext.ClinicPatientAssociations.Where(a => a.ClinicId == clinicId)
                .Select(x => x.PatientId).ToList();

            List<Patient> patientList = MainContext.Patients.Where(x => patientIdList.Contains(x.Id)).ToList();


            return (patientList.ToList());
        }

        /// <summary>
        /// Gets the clinic dot net time zone information.
        /// </summary>
        /// <param name="clinicId">The clinic identifier.</param>
        /// <returns>string representing time zone</returns>
        public string GetClinicDotNetTimeZoneInfo(Guid clinicId)
        {
            string timeZoneInfo = MainContext.Clinics
                .Join(MainContext.TimeZones,
                clinic => clinic.TimeZoneId,
                timeZone => timeZone.Id,
                (clinic, timeZone ) => new { Clinic = clinic, TimeZone = timeZone})
                .Where(x => x.Clinic.Id == clinicId)
                .Select(y => y.TimeZone.DotNetTimeZoneInfo).Single();

            return (timeZoneInfo);
        }

        /// <summary>
        /// Associates the patient.
        /// </summary>
        /// <param name="clinicId">The clinic identifier.</param>
        /// <param name="patientId">The patient identifier.</param>
        /// <param name="clinicStaffId">The clinic staff identifier.</param>
        /// <param name="followingPhysician">The following physician.</param>
        public void AssociatePatient(Guid clinicId, Guid patientId, Guid clinicStaffId, Guid followingPhysician)
        {
            ClinicPatientAssociation cpa = new ClinicPatientAssociation();

            cpa.ClinicId = clinicId;
            cpa.PatientId = patientId;
            cpa.LastUpdatedBy = clinicStaffId;
            cpa.LastUpdateTimeUTC = DateTime.UtcNow;
            cpa.PatientDiscontinueReasonTypeId = 0;  // Not discontinued.
            cpa.ClinicPatientAssociationStatusTypeId = 1; // Active
            cpa.AssociationCreateDateUTC = DateTime.UtcNow;
            cpa.Physician = followingPhysician;

            MainContext.ClinicPatientAssociations.Add(cpa);
        }

        /// <summary>
        /// Gets the clinic patient association.
        /// </summary>
        /// <param name="clinicId">The clinic identifier.</param>
        /// <param name="patientId">The patient identifier.</param>
        /// <returns>ClinicPatientAssociation</returns>
        public ClinicPatientAssociation GetClinicPatientAssociation(Guid clinicId, Guid patientId)
        {
            ClinicPatientAssociation cpa = MainContext.ClinicPatientAssociations.Where(x => (x.ClinicId == clinicId) && (x.PatientId == patientId)).FirstOrDefault();

            return (cpa);
        }

        /// <summary>
        /// Updates the most recent transmission serial number information.
        /// </summary>
        /// <param name="clinicId">The clinic identifier.</param>
        /// <param name="patientId">The patient identifier.</param>
        /// <param name="serialNumber">The serial number.</param>
        /// <param name="implantEye">The implant eye.</param>
        public void UpdateMostRecentTransmissionSerialNumberInformation(Guid clinicId, Guid patientId, string serialNumber, string implantEye)
        {
            ClinicPatientAssociation cpa = MainContext.ClinicPatientAssociations.Where(x => (x.ClinicId == clinicId) && (x.PatientId == patientId)).FirstOrDefault();

            if (implantEye.ToLower().StartsWith("l"))
            {
                cpa.MostRecentTransmissionLeftEyeDSN = serialNumber;
            }
            else
            {
                cpa.MostRecentTransmissionRightEyeDSN = serialNumber;
            }

            MainContext.Entry(cpa).State = EntityState.Modified;
        }

        /// <summary>
        /// Updates the most recent patient transmission information.
        /// </summary>
        /// <param name="transmissionId">The transmission identifier.</param>
        /// <param name="patientId">The patient identifier.</param>
        /// <param name="leftDeviceSerialNumber">The left device serial number.</param>
        /// <param name="rightDeviceSerialNumber">The right device serial number.</param>
        /// <param name="receivedTimeUTC">The received time UTC.</param>
        public void UpdateMostRecentPatientTransmissionInformation(long transmissionId, Guid patientId, string leftDeviceSerialNumber, string rightDeviceSerialNumber, DateTime receivedTimeUTC)
        {
            // Find the clinic patient association and update it.
            ClinicPatientAssociation cpa = MainContext.ClinicPatientAssociations.Where(x => x.PatientId == patientId).Single();

            // Only update the table if incoming is newer than what is in table.
            if (cpa.MostRecentTransmissionReceivedDateUTC < receivedTimeUTC)
            {
                cpa.MostRecentTransmissionId = transmissionId;
                cpa.MostRecentTransmissionLeftEyeDSN = leftDeviceSerialNumber;
                cpa.MostRecentTransmissionRightEyeDSN = rightDeviceSerialNumber;
                cpa.MostRecentTransmissionReceivedDateUTC = receivedTimeUTC;

                MainContext.Entry(cpa).State = EntityState.Modified;
            }
        }

        /// <summary>
        /// Gets the main context.
        /// </summary>
        /// <value>
        /// The main context.
        /// </value>
        public MainContext MainContext
        {
            get { return Context as MainContext; }
        }
    }
}

﻿// ************************************************************
// Copyright (C) 2018 Injectsense, Inc. - All Rights Reserved
// ************************************************************
using System;
using Injectsense.DataAccess.Entities;

namespace Injectsense.DataAccess.Repositories
{
    public interface IClinicStaffRepository : IRepository<ClinicStaff>
    {
        bool IsValidUser(string user, string password, out string[] roles, out Guid clinicId);
        bool IsValidUser(string user, out string[] roles, out Guid clinicId, out Guid clinicStaffId);
    }
}

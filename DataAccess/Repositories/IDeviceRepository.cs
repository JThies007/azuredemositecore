﻿// ************************************************************
// Copyright (C) 2018 Injectsense, Inc. - All Rights Reserved
// ************************************************************
using System;
using System.Collections.Generic;
using Injectsense.DataAccess.Entities;

namespace Injectsense.DataAccess.Repositories
{
    public interface IDeviceRepository : IRepository<Device>
    {
        string GetSerialNumber(Guid id);

        (Guid DeviceId, string SensorSerialNumber) GetSerialNumber(string sensorId);

        string GetImplantEye(string deviceSerialNumber);

        string GetDeviceModel(Guid deviceId);

        List<(int Id, string Name)> GetAvailableDeviceModelList();

        bool DeviceIsAvailable(string deviceSerialNumber);

        Guid GetDeviceId(string serialNumber);

        string GetModelSerialNumberPrefix(string modelName);
    }
}

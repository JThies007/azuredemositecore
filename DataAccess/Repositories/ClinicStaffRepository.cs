﻿// ************************************************************
// Copyright (C) 2018 Injectsense, Inc. - All Rights Reserved
// ************************************************************

using System;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using Injectsense.DataAccess.Entities;

namespace Injectsense.DataAccess.Repositories
{
    /// <summary>
    /// ClinicStaff Repository.
    /// </summary>
    public class ClinicStaffRepository : Repository<ClinicStaff>, IClinicStaffRepository
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ClinicStaffRepository"/> class.
        /// </summary>
        /// <param name="context">The context.</param>
        public ClinicStaffRepository(MainContext context) : base(context)
        {
        }

        /// <summary>
        /// Determines whether [is valid user] [the specified user].
        /// </summary>
        /// <param name="user">The user.</param>
        /// <param name="password">The password.</param>
        /// <param name="roles">The roles.</param>
        /// <param name="clinicId">The clinic identifier.</param>
        /// <returns>
        ///   <c>true</c> if [is valid user] [the specified user]; otherwise, <c>false</c>.
        /// </returns>
        public bool IsValidUser(string user, string password, out string[] roles, out Guid clinicId)
        {
            roles = null;
            clinicId = Guid.Empty;

            ClinicStaff clinicStaff = MainContext.ClinicStaffs.Where(a => 
                a.Username.Equals(user, StringComparison.InvariantCultureIgnoreCase)).FirstOrDefault();

            if ((clinicStaff != null) && clinicStaff.Password.Equals(password))
            {
                roles = MapRoles(clinicStaff.Role);
                clinicId = clinicStaff.ClinicId;

                return (true);
            }

            return (false);
        }

        /// <summary>
        /// Determines whether [is valid user] [the specified user].
        /// </summary>
        /// <param name="user">The user.</param>
        /// <param name="roles">The roles.</param>
        /// <param name="clinicId">The clinic identifier.</param>
        /// <param name="clinicStaffId">The clinic staff identifier.</param>
        /// <returns>
        ///   <c>true</c> if [is valid user] [the specified user]; otherwise, <c>false</c>.
        /// </returns>
        public bool IsValidUser(string user, out string[] roles, out Guid clinicId, out Guid clinicStaffId)
        {
            roles = null;
            clinicId = Guid.Empty;
            clinicStaffId = Guid.Empty;

            ClinicStaff clinicStaff = MainContext.ClinicStaffs.Where(a =>
                (string.Compare(a.Username, user, StringComparison.InvariantCultureIgnoreCase) == 0)).FirstOrDefault();

            if (clinicStaff != null)
            {
                roles = MapRoles(clinicStaff.Role);
                clinicId = clinicStaff.ClinicId;
                clinicStaffId = clinicStaff.Id;
                return (true);
            }

            return (false);
        }

        /// <summary>
        /// Gets the main context.
        /// </summary>
        /// <value>
        /// The main context.
        /// </value>
        public MainContext MainContext
        {
            get { return Context as MainContext; }
        }

        /// <summary>
        /// Maps the roles.
        /// </summary>
        /// <param name="roles">The roles.</param>
        /// <returns></returns>
        /// <exception cref="Exception">A Clinic Staff Role has been defined that does not exist in ClinicStaffRepository.MapRoles()</exception>
        private string[] MapRoles(byte roles)
        {
            // TODO: Currently for demo and animal study only Admin role is supported.
            string[] returnRoles = null ;

            if ((roles & 0x04) == 0x04)
            {
                returnRoles = new string[1] { "Admin" };
            }
            else
            {
                throw new Exception("A Clinic Staff Role has been defined that does not exist in ClinicStaffRepository.MapRoles()");
            }

            return (returnRoles);
        }
    }
}

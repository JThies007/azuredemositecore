﻿// ************************************************************
// Copyright (C) 2018 Injectsense, Inc. - All Rights Reserved
// ************************************************************
using System;
using System.Collections.Generic;
using Injectsense.DataAccess.Entities;


namespace Injectsense.DataAccess.Repositories
{
    public class PlotPressureDataPoint
    {
        public DateTime DateValue;
        public Decimal PressureValue;
    }

    public enum PlotDataType
    {
        LeftSensorBaselinePlotData,
        LeftSensorPlotData,
        RightSensorBaselinePlotData,
        RightSensorPlotData
    }

    public interface IPatientRepository : IRepository<Patient>
    {
        Patient GetPatient(Guid PatientId);

        List<(Guid DeviceId, string ImplantEye, DateTime? ImplantDate)> GetImplantedDeviceInfo(Guid item1);

        (Guid DeviceId, string ImplantEye, DateTime? ImplantDate) GetImplantedDeviceInfo(string deviceSerialNumber);

        string GetPhysicianName(Guid PatientId);

        string GetDeviceModelName(Guid deviceId);

        void AssociateDeviceToPatient(Guid patientId, string serialNumber, string implantEye, DateTime? implantDate, int deviceStatus, Guid clinicStaffId);

        void UpdatePatientDeviceAssociation(Guid patientId, string serialNumber, string implantEye, DateTime? implantDate, int deviceStatus, Guid clinicStaffId);

        void RemoveDevice(Guid patientId, string serialNumber, int deviceStatus, Guid clinicStaffId);

        void SavePlotData(long transmissionId,
                          List<PlotPressureDataPoint> leftSensorPlotPoints,
                          List<PlotPressureDataPoint> rightSensorPlotPoints,
                          List<PlotPressureDataPoint> leftSensorBaselinePlotPoints,
                          List<PlotPressureDataPoint> rightSensorBaselinePlotPoints);

        List<PlotPressureDataPoint> GetPlotData(Guid patientId, DateTime startDate, DateTime stopDate, PlotDataType plotDataType);

        void UpdatePatientDeviceAssociation(Guid patientId, string nameFirst, string nameLast, string nameMiddle, 
                                            DateTime dateOfBirthNotUTC, string identifier, string phoneHome, string gender);

        Guid GetPatientFromDeviceId(Guid deviceId);
    }
}

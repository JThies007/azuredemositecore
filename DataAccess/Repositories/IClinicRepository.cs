﻿// ************************************************************
// Copyright (C) 2018 Injectsense, Inc. - All Rights Reserved
// ************************************************************
using System;
using System.Collections.Generic;
using Injectsense.DataAccess.Entities;

namespace Injectsense.DataAccess.Repositories
{
    public interface IClinicRepository : IRepository<Clinic>
    {
        List<ClinicPatientAssociation> GetAllPatientsWithNewTransmissionsForClinic(Guid clinicId);

        DateTime? GetLastOfficeVisitDate(Guid clinicId, Guid patientId);

        List<Patient> GetAllPatients(Guid clinicId);

        string GetClinicDotNetTimeZoneInfo(Guid clinicId);

        void AssociatePatient(Guid clinicId, Guid patientId, Guid clinicStaffId, Guid followingPhysician);

        ClinicPatientAssociation GetClinicPatientAssociation(Guid clinicId, Guid id);

        void UpdateMostRecentTransmissionSerialNumberInformation(Guid clinicId, Guid patientId, string serialNumber, string implantEye);

        void UpdateMostRecentPatientTransmissionInformation(long transmissionId, Guid patientId, string leftDeviceSerialNumber, string rightDeviceSerialNumber, DateTime receivedTimeUTC);
    }
}

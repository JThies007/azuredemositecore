﻿// ************************************************************
// Copyright (C) 2018 Injectsense, Inc. - All Rights Reserved
// ************************************************************
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Injectsense.DataAccess.Entities;

namespace Injectsense.DataAccess.Mapping
{
    /// <summary>
    /// Maps the entity Transmission to the database.
    /// </summary>
    public class TransmissionMap : IEntityTypeConfiguration<Transmission>
    {
        /// <summary>
        /// Configures the entity of type <typeparamref name="TEntity" />.
        /// </summary>
        /// <param name="builder">The builder to be used to configure the entity type.</param>
        public void Configure(EntityTypeBuilder<Transmission> builder)
        {
            // Primary Key
            builder.HasKey(t => t.Id);

            // Properties
            builder.Property(t => t.Id)
                .ValueGeneratedNever();

            builder.Property(t => t.ReceivedTimeUTC)
                .IsRequired();

            builder.Property(t => t.Package)
                .IsRequired();

            builder.Property(t => t.DeviceSerialNumber1)
                .IsRequired()
                .HasMaxLength(20);

            builder.Property(t => t.DeviceSerialNumber2)
                .HasMaxLength(20);

            builder.Property(t => t.PatientId)
                .IsRequired();

            builder.Property(t => t.TimeOfInterrogationUTC)
                .IsRequired();

            builder.Property(t => t.ReaderSerialNumber)
                .IsRequired();

            // Table & Column Mappings
            builder.ToTable("Transmission");
            builder.Property(t => t.Id).HasColumnName("Id");
            builder.Property(t => t.ReceivedTimeUTC).HasColumnName("ReceivedTimeUTC");
            builder.Property(t => t.Package).HasColumnName("Package");
            builder.Property(t => t.DeviceSerialNumber1).HasColumnName("DeviceSerialNumber1");
            builder.Property(t => t.DeviceSerialNumber2).HasColumnName("DeviceSerialNumber2");
            builder.Property(t => t.PatientId).HasColumnName("PatientId");
            builder.Property(t => t.TimeOfInterrogationUTC).HasColumnName("TimeOfInterrogationUTC");
            builder.Property(t => t.ReaderSerialNumber).HasColumnName("ReaderSerialNumber");
        }
    }
}
﻿// ************************************************************
// Copyright (C) 2018 Injectsense, Inc. - All Rights Reserved
// ************************************************************
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Injectsense.DataAccess.Entities;

namespace Injectsense.DataAccess.Mapping
{
    /// <summary>
    /// Maps the entity SupplementalPlotData to the database.
    /// </summary>
    class SupplementalPlotDataMap : IEntityTypeConfiguration<SupplementalPlotData>
    {
        /// <summary>
        /// Configures the entity of type <typeparamref name="TEntity" />.
        /// </summary>
        /// <param name="builder">The builder to be used to configure the entity type.</param>
        public void Configure(EntityTypeBuilder<SupplementalPlotData> builder)
        {
            // Primary Key
            builder.HasKey(t => t.Id);

            // Properties
            builder.Property(t => t.Id)
                .ValueGeneratedNever();

            builder.Property(t => t.TransmissionId)
                .IsRequired();

            builder.Property(t => t.EventDateTime)
                .IsRequired();

            builder.Property(t => t.SupplementalPlotItemTypeId)
                .IsRequired();

            // Table & Column Mappings
            builder.ToTable("SupplementalPlotData");
            builder.Property(t => t.EventDateTime).HasColumnName("EventDateTime");
            builder.Property(t => t.SupplementalPlotItemTypeId).HasColumnName("Value");
        }
    }
}

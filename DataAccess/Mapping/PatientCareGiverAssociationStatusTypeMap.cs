﻿// ************************************************************
// Copyright (C) 2018 Injectsense, Inc. - All Rights Reserved
// ************************************************************
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Injectsense.DataAccess.Entities;

namespace Injectsense.DataAccess.Mapping
{
    /// <summary>
    /// Maps the PatientCareGiverAssociationStatus entity to the database.
    /// </summary>
    /// <seealso cref="System.Data.Entity.ModelConfiguration.EntityTypeConfiguration{Injectsense.DataAccess.Entities.PatientCareGiverAssociationStatusType}" />
    class PatientCareGiverAssociationStatusTypeMap : IEntityTypeConfiguration<PatientCareGiverAssociationStatusType>
    {
        /// <summary>
        /// Configures the entity of type <typeparamref name="TEntity" />.
        /// </summary>
        /// <param name="builder">The builder to be used to configure the entity type.</param>
        public void Configure(EntityTypeBuilder<PatientCareGiverAssociationStatusType> builder)
        {
            // Primary Key
            builder.HasKey(t => t.Id);

            // Properties
            builder.Property(t => t.Id)
                .ValueGeneratedNever();

            builder.Property(t => t.Status)
                .IsRequired()
                .HasMaxLength(50);

            builder.Property(t => t.Description)
                .IsRequired()
                .HasMaxLength(100);

            builder.Property(t => t.UpdateRowTimestamp)
                .IsRequired()
                .IsFixedLength()
                .HasMaxLength(8)
                .IsRowVersion();

            // Table & Column Mappings
            builder.ToTable("PatientCareGiverAssociationStatusType");
            builder.Property(t => t.Id).HasColumnName("Id");
            builder.Property(t => t.Status).HasColumnName("Status");
            builder.Property(t => t.Description).HasColumnName("Description");
            builder.Property(t => t.UpdateRowTimestamp).HasColumnName("UpdateRowTimestamp");
        }
    }
}

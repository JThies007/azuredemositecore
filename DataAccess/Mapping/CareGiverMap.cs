﻿// ************************************************************
// Copyright (C) 2018 Injectsense, Inc. - All Rights Reserved
// ************************************************************
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Injectsense.DataAccess.Entities;

namespace Injectsense.DataAccess.Mapping
{
    /// <summary>
    /// Maps the entity CareGiver to database.
    /// </summary>
    /// <seealso cref="System.Data.Entity.ModelConfiguration.EntityTypeConfiguration{Injectsense.DataAccess.Entities.CareGiver}" />
    public class CareGiverMap : IEntityTypeConfiguration<CareGiver>
    {
        /// <summary>
        /// Configures the entity of type <typeparamref name="TEntity" />.
        /// </summary>
        /// <param name="builder">The builder to be used to configure the entity type.</param>
        public void Configure(EntityTypeBuilder<CareGiver> builder)
        {
            // Primary Key
            builder.HasKey(t => t.Id);

            // Properties
            builder.Property(t => t.Id)
                .ValueGeneratedNever();

            builder.Property(t => t.NamePrefix)
                .HasMaxLength(10);

            builder.Property(t => t.NameFirst)
                .IsRequired()
                .HasMaxLength(36);

            builder.Property(t => t.NameMiddle)
                .HasMaxLength(36);

            builder.Property(t => t.NameLast)
                .IsRequired()
                .HasMaxLength(36);

            builder.Property(t => t.NameSuffix)
                .HasMaxLength(16);

            builder.Property(t => t.PhoneHome)
                .HasMaxLength(20);

            builder.Property(t => t.PhoneMobile)
                .HasMaxLength(20);

            builder.Property(t => t.EmailAddress)
                .HasMaxLength(320);

            builder.Property(t => t.Username)
                .IsRequired()
                .HasMaxLength(20);

            builder.Property(t => t.LastUpdateTimeUTC)
                .IsRequired();

            builder.Property(t => t.LastUpdatedBy)
                .IsRequired();

            // Table & Column Mappings
            builder.ToTable("CareGiver");
            builder.Property(t => t.Id).HasColumnName("Id");
            builder.Property(t => t.NamePrefix).HasColumnName("NamePrefix");
            builder.Property(t => t.NameFirst).HasColumnName("NameFirst");
            builder.Property(t => t.NameMiddle).HasColumnName("NameMiddle");
            builder.Property(t => t.NameLast).HasColumnName("NameLast");
            builder.Property(t => t.NameSuffix).HasColumnName("NameSuffix");
            builder.Property(t => t.PhoneHome).HasColumnName("PhoneHome");
            builder.Property(t => t.PhoneMobile).HasColumnName("PhoneMobile");
            builder.Property(t => t.EmailAddress).HasColumnName("EmailAddress");
            builder.Property(t => t.Username).HasColumnName("Username");
            builder.Property(t => t.LastUpdateTimeUTC).HasColumnName("LastUpdateTimeUTC");
            builder.Property(t => t.LastUpdatedBy).HasColumnName("LastUpdatedBy");
        }
    }
}  

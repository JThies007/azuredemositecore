﻿// ************************************************************
// Copyright (C) 2018 Injectsense, Inc. - All Rights Reserved
// ************************************************************
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Injectsense.DataAccess.Entities;

namespace Injectsense.DataAccess.Mapping
{
    /// <summary>
    /// Maps the RightSensorPlotDataMap entity to the database.
    /// </summary>
    /// <seealso cref="System.Data.Entity.ModelConfiguration.EntityTypeConfiguration{Injectsense.DataAccess.Entities.RightSensorPlotData}" />
    class RightSensorPlotDataMap : IEntityTypeConfiguration<RightSensorPlotData>
    {
        /// <summary>
        /// Configures the entity of type <typeparamref name="TEntity" />.
        /// </summary>
        /// <param name="builder">The builder to be used to configure the entity type.</param>
        public void Configure(EntityTypeBuilder<RightSensorPlotData> builder)
        {
            // Primary Key
            builder.HasKey(t => t.Id);

            // Properties
            builder.Property(t => t.Id)
                .ValueGeneratedNever();

            builder.Property(t => t.TransmissionId)
                .IsRequired();

            builder.Property(t => t.EventDateTime)
                .IsRequired();

            builder.Property(t => t.Value)
                .IsRequired();

            // Table & Column Mappings
            builder.ToTable("RightSensorPlotData");
            builder.Property(t => t.Id).HasColumnName("Id");
            builder.Property(t => t.TransmissionId).HasColumnName("TransmissionId");
            builder.Property(t => t.EventDateTime).HasColumnName("EventDateTime");
            builder.Property(t => t.Value).HasColumnName("Value");
        }
    }
}

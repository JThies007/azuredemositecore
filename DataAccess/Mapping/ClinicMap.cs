﻿// ************************************************************
// Copyright (C) 2018 Injectsense, Inc. - All Rights Reserved
// ************************************************************
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Injectsense.DataAccess.Entities;

namespace Injectsense.DataAccess.Mapping
{
    /// <summary>
    /// Maps the entity Clinic to the database.
    /// </summary>
    public class ClinicMap : IEntityTypeConfiguration<Clinic>
    {
        /// <summary>
        /// Configures the entity of type <typeparamref name="TEntity" />.
        /// </summary>
        /// <param name="builder">The builder to be used to configure the entity type.</param>
        public void Configure(EntityTypeBuilder<Clinic> builder)
        {
            // Primary Key
            builder.HasKey(t => t.Id);

            // Properties
            builder.Property(t => t.Id)
                .ValueGeneratedNever();

            builder.Property(t => t.NamePrimary)
                .IsRequired()
                .HasMaxLength(128);

            builder.Property(t => t.NameSecondary)
                .HasMaxLength(128);

            builder.Property(t => t.ClinicContractReference)
                .IsRequired()
                .HasMaxLength(64);

            builder.Property(t => t.AddressLine1)
                .HasMaxLength(80);

            builder.Property(t => t.AddressLine2)
                .HasMaxLength(80);

            builder.Property(t => t.AddressLine3)
                .HasMaxLength(80);

            builder.Property(t => t.AddressLine4)
                .HasMaxLength(80);

            builder.Property(t => t.City)
                .HasMaxLength(50);

            builder.Property(t => t.StateCode)
                .HasMaxLength(3);

            builder.Property(t => t.Zipcode)
                .HasMaxLength(10);

            builder.Property(t => t.PostOfficeNumber)
                .HasMaxLength(25);

            builder.Property(t => t.PrimaryPhone)
                .IsRequired()
                .HasMaxLength(20);

            builder.Property(t => t.SecondaryPhone)
                .HasMaxLength(20);

            builder.Property(t => t.FAX)
                .HasMaxLength(20);

            builder.Property(t => t.BillingAddressLine1)
                .HasMaxLength(80);

            builder.Property(t => t.BillingAddressLine2)
                .HasMaxLength(80);

            builder.Property(t => t.BillingAddressLine3)
                .HasMaxLength(80);

            builder.Property(t => t.BillingAddressLine4)
                .HasMaxLength(80);

            builder.Property(t => t.BillingCity)
                .HasMaxLength(40);

            builder.Property(t => t.BillingStateCode)
                .HasMaxLength(3);

            builder.Property(t => t.Zipcode)
                .HasMaxLength(10);

            builder.Property(t => t.BillingAddressSameAsClinic)
                .IsRequired();

            builder.Property(t => t.PrimaryContactFirstName)
                .IsRequired()
                .HasMaxLength(40);

            builder.Property(t => t.PrimaryContactLastName)
                .IsRequired()
                .HasMaxLength(40);

            builder.Property(t => t.PrimaryContactEmailAddress)
                .HasMaxLength(320);

            builder.Property(t => t.PrimaryAdminId);

            builder.Property(t => t.SecondaryAdminId);

            builder.Property(t => t.ClinicEmailAddress)
                .HasMaxLength(320);

            builder.Property(t => t.LastUpdateTimeUTC)
                .IsRequired();

            builder.Property(t => t.LastUpdatedBy)
                .IsRequired();

            builder.Property(t => t.TimeZoneId)
                .IsRequired();

            builder.Property(t => t.CountryCodeId)
                .IsRequired();

            builder.Property(t => t.LanguageId)
                .IsRequired();

            builder.Property(t => t.ClinicStatusTypeId)
                .IsRequired();

            builder.Property(t => t.ParentClinicId);

            // Table & Column Mappings
            builder.ToTable("Clinic");
            builder.Property(t => t.Id).HasColumnName("Id");
            builder.Property(t => t.NamePrimary).HasColumnName("NamePrimary");
            builder.Property(t => t.NameSecondary).HasColumnName("NameSecondary");
            builder.Property(t => t.ClinicContractReference).HasColumnName("ClinicContractReference");
            builder.Property(t => t.AddressLine1).HasColumnName("AddressLine1");
            builder.Property(t => t.AddressLine2).HasColumnName("AddressLine2");
            builder.Property(t => t.AddressLine3).HasColumnName("AddressLine3");
            builder.Property(t => t.AddressLine4).HasColumnName("AddressLine4");
            builder.Property(t => t.City).HasColumnName("City");
            builder.Property(t => t.StateCode).HasColumnName("StateCode");
            builder.Property(t => t.Zipcode).HasColumnName("Zipcode");
            builder.Property(t => t.PostOfficeNumber).HasColumnName("PostOfficeNumber");
            builder.Property(t => t.PrimaryPhone).HasColumnName("PrimaryPhone");
            builder.Property(t => t.SecondaryPhone).HasColumnName("SecondaryPhone");
            builder.Property(t => t.FAX).HasColumnName("FAX");
            builder.Property(t => t.BillingAddressLine1).HasColumnName("BillingAddressLine1");
            builder.Property(t => t.BillingAddressLine2).HasColumnName("BillingAddressLine2");
            builder.Property(t => t.BillingAddressLine3).HasColumnName("BillingAddressLine3");
            builder.Property(t => t.BillingAddressLine4).HasColumnName("BillingAddressLine4");
            builder.Property(t => t.BillingCity).HasColumnName("BillingCity");
            builder.Property(t => t.BillingStateCode).HasColumnName("BillingStateCode");
            builder.Property(t => t.Zipcode).HasColumnName("Zipcode");
            builder.Property(t => t.BillingAddressSameAsClinic).HasColumnName("BillingAddressSameAsClinic");
            builder.Property(t => t.PrimaryContactFirstName).HasColumnName("PrimaryContactFirstName");
            builder.Property(t => t.PrimaryContactLastName).HasColumnName("PrimaryContactLastName");
            builder.Property(t => t.PrimaryContactEmailAddress).HasColumnName("PrimaryContactEmailAddress");
            builder.Property(t => t.PrimaryAdminId).HasColumnName("PrimaryAdminId");
            builder.Property(t => t.SecondaryAdminId).HasColumnName("SecondaryAdminId");
            builder.Property(t => t.ClinicEmailAddress).HasColumnName("ClinicEmailAddress");
            builder.Property(t => t.LastUpdateTimeUTC).HasColumnName("LastUpdateTimeUTC");
            builder.Property(t => t.LastUpdatedBy).HasColumnName("LastUpdatedBy");
            builder.Property(t => t.TimeZoneId).HasColumnName("TimeZoneId");
            builder.Property(t => t.CountryCodeId).HasColumnName("CountryCodeId");
            builder.Property(t => t.LanguageId).HasColumnName("LanguageId");
            builder.Property(t => t.ClinicStatusTypeId).HasColumnName("ClinicStatusTypeId");
            builder.Property(t => t.ParentClinicId).HasColumnName("ParentClinicId");
        }
    }
} 


﻿// ************************************************************
// Copyright (C) 2018 Injectsense, Inc. - All Rights Reserved
// ************************************************************
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Injectsense.DataAccess.Entities;

namespace Injectsense.DataAccess.Mapping
{
    /// <summary>
    /// Maps the Language entity to the database.
    /// </summary>
    public class LanguageMap : IEntityTypeConfiguration<Language>
    {
        /// <summary>
        /// Configures the entity of type <typeparamref name="TEntity" />.
        /// </summary>
        /// <param name="builder">The builder to be used to configure the entity type.</param>
        public void Configure(EntityTypeBuilder<Language> builder)
        {
            // Primary Key
            builder.HasKey(t => t.Id);

            // Properties
            builder.Property(t => t.Id)
                .ValueGeneratedNever();

            builder.Property(t => t.Description)
                .IsRequired()
                .HasMaxLength(50);

            builder.Property(t => t.DisplayString)
                .IsRequired()
                .HasMaxLength(50);

            builder.Property(t => t.IsDisplayed)
                .IsRequired();

            builder.Property(t => t.UpdateRowTimestamp)
                .IsRequired()
                .IsFixedLength()
                .HasMaxLength(8)
                .IsRowVersion();

            // Table & Column Mappings
            builder.ToTable("Language");
            builder.Property(t => t.Id).HasColumnName("Id");
            builder.Property(t => t.Description).HasColumnName("Description");
            builder.Property(t => t.DisplayString).HasColumnName("DisplayString");
            builder.Property(t => t.IsDisplayed).HasColumnName("IsDisplayed");
            builder.Property(t => t.UpdateRowTimestamp).HasColumnName("UpdateRowTimestamp");
        }
    }
}

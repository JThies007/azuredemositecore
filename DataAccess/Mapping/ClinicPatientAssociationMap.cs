﻿// ************************************************************
// Copyright (C) 2018 Injectsense, Inc. - All Rights Reserved
// ************************************************************
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Injectsense.DataAccess.Entities;

namespace Injectsense.DataAccess.Mapping
{
    /// <summary>
    /// Maps the entity ClinicPatientAssociation to the database.
    /// </summary>
    /// <seealso cref="System.Data.Entity.ModelConfiguration.EntityTypeConfiguration{Injectsense.DataAccess.Entities.ClinicPatientAssociation}" />
    public class ClinicPatientAssociationMap : IEntityTypeConfiguration<ClinicPatientAssociation>
    {
        /// <summary>
        /// Configures the entity of type <typeparamref name="TEntity" />.
        /// </summary>
        /// <param name="builder">The builder to be used to configure the entity type.</param>
        public void Configure(EntityTypeBuilder<ClinicPatientAssociation> builder)
        {
            // Primary Key
            builder.HasKey(t => t.Id);

            // Properties
            builder.Property(t => t.Id)
                .ValueGeneratedNever();

            builder.Property(t => t.ClinicId)
                .IsRequired();

            builder.Property(t => t.PatientId)
                .IsRequired();

            builder.Property(t => t.PatientDiscontinueReasonTypeId)
                .IsRequired();

            builder.Property(t => t.MostRecentTransmissionId);

            builder.Property(t => t.NewTransmissionCount);

            builder.Property(t => t.ViewableTransmissionCount);

            builder.Property(t => t.AssociationCreateDateUTC)
                .IsRequired();

            builder.Property(t => t.AssociationRemovalDateUTC);

            builder.Property(t => t.ClinicPatientAssociationStatusTypeId)
                .IsRequired();

            builder.Property(t => t.LastUpdateTimeUTC)
                .IsRequired();

            builder.Property(t => t.LastUpdatedBy)
                .IsRequired();

            builder.Property(t => t.LastOfficeVisitDateUTC);

            builder.Property(t => t.MostRecentTransmissionLeftEyeDSN)
                .HasMaxLength(20);

            builder.Property(t => t.MostRecentTransmissionRightEyeDSN)
                .HasMaxLength(20);

            builder.Property(t => t.MostRecentTransmissionReceivedDateUTC);

            builder.Property(t => t.Physician)
                .IsRequired();

            // Table & Column Mappings
            builder.ToTable("ClinicPatientAssociation");
            builder.Property(t => t.Id).HasColumnName("Id");
            builder.Property(t => t.ClinicId).HasColumnName("ClinicId");
            builder.Property(t => t.PatientId).HasColumnName("PatientId");
            builder.Property(t => t.PatientDiscontinueReasonTypeId).HasColumnName("PatientDiscontinueReasonTypeId");
            builder.Property(t => t.MostRecentTransmissionId).HasColumnName("MostRecentTransmissionId");
            builder.Property(t => t.NewTransmissionCount).HasColumnName("NewTransmissionCount");
            builder.Property(t => t.ViewableTransmissionCount).HasColumnName("ViewableTransmissionCount");
            builder.Property(t => t.AssociationCreateDateUTC).HasColumnName("AssociationCreateDateUTC");
            builder.Property(t => t.AssociationRemovalDateUTC).HasColumnName("AssociationRemovalDateUTC");
            builder.Property(t => t.ClinicPatientAssociationStatusTypeId).HasColumnName("ClinicPatientAssociationStatusTypeId");
            builder.Property(t => t.LastUpdateTimeUTC).HasColumnName("LastUpdateTimeUTC");
            builder.Property(t => t.LastUpdatedBy).HasColumnName("LastUpdatedBy");
            builder.Property(t => t.LastOfficeVisitDateUTC).HasColumnName("LastOfficeVisitDateUTC");
            builder.Property(t => t.MostRecentTransmissionLeftEyeDSN).HasColumnName("MostRecentTransmissionLeftEyeDSN");
            builder.Property(t => t.MostRecentTransmissionRightEyeDSN).HasColumnName("MostRecentTransmissionRightEyeDSN");
            builder.Property(t => t.MostRecentTransmissionReceivedDateUTC).HasColumnName("MostRecentTransmissionReceivedDateUTC");
            builder.Property(t => t.Physician).HasColumnName("Physician");
        }
    }
}

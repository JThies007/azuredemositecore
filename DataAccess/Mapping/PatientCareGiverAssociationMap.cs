﻿// ************************************************************
// Copyright (C) 2018 Injectsense, Inc. - All Rights Reserved
// ************************************************************
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Injectsense.DataAccess.Entities;

namespace Injectsense.DataAccess.Mapping
{
    /// <summary>
    /// Maps the PatientCareGiverAssociation entity to the database.
    /// </summary>
    public class PatientCareGiverAssociationMap : IEntityTypeConfiguration<PatientCareGiverAssociation>
    {
        /// <summary>
        /// Configures the entity of type <typeparamref name="TEntity" />.
        /// </summary>
        /// <param name="builder">The builder to be used to configure the entity type.</param>
        public void Configure(EntityTypeBuilder<PatientCareGiverAssociation> builder)
        {
            // Primary Key
            builder.HasKey(t => t.Id);

            // Properties
            builder.Property(t => t.Id)
                .ValueGeneratedNever();

            builder.Property(t => t.PatientId)
                .IsRequired();

            builder.Property(t => t.CareGiverId)
                .IsRequired();

            builder.Property(t => t.CareGiverDiscontinueReasonTypeId);

            builder.Property(t => t.AssociationCreateDateUTC);

            builder.Property(t => t.AssociationRemovalDateUTC)
                .IsRequired();

            builder.Property(t => t.PatientCareGiverAssociationStatusTypeId)
                .IsRequired();

            builder.Property(t => t.LastUpdateTimeUTC)
                .IsRequired();

            builder.Property(t => t.LastUpdatedBy)
                .IsRequired();

            // Table & Column Mappings
            builder.ToTable("PatientCareGiverAssociation");
            builder.Property(t => t.Id).HasColumnName("Id");
            builder.Property(t => t.PatientId).HasColumnName("PatientId");
            builder.Property(t => t.CareGiverId).HasColumnName("CareGiverId");
            builder.Property(t => t.CareGiverDiscontinueReasonTypeId).HasColumnName("CareGiverDiscontinueReasonTypeId");
            builder.Property(t => t.AssociationCreateDateUTC).HasColumnName("AssociationCreateDateUTC");
            builder.Property(t => t.AssociationRemovalDateUTC).HasColumnName("AssociationRemovalDateUTC");
            builder.Property(t => t.PatientCareGiverAssociationStatusTypeId).HasColumnName("PatientCareGiverAssociationStatusTypeId");
            builder.Property(t => t.LastUpdateTimeUTC).HasColumnName("LastUpdateTimeUTC");
            builder.Property(t => t.LastUpdatedBy).HasColumnName("LastUpdatedBy");
        }
    }
}


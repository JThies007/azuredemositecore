﻿// ************************************************************
// Copyright (C) 2018 Injectsense, Inc. - All Rights Reserved
// ************************************************************
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Injectsense.DataAccess.Entities;

namespace Injectsense.DataAccess.Mapping
{
    /// <summary>
    /// Maps the entity TimeZone to the database.
    /// </summary>
    public class TimeZoneMap : IEntityTypeConfiguration<TimeZone>
    {
        /// <summary>
        /// Configures the entity of type <typeparamref name="TEntity" />.
        /// </summary>
        /// <param name="builder">The builder to be used to configure the entity type.</param>
        public void Configure(EntityTypeBuilder<TimeZone> builder)
        {
            // Primary Key
            builder.HasKey(t => t.Id);

            // Properties
            builder.Property(t => t.Id)
                .ValueGeneratedNever();

            builder.Property(t => t.CountryCode)
                .IsRequired()
                .HasMaxLength(3);

            builder.Property(t => t.Coordinates)
                .HasMaxLength(20);

            builder.Property(t => t.Name)
                .IsRequired()
                .HasMaxLength(50);

            builder.Property(t => t.DisplayString)
                .IsRequired()
                .HasMaxLength(100);

            builder.Property(t => t.DisplayOrder)
                .IsRequired();

            builder.Property(t => t.IsDisplayed)
                .IsRequired();

            builder.Property(t => t.OffsetFromGMTHours)
                .IsRequired();

            builder.Property(t => t.OffsetFromGMTHoursDST)
                .IsRequired();

            builder.Property(t => t.UpdateRowTimestamp)
                .IsRequired()
                .IsFixedLength()
                .HasMaxLength(8)
                .IsRowVersion();

            builder.Property(t => t.DotNetTimeZoneInfo)
                .HasMaxLength(100);

            // Table & Column Mappings
            builder.ToTable("TimeZone");
            builder.Property(t => t.Id).HasColumnName("Id");
            builder.Property(t => t.CountryCode).HasColumnName("CountryCode");
            builder.Property(t => t.Coordinates).HasColumnName("Coordinates");
            builder.Property(t => t.Name).HasColumnName("Name");
            builder.Property(t => t.DisplayString).HasColumnName("DisplayString");
            builder.Property(t => t.DisplayOrder).HasColumnName("DisplayOrder");
            builder.Property(t => t.IsDisplayed).HasColumnName("IsDisplayed");
            builder.Property(t => t.OffsetFromGMTHours).HasColumnName("OffsetFromGMTHours");
            builder.Property(t => t.OffsetFromGMTHoursDST).HasColumnName("OffsetFromGMTHoursDST");
            builder.Property(t => t.UpdateRowTimestamp).HasColumnName("UpdateRowTimestamp");
            builder.Property(t => t.DotNetTimeZoneInfo).HasColumnName("DotNetTimeZoneInfo");
        }
    }
}

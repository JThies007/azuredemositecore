﻿// ************************************************************
// Copyright (C) 2018 Injectsense, Inc. - All Rights Reserved
// ************************************************************
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Injectsense.DataAccess.Entities;

namespace Injectsense.DataAccess.Mapping
{
    /// <summary>
    /// Maps the entity Device to the database.
    /// </summary>
    public class DeviceMap : IEntityTypeConfiguration<Device>
    {
        /// <summary>
        /// Configures the entity of type <typeparamref name="TEntity" />.
        /// </summary>
        /// <param name="builder">The builder to be used to configure the entity type.</param>
        public void Configure(EntityTypeBuilder<Device> builder)
        {
            // Primary Key
            builder.HasKey(t => t.Id);

            // Properties
            builder.Property(t => t.Id)
                .ValueGeneratedNever();

            builder.Property(t => t.HardwareVersion)
                .IsRequired();

            builder.Property(t => t.CalCoeffA)
                .IsRequired();

            builder.Property(t => t.CalCoeffB)
                .IsRequired();

            builder.Property(t => t.CalCoeffC)
                .IsRequired();

            builder.Property(t => t.ManufactureDate)
                .IsRequired();

            builder.Property(t => t.DeviceTypeId)
                .IsRequired();

            builder.Property(t => t.DateAddedUTC)
                .IsRequired();

            builder.Property(t => t.IsAvailableForUse)
                .IsRequired();

            builder.Property(t => t.UniqueManufacturingIdentifier)
                .IsRequired();

            // Table & Column Mappings
            builder.ToTable("Device");
            builder.Property(t => t.Id).HasColumnName("Id");
            builder.Property(t => t.HardwareVersion).HasColumnName("HardwareVersion");
            builder.Property(t => t.CalCoeffA).HasColumnName("CalCoeffA");
            builder.Property(t => t.CalCoeffB).HasColumnName("CalCoeffB");
            builder.Property(t => t.CalCoeffC).HasColumnName("CalCoeffC");
            builder.Property(t => t.ManufactureDate).HasColumnName("ManufactureDate");
            builder.Property(t => t.DeviceTypeId).HasColumnName("DeviceTypeId");
            builder.Property(t => t.DateAddedUTC).HasColumnName("DateAddedUTC");
            builder.Property(t => t.IsAvailableForUse).HasColumnName("IsAvailableForUse");
            builder.Property(t => t.UniqueManufacturingIdentifier).HasColumnName("UniqueManufacturingIdentifer");
        }
    }
}

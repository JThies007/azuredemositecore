﻿// ************************************************************
// Copyright (C) 2018 Injectsense, Inc. - All Rights Reserved
// ************************************************************
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Injectsense.DataAccess.Entities;

namespace Injectsense.DataAccess.Mapping
{
    /// <summary>
    /// Maps the entity PatientDeviceAssociation to the database.
    /// </summary>
    class PatientDeviceAssociationMap : IEntityTypeConfiguration<PatientDeviceAssociation>
    {
        /// <summary>
        /// Configures the entity of type <typeparamref name="TEntity" />.
        /// </summary>
        /// <param name="builder">The builder to be used to configure the entity type.</param>
        public void Configure(EntityTypeBuilder<PatientDeviceAssociation> builder)
        {
            // Primary Key
            builder.HasKey(t => t.Id);

            // Properties
            builder.Property(t => t.Id)
                .ValueGeneratedNever();

            builder.Property(t => t.PatientId)
                .IsRequired();

            builder.Property(t => t.DeviceId)
                .IsRequired();

            builder.Property(t => t.ImplantEye)
                .HasMaxLength(1);

            builder.Property(t => t.ImplantDateNotUTC);

            builder.Property(t => t.ExplantDateNotUTC);

            builder.Property(t => t.DisabledDateNotUTC);

            builder.Property(t => t.DeviceStatusTypeId)
                .IsRequired();

            builder.Property(t => t.LastUpdateTimeUTC)
                .IsRequired();

            builder.Property(t => t.LastUpdatedBy)
                .IsRequired();

            // Table & Column Mappings
            builder.ToTable("PatientDeviceAssociation");
            builder.Property(t => t.Id).HasColumnName("Id");
            builder.Property(t => t.PatientId).HasColumnName("PatientId");
            builder.Property(t => t.DeviceId).HasColumnName("DeviceId");
            builder.Property(t => t.ImplantEye).HasColumnName("ImplantEye");
            builder.Property(t => t.ImplantDateNotUTC).HasColumnName("ImplantDateNotUTC");
            builder.Property(t => t.ExplantDateNotUTC).HasColumnName("ExplantDateNotUTC");
            builder.Property(t => t.DisabledDateNotUTC).HasColumnName("DisabledDateNotUTC");
            builder.Property(t => t.DeviceStatusTypeId).HasColumnName("DeviceStatusTypeId");
            builder.Property(t => t.LastUpdateTimeUTC).HasColumnName("lastUpdateTimeUTC");
            builder.Property(t => t.LastUpdatedBy).HasColumnName("LastUpdatedBy");
        }
    }
}
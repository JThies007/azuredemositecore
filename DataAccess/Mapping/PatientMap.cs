﻿// ************************************************************
// Copyright (C) 2018 Injectsense, Inc. - All Rights Reserved
// ************************************************************
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Injectsense.DataAccess.Entities;

namespace Injectsense.DataAccess.Mapping
{
    /// <summary>
    /// Maps the entity Patient to the database.
    /// </summary>
    public class PatientMap : IEntityTypeConfiguration<Patient>
    {
        /// <summary>
        /// Configures the entity of type <typeparamref name="TEntity" />.
        /// </summary>
        /// <param name="builder">The builder to be used to configure the entity type.</param>
        public void Configure(EntityTypeBuilder<Patient> builder)
        {
            // Primary Key
            builder.HasKey(t => t.Id);

            // Properties
            builder.Property(t => t.Id)
                .ValueGeneratedNever();

            builder.Property(t => t.Identifier)
                .HasMaxLength(50);

            builder.Property(t => t.ApplicationUsername)
                .IsRequired()
                .HasMaxLength(20);

            builder.Property(t => t.HasReader)
                .IsRequired();

            builder.Property(t => t.NamePrefix)
                .HasMaxLength(10);

            builder.Property(t => t.NameFirst)
                .IsRequired()
                .HasMaxLength(36);

            builder.Property(t => t.NameMiddle)
                .HasMaxLength(36);

            builder.Property(t => t.NameLast)
                .IsRequired()
                .HasMaxLength(36);

            builder.Property(t => t.NameSuffix)
                .HasMaxLength(16);

            builder.Property(t => t.Gender)
                .IsRequired()
                .HasMaxLength(1);

            builder.Property(t => t.DateOfBirthNotUTC)
                .IsRequired();

            builder.Property(t => t.AddressLine1)
                .HasMaxLength(80);

            builder.Property(t => t.AddressLine2)
                .HasMaxLength(80);

            builder.Property(t => t.AddressLine3)
                .HasMaxLength(80);

            builder.Property(t => t.AddressLine4)
                .HasMaxLength(80);

            builder.Property(t => t.City)
                .HasMaxLength(50);

            builder.Property(t => t.StateCode)
                .HasMaxLength(3);

            builder.Property(t => t.Zipcode)
                .HasMaxLength(10);

            builder.Property(t => t.PostOfficeNumber)
                .HasMaxLength(25);

            builder.Property(t => t.PhoneHome)
                .HasMaxLength(20);

            builder.Property(t => t.PhoneMobile)
                .HasMaxLength(20);

            builder.Property(t => t.PhoneWorkNumber)
                .HasMaxLength(20);

            builder.Property(t => t.PhoneWorkExtension)
                .HasMaxLength(10);

            builder.Property(t => t.PhoneOtherNumber)
                .HasMaxLength(20);

            builder.Property(t => t.PhoneOtherDescription)
                .HasMaxLength(20);

            builder.Property(t => t.PrimaryEmail)
                .HasMaxLength(320);

            builder.Property(t => t.SecondaryEmail)
                .HasMaxLength(320);

            builder.Property(t => t.LastUpdateTimeUTC)
                .IsRequired();

            builder.Property(t => t.LastUpdatedBy)
                .IsRequired();

            // Table & Column Mappings
            builder.ToTable("Patient");
            builder.Property(t => t.Id).HasColumnName("Id");
            builder.Property(t => t.Identifier).HasColumnName("Identifier");
            builder.Property(t => t.ApplicationUsername).HasColumnName("ApplicationUserName");
            builder.Property(t => t.HasReader).HasColumnName("HasReader");
            builder.Property(t => t.NamePrefix).HasColumnName("NamePrefix");
            builder.Property(t => t.NameFirst).HasColumnName("NameFirst");
            builder.Property(t => t.NameMiddle).HasColumnName("NameMiddle");
            builder.Property(t => t.NameLast).HasColumnName("NameLast");
            builder.Property(t => t.NameSuffix).HasColumnName("NameSuffix");
            builder.Property(t => t.Gender).HasColumnName("Gender");
            builder.Property(t => t.DateOfBirthNotUTC).HasColumnName("DateOfBirthNotUTC");
            builder.Property(t => t.AddressLine1).HasColumnName("AddressLine1");
            builder.Property(t => t.AddressLine2).HasColumnName("AddressLine2");
            builder.Property(t => t.AddressLine3).HasColumnName("AddressLine3");
            builder.Property(t => t.AddressLine4).HasColumnName("AddressLine4");
            builder.Property(t => t.City).HasColumnName("City");
            builder.Property(t => t.StateCode).HasColumnName("StateCode");
            builder.Property(t => t.Zipcode).HasColumnName("Zipcode");
            builder.Property(t => t.PostOfficeNumber).HasColumnName("PostOfficeNumber");
            builder.Property(t => t.PhoneHome).HasColumnName("PhoneHome");
            builder.Property(t => t.PhoneMobile).HasColumnName("PhoneMobile");
            builder.Property(t => t.PhoneWorkNumber).HasColumnName("PhoneWorkNumber");
            builder.Property(t => t.PhoneWorkExtension).HasColumnName("PhoneWorkExtension");
            builder.Property(t => t.PhoneOtherNumber).HasColumnName("PhoneOtherNumber");
            builder.Property(t => t.PhoneOtherDescription).HasColumnName("PhoneOtherDescription");
            builder.Property(t => t.PrimaryEmail).HasColumnName("PrimaryEmail");
            builder.Property(t => t.SecondaryEmail).HasColumnName("SecondaryEmail");
            builder.Property(t => t.LastUpdateTimeUTC).HasColumnName("LastUpdateTimeUTC");
            builder.Property(t => t.LastUpdatedBy).HasColumnName("LastUpdatedBy");
        }
    }
} 


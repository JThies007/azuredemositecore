﻿// ************************************************************
// Copyright (C) 2018 Injectsense, Inc. - All Rights Reserved
// ************************************************************
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Injectsense.DataAccess.Entities;

namespace Injectsense.DataAccess.Mapping
{
    /// <summary>
    /// Maps the entity CareGiverDiscontinueReasonType to the database.
    /// </summary>
    public class CareGiverDiscontinueReasonTypeMap : IEntityTypeConfiguration<CareGiverDiscontinueReasonType>
    {
        /// <summary>
        /// Configures the entity of type <typeparamref name="TEntity" />.
        /// </summary>
        /// <param name="builder">The builder to be used to configure the entity type.</param>
        public void Configure(EntityTypeBuilder<CareGiverDiscontinueReasonType> builder)
        {
            // Primary Key
            builder.HasKey(t => t.Id);

            // Properties
            builder.Property(t => t.Id)
                .ValueGeneratedNever();

            builder.Property(t => t.Reason)
                .IsRequired()
                .HasMaxLength(50);

            builder.Property(t => t.Description)
                .IsRequired()
                .HasMaxLength(100);

            builder.Property(t => t.UpdateRowTimestamp)
                .IsRequired()
                .IsFixedLength()
                .HasMaxLength(8)
                .IsRowVersion();

            // Table & Column Mappings
            builder.ToTable("CareGiverDiscontinueReasonType");
            builder.Property(t => t.Id).HasColumnName("Id");
            builder.Property(t => t.Reason).HasColumnName("Reason");
            builder.Property(t => t.Description).HasColumnName("Description");
            builder.Property(t => t.UpdateRowTimestamp).HasColumnName("UpdateRowTimestamp");
        }
    }
}
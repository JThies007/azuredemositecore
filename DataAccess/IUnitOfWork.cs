﻿// ************************************************************
// Copyright (C) 2018 Injectsense, Inc. - All Rights Reserved
// ************************************************************
using System;
using Injectsense.DataAccess.Repositories;

namespace Injectsense.DataAccess
{
    public interface IUnitOfWork : IDisposable
    {
        IClinicRepository Clinics { get; }
        IPatientRepository Patients { get; }
        IDeviceRepository Devices { get; }
        IClinicStaffRepository ClinicStaffs { get; }
        ITransmissionRepository Transmissions { get; }
        int Commit();
    }
}

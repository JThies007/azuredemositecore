﻿// ************************************************************
// Copyright (C) 2018 Injectsense, Inc. - All Rights Reserved
// ************************************************************
using Injectsense.DataAccess.Entities;
using Injectsense.DataAccess.Mapping;
using Microsoft.EntityFrameworkCore;

namespace Injectsense.DataAccess
{
    public partial class MainContext : DbContext
    {

        public MainContext(DbContextOptions<MainContext> options)
            : base(options)
        //public MainContext()
        //    : base()
        {
        }

        public DbSet<Clinic> Clinics { get; set; }
        public DbSet<TimeZone> TimeZones { get; set; }
        public DbSet<ClinicStatusType> ClinicStatusTypes { get; set; }
        public DbSet<ClinicStaff> ClinicStaffs { get; set; }
        public DbSet<Language> Languages { get; set; }
        public DbSet<ClinicPatientAssociationStatusType> ClinicPatientAssociationStatusTypes { get; set; }
        public DbSet<ClinicPatientAssociation> ClinicPatientAssociations { get; set; }
        public DbSet<PatientDiscontinueReasonType> PatientDiscontinueReasonTypes { get; set; }
        public DbSet<Patient> Patients { get; set; }
        public DbSet<CountryCode> CountryCodes { get; set; }
        public DbSet<CareGiverDiscontinueReasonType> CareGiverDiscontinueReasonTypes { get; set; }
        public DbSet<CareGiver> CareGivers { get; set; }
        public DbSet<PatientCareGiverAssociation> PatientCareGiverAssociations { get; set; }
        public DbSet<PatientDeviceAssociation> PatientDeviceAssociations { get; set; }
        public DbSet<DeviceStatusType> DeviceStatusTypes { get; set; }
        public DbSet<PatientCareGiverAssociationStatusType> PatientCareGiverAssociationStatusTypes { get; set; }
        public DbSet<DeviceType> DeviceTypes { get; set; }
        public DbSet<Device> Devices { get; set; }
        public DbSet<Transmission> Transmissions { get; set; }
        public DbSet<LeftSensorPlotData> LeftSensorPlotDatas { get; set; }
        public DbSet<LeftSensorBaselinePlotData> LeftSensorBaselinePlotDatas { get; set; }
        public DbSet<RightSensorPlotData> RightSensorPlotDatas { get; set; }
        public DbSet<RightSensorBaselinePlotData> RightSensorBaselinePlotDatas { get; set; }
        public DbSet<SupplementalPlotData> SupplementalPlotDatas { get; set; }
        public DbSet<SupplementalPlotItemType> SupplementalPlotItemTypes { get; set; }


        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            ClinicMap clinicMap = new ClinicMap();
            clinicMap.Configure(modelBuilder.Entity<Clinic>());

            TimeZoneMap timezoneMap = new TimeZoneMap();
            timezoneMap.Configure(modelBuilder.Entity<TimeZone>());

            ClinicStatusTypeMap clinicStatusTypeMap = new ClinicStatusTypeMap();
            clinicStatusTypeMap.Configure(modelBuilder.Entity<ClinicStatusType>());

            ClinicStaffMap clinicStaffMap = new ClinicStaffMap();
            clinicStaffMap.Configure(modelBuilder.Entity<ClinicStaff>());

            LanguageMap languageMap = new LanguageMap();
            languageMap.Configure(modelBuilder.Entity<Language>());

            ClinicPatientAssociationStatusTypeMap clinicPatientAssociationStatusTypeMap = new ClinicPatientAssociationStatusTypeMap();
            clinicPatientAssociationStatusTypeMap.Configure(modelBuilder.Entity<ClinicPatientAssociationStatusType>());

            ClinicPatientAssociationMap clinicPatientAssociationMap = new ClinicPatientAssociationMap();
            clinicPatientAssociationMap.Configure(modelBuilder.Entity<ClinicPatientAssociation>());

            PatientDiscontinueReasonTypeMap patientDiscontinueReasonTypeMap = new PatientDiscontinueReasonTypeMap();
            patientDiscontinueReasonTypeMap.Configure(modelBuilder.Entity<PatientDiscontinueReasonType>());

            PatientMap patientMap = new PatientMap();
            patientMap.Configure(modelBuilder.Entity<Patient>());

            CountryCodeMap countryCodeMap = new CountryCodeMap();
            countryCodeMap.Configure(modelBuilder.Entity<CountryCode>());

            CareGiverDiscontinueReasonTypeMap careGiverDiscontinueReasonTypeMap = new CareGiverDiscontinueReasonTypeMap();
            careGiverDiscontinueReasonTypeMap.Configure(modelBuilder.Entity<CareGiverDiscontinueReasonType>());

            PatientCareGiverAssociationMap patientCareGiverAssociationMap = new PatientCareGiverAssociationMap();
            patientCareGiverAssociationMap.Configure(modelBuilder.Entity<PatientCareGiverAssociation>());

            CareGiverMap careGiverMap = new CareGiverMap();
            careGiverMap.Configure(modelBuilder.Entity<CareGiver>());

            PatientDeviceAssociationMap patientDeviceAssociationMap = new PatientDeviceAssociationMap();
            patientDeviceAssociationMap.Configure(modelBuilder.Entity<PatientDeviceAssociation>());

            DeviceStatusTypeMap deviceStatusTypeMap = new DeviceStatusTypeMap();
            deviceStatusTypeMap.Configure(modelBuilder.Entity<DeviceStatusType>());

            PatientCareGiverAssociationStatusTypeMap patientCareGiverAssociationStatusTypeMap = new PatientCareGiverAssociationStatusTypeMap();
            patientCareGiverAssociationStatusTypeMap.Configure(modelBuilder.Entity<PatientCareGiverAssociationStatusType>());

            DeviceTypeMap deviceTypeMap = new DeviceTypeMap();
            deviceTypeMap.Configure(modelBuilder.Entity<DeviceType>());

            DeviceMap deviceMap = new DeviceMap();
            deviceMap.Configure(modelBuilder.Entity<Device>());

            TransmissionMap transmissionMap = new TransmissionMap();
            transmissionMap.Configure(modelBuilder.Entity<Transmission>());

            LeftSensorPlotDataMap leftSensorPlotDataMap = new LeftSensorPlotDataMap();
            leftSensorPlotDataMap.Configure(modelBuilder.Entity<LeftSensorPlotData>());

            LeftSensorBaselinePlotDataMap leftSensorBaselinePlotDataMap = new LeftSensorBaselinePlotDataMap();
            leftSensorBaselinePlotDataMap.Configure(modelBuilder.Entity<LeftSensorBaselinePlotData>());

            RightSensorPlotDataMap rightSensorPlotDataMap = new RightSensorPlotDataMap();
            rightSensorPlotDataMap.Configure(modelBuilder.Entity<RightSensorPlotData>());

            RightSensorBaselinePlotDataMap rightSensorBaselinePlotDataMap = new RightSensorBaselinePlotDataMap();
            rightSensorBaselinePlotDataMap.Configure(modelBuilder.Entity<RightSensorBaselinePlotData>());

            SupplementalPlotDataMap supplementalPlotDataMap = new SupplementalPlotDataMap();
            supplementalPlotDataMap.Configure(modelBuilder.Entity<SupplementalPlotData>());

            SupplementalPlotItemTypeMap supplementalPlotItemTypeMap = new SupplementalPlotItemTypeMap();
            supplementalPlotItemTypeMap.Configure(modelBuilder.Entity<SupplementalPlotItemType>());
        }
    }
}
﻿// ************************************************************
// Copyright (C) 2018 Injectsense, Inc. - All Rights Reserved
// ************************************************************
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;


namespace Injectsense.DataAccess.Entities
{
    /// <summary>
    /// Entity that represents the CareGiver table.
    /// </summary>
    public partial class CareGiver
    {
        [Key]
        public int Id { get; set; }
        public string NamePrefix { get; set; }
        public string NameFirst { get; set; }
        public string NameMiddle { get; set; }
        public string NameLast { get; set; }
        public string NameSuffix { get; set; }
        public string PhoneHome { get; set; }
        public string PhoneMobile { get; set; }
        public string EmailAddress { get; set; }
        public string Username { get; set; }
        public DateTime LastUpdateTimeUTC { get; set; }
        public Guid LastUpdatedBy { get; set; }

        public List<PatientCareGiverAssociation> PatientCareGiverAssociations { get; set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="CareGiver"/> class.
        /// </summary>
        public CareGiver()
        {
        }
    }
}
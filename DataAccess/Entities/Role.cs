﻿// ************************************************************
// Copyright (C) 2018 Injectsense, Inc. - All Rights Reserved
// ************************************************************

using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Injectsense.DataAccess.Entities
{
    // Note: This is annotating the fields/columns directly.
    // Not using a "mapper" to get from code to database.
    // This is not part of regular repository pattern used for regular data access.

    [Table("Role")]
    public class Role
    {
        [Key, Required]
        public int Id { get; set; }

        [Required]
        public string Name { get; set; }

        public List<UserRole> UserRoles { get; set; }
    }
}

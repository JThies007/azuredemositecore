﻿// ************************************************************
// Copyright (C) 2018 Injectsense, Inc. - All Rights Reserved
// ************************************************************
using System;
using System.ComponentModel.DataAnnotations;


namespace Injectsense.DataAccess.Entities
{
    /// <summary>
    /// Entity that represents the PatientCareGiverAssocation table in database.
    /// </summary>
    public partial class PatientCareGiverAssociation
    {
        [Key]
        public int Id { get; set; }

        public Guid PatientId { get; set; }
        public Patient Patient { get; set; }

        public Guid CareGiverId { get; set; }
        public CareGiver CareGiver { get; set; }

        public int CareGiverDiscontinueReasonTypeId { get; set; }
        public CareGiverDiscontinueReasonType CareGiverDiscontinueReasonType { get; set; }

        public int PatientCareGiverAssociationStatusTypeId { get; set; }
        PatientCareGiverAssociationStatusType PatientCareGiverAssociationStatusType { get; set; }

        public DateTime AssociationCreateDateUTC { get; set; }
        public DateTime AssociationRemovalDateUTC { get; set; }
        public DateTime LastUpdateTimeUTC { get; set; }
        public Guid LastUpdatedBy { get; set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="PatientCareGiverAssociation"/> class.
        /// </summary>
        public PatientCareGiverAssociation()
        {
        }
    }
}
﻿// ************************************************************
// Copyright (C) 2018 Injectsense, Inc. - All Rights Reserved
// ************************************************************
using System;
using System.ComponentModel.DataAnnotations;

namespace Injectsense.DataAccess.Entities
{
    /// <summary>
    /// Entity that represents the SupplementalPlotData table in database.
    /// </summary>
    public partial class SupplementalPlotData
    {
        [Key]
        public long Id { get; set; }
        public long TransmissionId { get; set; }
        public DateTime EventDateTime { get; set; }

        // Storage for foreign-key.
        public int SupplementalPlotItemTypeId { get; set; }
        public SupplementalPlotItemType GetSupplementalPlotItemType { get; set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="SupplementalPlotData"/> class.
        /// </summary>
        public SupplementalPlotData()
        {
        }
    }
}

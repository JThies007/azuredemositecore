﻿// ************************************************************
// Copyright (C) 2018 Injectsense, Inc. - All Rights Reserved
// ************************************************************
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Injectsense.DataAccess.Entities
{
    /// <summary>
    /// Entity representing the DeviceType table in the database.
    /// </summary>
    public partial class DeviceType
    {
        [Key]
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public byte[] UpdateRowTimestamp { get; set; }
        public string SerialNumberPrefix { get; set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="DeviceType"/> class.
        /// </summary>
        public DeviceType()
        {
        }
    }
}

﻿// ************************************************************
// Copyright (C) 2018 Injectsense, Inc. - All Rights Reserved
// ************************************************************
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Injectsense.DataAccess.Entities
{
    /// <summary>
    /// Entity representing the CountryCode table in the database.
    /// </summary>
    public partial class CountryCode
    {
        [Key]
        public int Id { get; set; }
        public string Country { get; set; }
        public string Code { get; set; }
        public bool IsDisplayed { get; set; }
        public byte[] UpdateRowTimestamp { get; set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="CountryCode"/> class.
        /// </summary>
        public CountryCode()
        {
        }
    }
}
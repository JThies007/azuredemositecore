﻿// ************************************************************
// Copyright (C) 2018 Injectsense, Inc. - All Rights Reserved
// ************************************************************
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Injectsense.DataAccess.Entities
{
    // Note: This is annotating the fields/columns directly.
    // Not using a "mapper" to get from code to database.
    // This is not part of regular repository pattern used for regular data access.

    [Table("User")]
    public class User
    {
        [Key, Required]
        public int Id { get; set; }

        [Required, MaxLength(20)]
        public string UserName { get; set; }

        [Required, MaxLength(1024)]
        public string PasswordHash { get; set; }

        [MaxLength(36)]
        public string FirstName { get; set; }

        [MaxLength(1)]
        public string MiddleInitial { get; set; }

        [MaxLength(36)]
        public string LastName { get; set; }

        public List<UserRole> UserRoles { get; set; }
    }
}

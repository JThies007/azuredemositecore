﻿// ************************************************************
// Copyright (C) 2018 Injectsense, Inc. - All Rights Reserved
// ************************************************************
using System;
using System.ComponentModel.DataAnnotations;

namespace Injectsense.DataAccess.Entities
{
    /// <summary>
    /// Entity that represents the ClinicStaff table in database.
    /// </summary>
    public partial class ClinicStaff
    {
        [Key]
        public Guid Id { get; set; }

        // Stores the foreign-key relationship to clinic
        public Guid ClinicId { get; set; }
        public Clinic Clinic { get; set; }

        public byte Role { get; set; }
        public string EmployeeId { get; set; }
        public string Username { get; set; }
        public string NamePrefix { get; set; }
        public string NameFirst { get; set; }
        public string NameMiddle { get; set; }
        public string NameLast { get; set; }
        public string NameSuffix { get; set; }
        public string PhoneWork { get; set; }
        public string PhoneWorkExtension { get; set; }
        public string PhoneMobile { get; set; }
        public string PrimaryEmail { get; set; }
        public string Password { get; set; }
        public DateTime LastUpdateTimeUTC { get; set; }
        public Guid LastUpdatedBy { get; set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="ClinicStaff"/> class.
        /// </summary>
        public ClinicStaff()
        {
        }
    }
}


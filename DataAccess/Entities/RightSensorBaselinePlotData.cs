﻿// ************************************************************
// Copyright (C) 2018 Injectsense, Inc. - All Rights Reserved
// ************************************************************
using System;
using System.ComponentModel.DataAnnotations;

namespace Injectsense.DataAccess.Entities
{
    /// <summary>
    /// Entity that represents the RightSensorBaselinePlotData
    /// </summary>
    public partial class RightSensorBaselinePlotData
    {
        [Key]
        public long Id { get; set; }
        public long TransmissionId { get; set; }
        public DateTime EventDateTime { get; set; }
        public Decimal Value { get; set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="RightSensorBaselinePlotData"/> class.
        /// </summary>
        public RightSensorBaselinePlotData()
        {
        }
    }
}

﻿// ************************************************************
// Copyright (C) 2018 Injectsense, Inc. - All Rights Reserved
// ************************************************************
using System;
using System.ComponentModel.DataAnnotations;

namespace Injectsense.DataAccess.Entities
{
    /// <summary>
    /// Entity representing the LeftSensorPlotData table in the database.
    /// </summary>
    public partial class LeftSensorPlotData
    {
        [Key]
        public long Id { get; set; }
        public long TransmissionId { get; set; }
        public DateTime EventDateTime { get; set; }
        public Decimal Value { get; set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="LeftSensorPlotData"/> class.
        /// </summary>
        public LeftSensorPlotData()
        {
        }
    }
}

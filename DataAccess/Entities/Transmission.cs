﻿// ************************************************************
// Copyright (C) 2018 Injectsense, Inc. - All Rights Reserved
// ************************************************************
using System;
using System.ComponentModel.DataAnnotations;


namespace Injectsense.DataAccess.Entities
{
    /// <summary>
    /// Entity that represents the Transmission table in the database.
    /// </summary>
    public partial class Transmission
    {
        [Key]
        public long Id { get; set; }
        public DateTime ReceivedTimeUTC { get; set; }
        public byte[] Package { get; set; }
        public string DeviceSerialNumber1 { get; set; }
        public string DeviceSerialNumber2 { get; set; }
        public Guid PatientId { get; set; }
        public DateTime TimeOfInterrogationUTC { get; set; }
        public string ReaderSerialNumber { get; set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="Transmission"/> class.
        /// </summary>
        public Transmission()
        {
        }
    }
}

﻿// ************************************************************
// Copyright (C) 2018 Injectsense, Inc. - All Rights Reserved
// ************************************************************
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Injectsense.DataAccess.Entities
{
    /// <summary>
    /// Entity that represents the Language table in the database.
    /// </summary>
    public partial class Language
    {
        [Key]
        public int Id { get; set; }
        public string Description { get; set; }
        public string DisplayString { get; set; }
        public bool IsDisplayed { get; set; }
        public byte[] UpdateRowTimestamp { get; set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="Language"/> class.
        /// </summary>
        public Language()
        {
        }
    }
}
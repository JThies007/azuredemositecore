﻿// ************************************************************
// Copyright (C) 2018 Injectsense, Inc. - All Rights Reserved
// ************************************************************
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Injectsense.DataAccess.Entities
{
    /// <summary>
    /// Entity representing the Patient table in the database.
    /// </summary>
    public partial class Patient
    {
        [Key]
        public Guid Id { get; set; }
        public string Identifier { get; set; }
        public string ApplicationUsername { get; set; }
        public bool HasReader { get; set; }
        public string NamePrefix { get; set; }
        public string NameFirst { get; set; }
        public string NameMiddle { get; set; }
        public string NameLast { get; set; }
        public string NameSuffix { get; set; }
        public string Gender { get; set; }
        public DateTime DateOfBirthNotUTC { get; set; }
        public string AddressLine1 { get; set; }
        public string AddressLine2 { get; set; }
        public string AddressLine3 { get; set; }
        public string AddressLine4 { get; set; }
        public string City { get; set; }
        public string StateCode { get; set; }
        public string Zipcode { get; set; }
        public string PostOfficeNumber { get; set; }

        public int CountryCodeId { get; set; }
        public CountryCode CountryCode { get; set; }

        public string PhoneHome { get; set; }
        public string PhoneMobile { get; set; }
        public string PhoneWorkNumber { get; set; }
        public string PhoneWorkExtension { get; set; }
        public string PhoneOtherNumber { get; set; }
        public string PhoneOtherDescription { get; set; }
        public string PrimaryEmail { get; set; }
        public string SecondaryEmail { get; set; }
        public DateTime LastUpdateTimeUTC { get; set; }
        public Guid LastUpdatedBy { get; set; }

        public List<PatientCareGiverAssociation> PatientCareGiverAssociations { get; set; }
        public List<ClinicPatientAssociation> ClinicPatientAssociations { get; set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="Patient"/> class.
        /// </summary>
        public Patient()
        {
        }
    }
} 

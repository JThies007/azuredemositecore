﻿// ************************************************************
// Copyright (C) 2018 Injectsense, Inc. - All Rights Reserved
// ************************************************************
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Injectsense.DataAccess.Entities
{
    /// <summary>
    /// Entity represents the ClinicPatientAssociationStatusType table in the database.
    /// </summary>
    public partial class ClinicPatientAssociationStatusType
    {
        [Key]
        public int Id { get; set; }
        public string Status { get; set; }
        public string Description { get; set; }
        public byte[] UpdateRowTimestamp { get; set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="ClinicPatientAssociationStatusType"/> class.
        /// </summary>
        public ClinicPatientAssociationStatusType()
        {
        }
    }
}

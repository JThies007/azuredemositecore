﻿// ************************************************************
// Copyright (C) 2018 Injectsense, Inc. - All Rights Reserved
// ************************************************************
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Injectsense.DataAccess.Entities
{
    /// <summary>
    /// Entity that represents the Clinic table in database.
    /// </summary>
    public partial class Clinic
    {
        [Key]
        public Guid Id { get; set; }
        public string NamePrimary { get; set; }
        public string NameSecondary { get; set; }
        public string ClinicContractReference { get; set; }
        public string AddressLine1 { get; set; }
        public string AddressLine2 { get; set; }
        public string AddressLine3 { get; set; }
        public string AddressLine4 { get; set; }
        public string City { get; set; }
        public string StateCode { get; set; }
        public string Zipcode { get; set; }
        public string PostOfficeNumber { get; set; }
        public string PrimaryPhone { get; set; }
        public string SecondaryPhone { get; set; }
        public string FAX { get; set; }
        public string BillingAddressLine1 { get; set; }
        public string BillingAddressLine2 { get; set; }
        public string BillingAddressLine3 { get; set; }
        public string BillingAddressLine4 { get; set; }
        public string BillingCity { get; set; }
        public string BillingStateCode { get; set; }
        public string BillingZipcode { get; set; }
        public bool BillingAddressSameAsClinic { get; set; }
        public string PrimaryContactFirstName { get; set; }
        public string PrimaryContactLastName { get; set; }
        public string PrimaryContactEmailAddress { get; set; }
        public Guid? PrimaryAdminId { get; set; }
        public Guid? SecondaryAdminId { get; set; }
        public string ClinicEmailAddress { get; set; }
        public DateTime LastUpdateTimeUTC { get; set; }
        public Guid LastUpdatedBy { get; set; }

        public int TimeZoneId { get; set; }
        public TimeZone TimeZone { get; set; }

        public int CountryCodeId { get; set; }
        public CountryCode CountryCode { get; set; }

        public int LanguageId { get; set; }
        public Language Language { get; set; }

        public int ClinicStatusTypeId { get; set; }
        public ClinicStatusType ClinicStatusType { get; set; }

        public Guid? ParentClinicId { get; set; }

        public List<ClinicPatientAssociation> ClinicPatientAssociations { get; set; }


        /// <summary>
        /// Initializes a new instance of the <see cref="Clinic"/> class.
        /// </summary>
        public Clinic()
        {
        }
    }
}

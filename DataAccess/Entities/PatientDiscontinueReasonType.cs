﻿// ************************************************************
// Copyright (C) 2018 Injectsense, Inc. - All Rights Reserved
// ************************************************************
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;


namespace Injectsense.DataAccess.Entities
{
    /// <summary>
    /// Entity representing the PatientDicsontinueReasonType table in the database.
    /// </summary>
    public partial class PatientDiscontinueReasonType
    {
        [Key]
        public int Id { get; set; }
        public string Reason { get; set; }
        public string Description { get; set; }
        public byte[] UpdateRowTimestamp { get; set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="PatientDiscontinueReasonType"/> class.
        /// </summary>
        public PatientDiscontinueReasonType()
        {
        }
    }
}

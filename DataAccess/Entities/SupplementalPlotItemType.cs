﻿// ************************************************************
// Copyright (C) 2018 Injectsense, Inc. - All Rights Reserved
// ************************************************************
using System.ComponentModel.DataAnnotations;

namespace Injectsense.DataAccess.Entities
{
    /// <summary>
    /// Entity that represents the SupplementalPlotItemType table in the database.
    /// </summary>
    public partial class SupplementalPlotItemType
    {
        [Key]
        public int Id { get; set; }
        public string Description { get; set; }
        public byte[] UpdateRowTimestamp { get; set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="SupplementalPlotItemType"/> class.
        /// </summary>
        public SupplementalPlotItemType()
        {
        }
    }
}

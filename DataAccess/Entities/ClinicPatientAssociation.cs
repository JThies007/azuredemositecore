﻿// ************************************************************
// Copyright (C) 2018 Injectsense, Inc. - All Rights Reserved
// ************************************************************
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;


namespace Injectsense.DataAccess.Entities
{
    /// <summary>
    /// Entity that represents the ClinicPatientAssociation table in database.
    /// </summary>
    public partial class ClinicPatientAssociation
    {
        [Key]
        public int Id { get; set; }

        // The following two are required to get from this "join" table to the primary-key tables.
        public Guid ClinicId { get; set; }
        public Clinic Clinic { get; set; }

        public Guid PatientId { get; set; }
        public Patient Patient { get; set; }

        // This stored the foreign-key for status type.
        public int ClinicPatientAssociationStatusTypeId { get; set; }
        public ClinicPatientAssociationStatusType ClinicPatientAssociationStatusType { get; set; }

        public int PatientDiscontinueReasonTypeId { get; set; }
        public long MostRecentTransmissionId { get; set; }
        public int NewTransmissionCount { get; set; }
        public int ViewableTransmissionCount { get; set; }
        public DateTime AssociationCreateDateUTC { get; set; }
        public DateTime? AssociationRemovalDateUTC { get; set; }
        public DateTime LastUpdateTimeUTC { get; set; }
        public Guid LastUpdatedBy { get; set; }
        public DateTime? LastOfficeVisitDateUTC { get; set; }
        public string MostRecentTransmissionLeftEyeDSN { get; set; }
        public string MostRecentTransmissionRightEyeDSN { get; set; }
        public DateTime? MostRecentTransmissionReceivedDateUTC { get; set; }
        public Guid Physician { get; set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="ClinicPatientAssociation"/> class.
        /// </summary>
        public ClinicPatientAssociation()
        {
        }
    }
}

﻿// ************************************************************
// Copyright (C) 2018 Injectsense, Inc. - All Rights Reserved
// ************************************************************
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Injectsense.DataAccess.Entities
{
    /// <summary>
    /// Entity that represents the TimeZone table in the database.
    /// </summary>
    public partial class TimeZone
    {
        [Key]
        public int Id { get; set; }
        public string CountryCode { get; set; }
        public string Coordinates { get; set; }
        public string Name { get; set; }
        public string DisplayString { get; set; }
        public int DisplayOrder { get; set; }
        public bool IsDisplayed { get; set; }
        public decimal OffsetFromGMTHours { get; set; }
        public decimal OffsetFromGMTHoursDST { get; set; }
        public byte[] UpdateRowTimestamp { get; set; }
        public string DotNetTimeZoneInfo { get; set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="TimeZone"/> class.
        /// </summary>
        public TimeZone()
        {
        }
    }
}

﻿// ************************************************************
// Copyright (C) 2018 Injectsense, Inc. - All Rights Reserved
// ************************************************************
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;


namespace Injectsense.DataAccess.Entities
{
    /// <summary>
    /// Entity that represents the PatientDeviceAssociation table in database.
    /// </summary>
    public partial class PatientDeviceAssociation
    {
        [Key]
        public int Id { get; set; }

        // Set up relationship/storage for foreign-key
        public Guid PatientId { get; set; }
        public Patient Patient { get; set; }

        // Set up relationship/storage for foreign-key
        public Guid DeviceId { get; set; }
        public Device Device { get; set; }

        // Set up relationship/storage for foreign-key
        public int DeviceStatusTypeId { get; set; }
        public DeviceStatusType DeviceStatusType { get; set; }

        public string ImplantEye { get; set; }
        public DateTime? ImplantDateNotUTC { get; set; }
        public DateTime? ExplantDateNotUTC { get; set; }
        public DateTime? DisabledDateNotUTC { get; set; }
        
        public DateTime LastUpdateTimeUTC { get; set; }
        public Guid LastUpdatedBy { get; set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="PatientDeviceAssociation"/> class.
        /// </summary>
        public PatientDeviceAssociation()
        {
        }
    }
}
﻿// ************************************************************
// Copyright (C) 2018 Injectsense, Inc. - All Rights Reserved
// ************************************************************
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;


namespace Injectsense.DataAccess.Entities
{
    /// <summary>
    /// Entity that represents Device table in database.
    /// </summary>
    public partial class Device
    {
        [Key]
        public Guid Id { get; set; }
        public string SerialNumber { get; set; }
        public decimal HardwareVersion { get; set; }
        public decimal CalCoeffA { get; set; }
        public decimal CalCoeffB { get; set; }
        public decimal CalCoeffC { get; set; }
        public DateTime ManufactureDate { get; set; }

        // Foreign-key that references DeviceType table.
        public int DeviceTypeId { get; set; }
        public DeviceType DeviceType { get; set; }

        public DateTime DateAddedUTC { get; set; }
        public bool IsAvailableForUse { get; set; }
        public string UniqueManufacturingIdentifier { get; set; }

        public List<PatientDeviceAssociation> PatientDeviceAssocations { get; set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="Device"/> class.
        /// </summary>
        public Device()
        {
        }
    }
}

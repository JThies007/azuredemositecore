﻿// ************************************************************
// Copyright (C) 2018 Injectsense, Inc. - All Rights Reserved
// ************************************************************

using Injectsense.DataAccess.Entities;
using Microsoft.EntityFrameworkCore;

namespace Injectsense.DataAccess
{
    // This is only used for UserManagement (login/security).
    // Not part of the standard repository pattern.
    public class UserDataContext : DbContext
    {
        public UserDataContext(DbContextOptions<UserDataContext> options) : base(options)
        {
        }

        public DbSet<Role> Roles
        {
            get;
            set;
        }

        public DbSet<User> Users
        {
            get;
            set;
        }

        public DbSet<UserRole> UserRoles
        {
            get;
            set;
        }
    }
}

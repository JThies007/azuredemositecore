﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Injectsense.Models
{
    public class ManageMyClinicModel : ModelBase
    {
        // This hidden field is used to indicate which tab has been submitted.
        public string IsClinicProfileTab { get; set; }

        // --- These fields are on the Clinic Info Contact Tab. ---
        public string ClinicNamePrimary { get; set; }
        public string ClinicAddressLine1 { get; set; }
        public string ClinicAddressLine2 { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string Zipcode { get; set; }
        public string PhonePrimary { get; set; }

        // --- These fields are on the Primary Contact Tab. ---
        public string PrimaryContactFirstName { get; set; }
        public string PrimaryContactLastName { get; set; }
        public string PrimaryContactEmailAddress { get; set; }
    }
}
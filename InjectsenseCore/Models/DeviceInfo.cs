﻿using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;


namespace Injectsense.Models
{
    public class DeviceInfo
    {
        public string ModelName { get; set; }

        [RegularExpression(@"^[a-zA-Z]{3}[0-9]{5}[a-zA-Z]{1}")]
        [StringLength(20, MinimumLength = 9)]
        [Display(Name = "Device Serial Number")]
        public string SerialNumber { get; set; }

        [Display(Name = "Date")]
        [RegularExpression(@"^(0[1-9]|[1-9]|[12][0-9]|3[01])-([Jj][Aa][Nn]|[Ff][Ee][Bb]|[Mm][Aa][Rr]|[Aa][Pp][Rr]|[Mm][Aa][Yy]|[Jj][Uu][Nn]|[Jj][Uu][Lj]|[Aa][Uu][Gg]|[Ss][Ee][Pp]|[Oo][Cc][Tt]|[Nn][Oo][Vv]|[Dd][Ee][Cc])-(19|20)\d\d$", ErrorMessage = "Invalid date entered.")]
        public string ImplantDate { get; set; }

        public string ImplantLocation { get; set; }

        public IEnumerable<SelectListItem> ImplantLocationList { get; set; }
        public int ImplantLocationSelectId { get; set; }

        public IEnumerable<SelectListItem> DeviceModelList { get; set; }
        public string DeviceModelSelectId { get; set; }
    }
}
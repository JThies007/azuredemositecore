﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace Injectsense.Models
{
    public class PatientModel : ModelBase
    {
        // Note: Only fields presented on the UI have attributes.

        // Patient Information
        public Guid PatientId { get; set; }

        // Used to indicate which tab(form) is being posted back to server.
        public string IsPatientInfoTab { get; set; }
        public bool PatientInfoEntryOnly { get; set; }

        public string Identifier { get; set; }

        public string ApplicationUsername { get; set; }
        public bool HasReader { get; set; }
        public string NamePrefix { get; set; }

        public string NameFirst { get; set; }

        public string NameMiddle { get; set; }

        public string NameLast { get; set; }

        public string NameSuffix { get; set; }
        public string NameCombined { get; set; }
        public string Gender { get; set; }
        public IList<string> NamePrefixes { get; set; }
        public IList<string> Genders { get; set; }

        public string DateOfBirth { get; set; }

        public string AddressLine1 { get; set; }
        public string AddressLine2 { get; set; }
        public string AddressLine3 { get; set; }
        public string AddressLine4 { get; set; }
        public string City { get; set; }
        public string StateCode { get; set; }
        public string Zipcode { get; set; }
        public string PostOfficeNumber { get; set; }
        public int CountryCodeId { get; set; }

        public string PhoneHome { get; set; }
        public string PhoneMobile { get; set; }
        public string PhoneWorkNumber { get; set; }
        public string PhoneWorkExtension { get; set; }
        public string PhoneOtherNumber { get; set; }
        public string PhoneOtherDescription { get; set; }
        public string PrimaryEmail { get; set; }
        public string SecondaryEmail { get; set; }

        // *******************************
        // Device Information Rows 1 & 2
        // *******************************

        // Device Information Row 1

        public string DeviceSerialNumberRow1 { get; set; }
        public string DeviceSerialNumberRow1Hidden { get; set; }

        public string DeviceImplantDateRow1 { get; set; }

        public List<SelectListItem> DeviceImplantLocationListRow1 { get; set; }
        public int DeviceImplantLocationSelectIdRow1 { get; set; }

        public List<SelectListItem> DeviceModelListRow1 { get; set; }
        public int DeviceModelSelectIdRow1 { get; set; }

        // Device Information Row 2

        public string DeviceSerialNumberRow2 { get; set; }
        public string DeviceSerialNumberRow2Hidden { get; set; }

        public string DeviceImplantDateRow2 { get; set; }

        public List<SelectListItem> DeviceImplantLocationListRow2 { get; set; }
        public int DeviceImplantLocationSelectIdRow2 { get; set; }

        public List<SelectListItem> DeviceModelListRow2 { get; set; }
        public int DeviceModelSelectIdRow2 { get; set; }

        public PatientModel()
        {
            PatientId = Guid.Empty;
            PatientInfoEntryOnly = false;

            NamePrefixes = new List<string>
            {
             "Dr.", "Mr.", "Mrs.", "Ms.", "None"
            };

            Genders = new List<string>
            {
             "Male", "Female"
            };
        }
    }
}
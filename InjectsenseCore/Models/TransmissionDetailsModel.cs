﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using Injectsense.Models;

namespace Injectsense.Models
{
    public class TransmissionDetailsModel : ModelBase
    {
        public int TransmissionId { get; set; }
        [Display(Name = "Name:")]
        public string PatientName { get; set; }
        [Display(Name = "Date of Birth:")]
        public DateTime DateOfBirth { get; set; }
        [Display(Name = "Physician:")]
        public string Physician { get; set; }
        public DateTime Received { get; set; }
        [Display(Name = "ID:")]
        public string IdNumber { get; set; }
        [Display(Name = "Smart-IOL®     S/N:")]
        public string LeftEyeSerialNumber { get; set; }
        [Display(Name = "Smart-IOL®     S/N:")]
        public string RightEyeSerialNumber { get; set; }
        [Display(Name = "Last Office Visit:")]
        public string LastOfficeVisitString { get; set; }
        [Display(Name = "Reporting Period:")]
        public string ReportingPeriod { get; set; } // Should be a new ojbect with info
        [Display(Name = "Sample Period:")]
        public int SamplePeriod { get; set; }
        [Display(Name = "Hour")]  //TODO: should be a dynamic label
        public int SampleUnits { get; set; }

        public string PressureImageFileReference { get; set; }
    }
}
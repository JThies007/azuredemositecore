﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Injectsense.DataAccess.Entities;

namespace Injectsense.Models
{
    public class PatientInfo
    {
        public List<Device> DeviceList { get; set; }
        public DateTime LastReceivedTransmissionDate { get; set; }
        public long LastReceivedTransmission { get; set; }
        public int TotalNumberOfTransmissionsReceived { get; set; }

        public string PatientName { get; set; }
        public string[] DeviceInfo { get; set; }
        public int NumberOfEnabledImplants { get; set; }
    }
}
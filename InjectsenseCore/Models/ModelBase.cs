﻿
using System;
using System.Web;
using System.Collections.Generic;
using Injectsense.Helpers;
using Injectsense.DataAccess.Repositories;
using Injectsense.DataAccess.Entities;
using Microsoft.AspNetCore.Http;
using TimeZoneConverter;
using Injectsense.DataAccess;

namespace Injectsense.Models
{
    public class ModelBase
    {
        public string LoggedInUserFirstName { get; private set; }
        public string LoggedInUserLastName { get; private set; }
        public DateTime CurrentDateTimeLocalizedToClinic { get; private set; }
        public Guid ClinicId { get; private set; }
        public string ClinicIdString { get; set; }
        public string ClinicName { get; set; }

        public ModelBase()
        {
        }

        public void Initialize(string userName, MainContext mainContext)
        {
            // We could put the clinic id in the html and get it back on this call.
            // But the goal is to leave as little info as possible in html.
            // So let's get user and map to their clinic, and from clinic, we'll get list of patients.
            (Guid ClinicId, Guid ClinicStaffId, string ClinicName, string ClinicStaffFirstName, string ClinicStaffLastName) clinicInfo = ClinicStaffHelper.GetUsersClinicIdFromRequest(userName, mainContext);

            // Get the TimeZone info for the clinic.
            IClinicRepository clinicRepository = new ClinicRepository(mainContext);
            string localTimeZoneInfo = clinicRepository.GetClinicDotNetTimeZoneInfo(clinicInfo.ClinicId);
            TimeZoneInfo timeZoneInfo = TZConvert.GetTimeZoneInfo(localTimeZoneInfo);

            // Get all patients for clinic that have new transmissions for home page list.
            // List returned is orded by Most Recent transmission received (newest == first in list).
            List<ClinicPatientAssociation> ClinicPatientInfoList = clinicRepository.GetAllPatientsWithNewTransmissionsForClinic(clinicInfo.ClinicId);

            DateTime currentDate = DateTime.UtcNow;
            DateTime.SpecifyKind(currentDate, DateTimeKind.Utc);
            this.CurrentDateTimeLocalizedToClinic = TimeZoneInfo.ConvertTimeFromUtc(currentDate, timeZoneInfo);

            this.ClinicId = clinicInfo.ClinicId;
            this.ClinicIdString = clinicInfo.ClinicId.ToString();
            this.ClinicName = clinicInfo.ClinicName;
            this.LoggedInUserFirstName = clinicInfo.ClinicStaffFirstName;
            this.LoggedInUserLastName = clinicInfo.ClinicStaffLastName;
        }
    }
}
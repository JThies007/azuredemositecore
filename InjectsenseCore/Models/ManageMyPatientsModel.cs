﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Injectsense.DataAccess.Entities;

namespace Injectsense.Models
{
    public class ManageMyPatientInfo
    {
        public long MostRecentTransmissionId { get; set; }
        public string PatientName { get; set; }
        public Guid PatientId { get; set; }
        public string LastReceivedTimestampString { get; set; }
        public string IdNumber { get; set; }
        public string LeftEyeSerialNumber { get; set; }
        public string RightEyeSerialNumber { get; set; }
        public string LastOfficeVisitString { get; set; }
        public int TotalNumberOfTransmissions { get; set; }
        public string DateOfBirthString { get; set; }
    }

    public class ManageMyPatientsModel : ModelBase
    {
        public List<ManageMyPatientInfo> PatientList { get; set; }
        public Guid ClinicStaffId { get; set; }

        public ManageMyPatientsModel()
        {
            PatientList = new List<ManageMyPatientInfo>();
        }
    }
}
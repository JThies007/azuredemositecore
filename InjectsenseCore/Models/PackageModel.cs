﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Injectsense.Models
{
    public class PackageInfo
    {
        public string ReceivedTimeUTC { get; set; }
        public string ReceivedTimeCST { get; set; }
        public string TimeOfInterrogationUTC { get; set; }
        public string TimeOfInterrogationCST { get; set; }
        public string SensorSerialNumber1 { get; set; }
        public string SensorSerialNumber2 { get; set; }
        public string ReaderSerialNumber { get; set; }
        public string TransmissionId { get; set; }
    }


    public class PackageModel : ModelBase
    {
        public string CurrentDateTime { get; set; }
        public List<PackageInfo> PackageInfoList { get; set; }
        public string SearchSensorSerialNumber { get; set; }
        public string DateRangeStart { get; set; }
        public string DateRangeStop { get; set; }

        public PackageModel()
        {
            PackageInfoList = new List<PackageInfo>();
        }
    }
}
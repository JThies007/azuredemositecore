﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Injectsense.Models
{
    public class LatestPatientActivity
    {
        public long TransmissionId { get; set; }
        public string PatientName { get; set; }
        public Guid PatientId { get; set; }
        public string ReceivedDateString { get; set; }
        public DateTime ReceivedDate { get; set; }
        public string Status { get; set; }
        public string IdNumber { get; set; }
        public string LeftEyeSerialNumber { get; set; }
        public string RightEyeSerialNumber { get; set; }
        public string LastOfficeVisitString { get; set; }
    }

    public class LatestPatientActivityModel : ModelBase
    {
        public List<LatestPatientActivity> PatientActivityList { get; set; }
        public string ClinicProfileImage { get; set; }

        public LatestPatientActivityModel()
        {
            PatientActivityList = new List<LatestPatientActivity>();
        }
    }
}
﻿// ************************************************************
// Copyright (C) 2018 Injectsense, Inc. - All Rights Reserved
// ************************************************************
using System;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.AspNetCore.Authentication.Cookies;
using Injectsense.DataAccess;
using Injectsense.DataAccess.Repositories;
using Injectsense.DataAccess.Entities;
using Injectsense.UserManagement;

namespace InjectsenseCore
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddDbContext<MainContext>(options =>
                options.UseSqlServer(Configuration.GetConnectionString("DefaultConnection")), ServiceLifetime.Transient);

            services.AddTransient<IClinicRepository, ClinicRepository>();
            services.AddTransient<IClinicStaffRepository, ClinicStaffRepository>();
            services.AddTransient<IDeviceRepository, DeviceRepository>();
            services.AddTransient<IPatientRepository, PatientRepository>();
            services.AddTransient<ITransmissionRepository, TransmissionRepository>();

            services.AddTransient<IUnitOfWork, UnitOfWork>();

            services.AddDbContext<UserDataContext>(options => options.UseSqlServer(Configuration.GetConnectionString("DefaultConnection")));

            services.AddIdentity<User, UserRole>()
                        .AddDefaultTokenProviders();

            services.AddTransient<IUserStore<User>, UserStore>();
            services.AddTransient<IRoleStore<UserRole>, RoleStore>();

            services.AddAuthentication(
                    CookieAuthenticationDefaults.AuthenticationScheme
                        ).AddCookie(CookieAuthenticationDefaults.AuthenticationScheme,
                            options =>
                            {
                                options.Cookie.HttpOnly = true;
                                options.Cookie.Expiration = TimeSpan.FromHours(5);
                                options.Cookie.Domain = "injectsense.com";
                                options.LoginPath = "/Account/Login";
                                options.LogoutPath = "/Account/Logout";
                            });

            services.AddMvc();

            services.AddDistributedMemoryCache();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, SignInManager<User> s)
        {
            if (env.IsDevelopment())
            {
                app.UseBrowserLink();
                app.UseDeveloperExceptionPage();
                app.UseDatabaseErrorPage();

                // TODO: Development code that enables users to be created at runtime.
                //if (s.UserManager.FindByNameAsync("ClinicStaff3").Result == null)
                //{
                //    var result = s.UserManager.CreateAsync(new User
                //    {
                //        UserName = "ClinicStaff1",
                //    }, "isdemo2018B!").Result;
                //}
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
            }

            // This needs to be called before UseMvc().
            // Middleware executes in the order you declare things in your app.
            app.UseAuthentication();

            app.UseStaticFiles();

            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "default",
                    template: "{controller=Home}/{action=Index}/{id?}");
            });
        }
    }
}

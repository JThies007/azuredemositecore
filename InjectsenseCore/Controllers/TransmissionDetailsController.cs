﻿// ************************************************************
// Copyright (C) 2018 Injectsense, Inc. - All Rights Reserved
// ************************************************************
using Injectsense.Models;
using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
//using System.Web.UI.DataVisualization.Charting;
using Injectsense.DataAccess.Repositories;
using Injectsense.DataAccess.Entities;
using Injectsense.DataAccess;
using Injectsense.Helpers;
using System.Drawing;
using System.IO;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.DependencyInjection;
using System.Security.Claims;

namespace Injectsense.Controllers
{
    [Authorize]
    [OutputCache(Duration = 0)]
    public class TransmissionDetailsController : Controller
    {
        IUnitOfWork Transaction { get; }
        IPatientRepository PatientRepository { get; }
        ITransmissionRepository TransmissionRepository { get; }
        IClinicRepository ClinicRepository { get; }
        IDeviceRepository DeviceRepository { get; }

        private readonly IHttpContextAccessor HttpContextAccessor;
        private readonly IServiceProvider Services;

        public TransmissionDetailsController(IUnitOfWork unitOfWork,
                                             IPatientRepository patientRepository,
                                             IClinicRepository clinicRepository,
                                             IDeviceRepository deviceRepository,
                                             ITransmissionRepository transmissionRepository,
                                             IHttpContextAccessor httpContextAccessor,
                                             IServiceProvider services)
        {
            Transaction = unitOfWork;
            PatientRepository = patientRepository;
            TransmissionRepository = transmissionRepository;
            ClinicRepository = clinicRepository;
            DeviceRepository = deviceRepository;

            HttpContextAccessor = httpContextAccessor;
            Services = services;
        }

        // GET: TransmissionDetails
        public ActionResult Index(long TransmissionId, Guid PatientId, Guid ClinicId)
        {
            MainContext mainContext = Services.GetRequiredService<MainContext>();

            ClaimsPrincipal currentUser = this.User;
            string currentUserName = currentUser.FindFirst(ClaimTypes.Name).Value;

            (Guid ClinicId, Guid ClinicStaffId, string ClinicName, string ClinicStaffFirstName, string ClinicStaffLastName) clinicInfo = ClinicStaffHelper.GetUsersClinicIdFromRequest(currentUserName, mainContext);

            Patient patient = PatientRepository.GetPatient(PatientId);
            Transmission transmission = TransmissionRepository.GetTransmission(TransmissionId);

            // Take into account Last Office Visit may be null.
            DateTime? lastOfficeVisit = ClinicRepository.GetLastOfficeVisitDate(ClinicId, PatientId);
            string lastOfficeVisitString = lastOfficeVisit.HasValue ? lastOfficeVisit.Value.ToString("dd-MMM-yyyy") : "";

            TransmissionDetailsModel model = new TransmissionDetailsModel();
            // model.Initialize(this.Request.Cookies);
            model.Initialize(currentUserName, mainContext);

            model.PatientName = patient.NameLast + ", " + patient.NameFirst;
            model.DateOfBirth = patient.DateOfBirthNotUTC;
            model.Physician = PatientRepository.GetPhysicianName(PatientId);
            model.Received = transmission.ReceivedTimeUTC;
            model.IdNumber = patient.Identifier;
            // TODO: Need figure out which serial number is Left and Right - just putting in serial number is not correct.
            model.LeftEyeSerialNumber = transmission.DeviceSerialNumber1;
            model.RightEyeSerialNumber = transmission.DeviceSerialNumber2;
            model.LastOfficeVisitString = lastOfficeVisitString;
            model.ReportingPeriod = "1 Mar 2018 - 31 Mar 2018"; // TODO: This is hardcoded and will be filled in when data is real.
            model.SamplePeriod = 1;  // TODO: This is hardcoded and will be filled in when data is real.

            // Get the left sensor plot data points. Do this from (time of interrogation) to back 30 days.
            List<PlotPressureDataPoint> leftSensorPlotPoints = PatientRepository.GetPlotData(PatientId, transmission.TimeOfInterrogationUTC.AddDays(-30), transmission.TimeOfInterrogationUTC, PlotDataType.LeftSensorPlotData);
            List<PlotPressureDataPoint> rightSensorPlotPoints = PatientRepository.GetPlotData(PatientId, transmission.TimeOfInterrogationUTC.AddDays(-30), transmission.TimeOfInterrogationUTC, PlotDataType.RightSensorPlotData);

            // TODO: Commented out since charts not available in Core.
            //string path = Server.MapPath("~/ChartImages/");
            //if (!Directory.Exists(path))
            //{
            //    Directory.CreateDirectory(path);
            //}

            //Random rnd = new Random();
            //int rndNumber = rnd.Next(10000);
            //string filename = PatientId.ToString() + "-" + rndNumber.ToString() + ".png";
            //string pathAndFilename = path + filename;

            //CreateChart(leftSensorPlotPoints, rightSensorPlotPoints, transmission.TimeOfInterrogationUTC, pathAndFilename);
            //model.PressureImageFileReference = @"/ChartImages/" + filename;

            return View(model);
        }

        /// <summary>
        /// Creates the chart. This will set the graph up to be time of interrogation - 30 days (independent of data being available).
        /// </summary>
        /// <param name="leftSensorPlotPoints">The left sensor plot points.</param>
        /// <param name="rightSensorPlotPoints">The right sensor plot points.</param>
        /// <param name="timeOfInterrogation">The time of interrogation.</param>
        /// <param name="filename">The filename.</param>
        // TODO: Commented out when moving to Core - assume not available.
        //private void CreateChart(List<PlotPressureDataPoint> leftSensorPlotPoints, List<PlotPressureDataPoint> rightSensorPlotPoints, DateTime timeOfInterrogation, string filename)
        //{
        //    Chart chart = new Chart();
        //    Series seriesLeftSensor = new Series();
        //    seriesLeftSensor.Name = "SeriesLeftSensor";
        //    chart.Series.Add(seriesLeftSensor);
        //    Series seriesRightSensor = new Series();
        //    seriesRightSensor.Name = "SeriesRightSensor";
        //    chart.Series.Add(seriesRightSensor);
        //    Series seriesBaselineSensor = new Series();
        //    seriesBaselineSensor.Name = "SeriesBaselineSensor";
        //    chart.Series.Add(seriesBaselineSensor);

        //    // feed the chart control the left sensor points.
        //    for (int pointIndex = 0; pointIndex < leftSensorPlotPoints.Count; pointIndex++)
        //    {
        //        chart.Series["SeriesLeftSensor"].Points.AddXY(leftSensorPlotPoints[pointIndex].DateValue, leftSensorPlotPoints[pointIndex].PressureValue);
        //    }

        //    // feed the chart control the right sensor points.
        //    for (int pointIndex = 0; pointIndex < rightSensorPlotPoints.Count; pointIndex++)
        //    {
        //        chart.Series["SeriesRightSensor"].Points.AddXY(rightSensorPlotPoints[pointIndex].DateValue, rightSensorPlotPoints[pointIndex].PressureValue);
        //    }

        //    // Set point chart type
        //    chart.Series["SeriesLeftSensor"].ChartType = SeriesChartType.Point;
        //    chart.Series["SeriesRightSensor"].ChartType = SeriesChartType.Point;

        //    // Disable data points labels
        //    chart.Series["SeriesLeftSensor"].IsValueShownAsLabel = false;
        //    chart.Series["SeriesRightSensor"].IsValueShownAsLabel = false;

        //    // Set marker size
        //    chart.Series["SeriesLeftSensor"].MarkerSize = 3;
        //    chart.Series["SeriesRightSensor"].MarkerSize = 3;

        //    // Set marker shape
        //    chart.Series["SeriesLeftSensor"].MarkerStyle = MarkerStyle.Circle;
        //    chart.Series["SeriesRightSensor"].MarkerStyle = MarkerStyle.Circle;

        //    // Set marker color
        //    //series.Color = Color.FromArgb(112, 255, 200);
        //    chart.Series["SeriesLeftSensor"].Color = Color.FromArgb(63, 72, 204); // Blue
        //    chart.Series["SeriesRightSensor"].Color = Color.FromArgb(175, 61, 60);  // Red

        //    // Set X and Y axis scale
        //    ChartArea chartArea = new ChartArea();
        //    chartArea.Name = "ChartArea";
        //    chart.ChartAreas.Add(chartArea);
        //    chart.ChartAreas["ChartArea"].AxisY.Maximum = 35.0;
        //    chart.ChartAreas["ChartArea"].AxisY.Minimum = 0.0;
        //    chart.ChartAreas["ChartArea"].AxisY.Interval = 5.0;
        //    chart.ChartAreas["ChartArea"].AxisX.IntervalAutoMode = IntervalAutoMode.FixedCount;
        //    chart.ChartAreas["ChartArea"].AxisX.Interval = 5;
        //    chart.ChartAreas["ChartArea"].AxisX.Minimum = timeOfInterrogation.AddDays(-30).ToOADate();
        //    chart.ChartAreas["ChartArea"].BorderDashStyle = ChartDashStyle.Solid;
        //    chart.ChartAreas["ChartArea"].BorderColor = Color.Black;

        //    // Show as 2D
        //    chart.ChartAreas["ChartArea"].Area3DStyle.Enable3D = false;
        //    chart.ChartAreas["ChartArea"].AxisX.IntervalAutoMode = IntervalAutoMode.FixedCount;

        //    chart.Width = 1200;
        //    chart.Height = 600;

        //    // Disable the left and right eye sensor series to limit chart to baseline sensor.
        //    //chart.Series["SeriesLeftSensor"].Enabled = false;
        //    //chart.Series["SeriesRightSensor"].Enabled = false;

        //    chart.SaveImage(filename, ChartImageFormat.Png);
        //}

        public ActionResult LogOut()
        {
            // This action/method is a kludge because I couldn't get the "sign out" link to change form Home/Logout to Account/Logout.
            // So this method is located here to call the LogOut action on the AccountController.


            // TODO: Remove after successful port to ASP.NET core.
            //var controller = DependencyResolver.Current.GetService<AccountController>();
            //controller.ControllerContext = new ControllerContext(this.Request.RequestContext, controller);
            //ActionInvoker.InvokeAction(controller.ControllerContext, "LogOut");

            return RedirectToRoute(new
            {
                controller = "Account",
                action = "Logout",
            });
        }
    }
}
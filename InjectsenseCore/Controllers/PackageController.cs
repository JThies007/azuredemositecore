﻿// ************************************************************
// Copyright (C) 2018 Injectsense, Inc. - All Rights Reserved
// ************************************************************
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using Injectsense.DataAccess;
using Injectsense.DataAccess.Repositories;
using Injectsense.DataAccess.Entities;
using Injectsense.Models;
using Injectsense.PackageUtilities;
using Microsoft.ApplicationInsights;
using TimeZoneConverter;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;

namespace Injectsense.Controllers
{
    public class PackageController : Controller
    {
        IUnitOfWork Transaction { get; }
        IDeviceRepository DeviceRepository { get; }
        IPatientRepository PatientRepository { get; }
        ITransmissionRepository TransmissionRepository { get; }

        IHttpContextAccessor HttpContextAccessor;

        public PackageController(IUnitOfWork unitOfWork,
                                 IDeviceRepository deviceRepository,
                                 IPatientRepository patientRepository,
                                 ITransmissionRepository transmissionRepository,
                                 IHttpContextAccessor httpContextAccessor)
        {
            Transaction = unitOfWork;
            DeviceRepository = deviceRepository;
            PatientRepository = patientRepository;
            TransmissionRepository = transmissionRepository;

            HttpContextAccessor = httpContextAccessor;
        }

        // Get: Package
        [HttpGet]
        [Authorize]
        [OutputCache(Duration = 0)]
        public ActionResult Index2(string sensorSerialNumber, string dateRangeStart, string dateRangeStop)
        {
            // Setup to log to application insights.
            TelemetryClient telemetryClient = new TelemetryClient();

            PackageModel model = new PackageModel();

            //DateTime startDate = DateTime.Now; // satisfy uninitialized variable.
            DateTime? queryStartDate = null;
            if (!string.IsNullOrEmpty(dateRangeStart) && DateTime.TryParse(dateRangeStart, out DateTime startDate))
            {
                queryStartDate = startDate;
            }
            else
            {
                queryStartDate = null;
            }

            DateTime? queryStopDate = null;
            if (!string.IsNullOrEmpty(dateRangeStop) && DateTime.TryParse(dateRangeStop, out DateTime stopDate))
            {
                queryStopDate = stopDate;
            }
            else
            {
                queryStopDate = null;
            }

            List<TransmissionListInfo> txList = TransmissionRepository.GetAllTransmissions(sensorSerialNumber, queryStartDate, queryStopDate).OrderByDescending(x => x.ReceivedTimeUTC).ToList();

            // return the values entered to the view
            model.SearchSensorSerialNumber = string.IsNullOrEmpty(sensorSerialNumber) ? "" : sensorSerialNumber;
            model.DateRangeStart = dateRangeStart;
            model.DateRangeStop = dateRangeStop;

            // Get the TimeZone info for Minneapolis
            string localTimeZoneInfo = "Central Standard Time";
            TimeZoneInfo timeZoneInfo = TZConvert.GetTimeZoneInfo(localTimeZoneInfo);
            DateTime currentDateTime = TimeZoneInfo.ConvertTimeFromUtc(DateTime.UtcNow, timeZoneInfo);
            model.CurrentDateTime = currentDateTime.ToString("dd-MMM-yyyy h:mm tt") + " CST";

            foreach(TransmissionListInfo tx in txList)
            {
                model.PackageInfoList.Add(
                    new PackageInfo
                    {
                        ReceivedTimeUTC = tx.ReceivedTimeUTC,
                        ReceivedTimeCST = TimeZoneInfo.ConvertTimeFromUtc(Convert.ToDateTime(tx.ReceivedTimeUTC), timeZoneInfo).ToString(),
                        ReaderSerialNumber = tx.ReaderSerialNumber,
                        SensorSerialNumber1 = tx.SensorSerialNumber1,
                        SensorSerialNumber2 = tx.SensorSerialNumber2,
                        TimeOfInterrogationUTC = tx.TimeOfInterrogationUTC,
                        TimeOfInterrogationCST = TimeZoneInfo.ConvertTimeFromUtc(Convert.ToDateTime(tx.TimeOfInterrogationUTC), timeZoneInfo).ToString(),
                        TransmissionId = tx.TransmissionId.ToString()
                    });
            }

            return View(model);
        }

        // Get: Package
        [HttpGet]
        [Authorize]
        [OutputCache(Duration = 0)]
        public FileContentResult GetPackage(string transmissionId)
        {
            Transmission transmission = TransmissionRepository.GetTransmission(Convert.ToInt64(transmissionId));

            // Determine the filename based on TimeOfInterrogation and sensor serial numbers.
            string filenameString = transmission.TimeOfInterrogationUTC.ToString("MMddyyyy");

            if (!string.IsNullOrEmpty(transmission.DeviceSerialNumber1))
            {
                filenameString = filenameString + "_" + transmission.DeviceSerialNumber1;
            }

            if (!string.IsNullOrEmpty(transmission.DeviceSerialNumber2))
            {
                filenameString = filenameString + "_" + transmission.DeviceSerialNumber2;
            }

            filenameString = filenameString + ".pkg";

            return File(transmission.Package, "application/octet-stream", filenameString);
        }

        // Post: Package
        [HttpPost]
        public ActionResult Index()
        {
            // Setup to log to application insights.
            TelemetryClient telemetryClient = new TelemetryClient();

            //HttpRequestBase request = this.Request;
           
            var headers = HttpContextAccessor.HttpContext.Request.Headers;

            string user = string.Empty;
            string pwd = string.Empty;

            // This is a case-insensitive reference (tested)
            if (headers.ContainsKey("username"))
            {
                user = headers["username"];
            }
            if (headers.ContainsKey("password"))
            {
                pwd = headers["password"];
            }

            if (string.IsNullOrEmpty(user) || string.IsNullOrEmpty(pwd))
            {
                return Unauthorized(); // Http status code 401
            }
            else
            {
                if (!(user.ToLowerInvariant().Equals("mobileapp007") && pwd.Equals("Injectsense2019")))
                {
                    return Unauthorized(); // Http status code 401
                }
            }

            // Check and retrieve the uploaded file.       
            if ((HttpContextAccessor.HttpContext.Request.Form.Files == null) 
                || (HttpContextAccessor.HttpContext.Request.Form.Files.Count == 0))
            {
                telemetryClient.TrackTrace(string.Format("The mobile-app-user: {0}, has not uploaded a file", user));
                return BadRequest(); // Http status code 400
            }

            IFormFile formFile = HttpContextAccessor.HttpContext.Request.Form.Files[0] as IFormFile;
            if (formFile.Length == 0)
            {
                telemetryClient.TrackTrace(string.Format("The mobile-app-user: {0}, has uploaded a file with length=0.", user));
                return BadRequest(); // Http status code 400
            }

            byte[] uploadedPackage = null;
            Package package = null;
            try
            {
                // File is available. Get the file.
                BinaryReader binaryReader = new BinaryReader(formFile.OpenReadStream());
                uploadedPackage = binaryReader.ReadBytes(Convert.ToInt32(formFile.Length));

                // Create a package from the file.
                package = PackageManager.ParseByteArrayToPackage(uploadedPackage);

            }
            catch (Exception ex)
            {
                telemetryClient.TrackTrace(string.Format("The mobile-app-user: {0}, has uploaded a file that failed on parsing the byte array", user));
                telemetryClient.TrackException(ex);
                return BadRequest(); // Http status code 400
            }

            // Find the patient id and device serial numbers.
            // DeviceId = Guid, System Identifier for sensor.
            // SensorId = Manufacturing Id placed in the sensor.
            // Serial Number = String that does not reside in the device, need to get from database.
            (Guid DeviceId, string SensorSerialNumber) sensor1Info = (Guid.Empty, string.Empty);
            (Guid DeviceId, string SensorSerialNumber) sensor2Info = (Guid.Empty, string.Empty);
            Guid patientIdentifier = Guid.Empty;
            try
            {
                // Find the patient the device serial number maps to.
                (string SensorId1, string SensorId2) results = PackageManager.GetSensorIdsFromPackage(package);

                bool patientIdFound = false;
                if (!string.IsNullOrEmpty(results.SensorId1))
                {
                    sensor1Info = DeviceRepository.GetSerialNumber(results.SensorId1);
                    patientIdentifier = PatientRepository.GetPatientFromDeviceId(sensor1Info.DeviceId);
                    patientIdFound = true;
                }
                if (!string.IsNullOrEmpty(results.SensorId2))
                {
                    sensor2Info = DeviceRepository.GetSerialNumber(results.SensorId2);

                    if (!patientIdFound)
                    {
                        patientIdentifier = PatientRepository.GetPatientFromDeviceId(sensor2Info.DeviceId);
                        patientIdFound = true;
                    }
                }
            }
            catch (Exception ex)
            {
                telemetryClient.TrackTrace(string.Format("The mobile-app-user: {0}, has uploaded a file that failed on finding patient id", user));
                telemetryClient.TrackException(ex);
                return BadRequest(); // Http status code 400
            }

            // Check for duplicates and do not save if this is a duplicate transmission.
            if (TransmissionRepository.IsDuplicateTransmission(Convert.ToDateTime(package.TimeOfInterrogationUTC), sensor1Info.SensorSerialNumber, sensor2Info.SensorSerialNumber))
            {
                telemetryClient.TrackTrace(string.Format("The mobile-app-user: {0}, has uploaded duplicate transmission/package. Sensor 1 Serial Number: {1}, Sensor 2 Serial Number: {2}, Time Of Interrogation: {3}", 
                    user, sensor1Info.SensorSerialNumber, sensor2Info.SensorSerialNumber, package.TimeOfInterrogationUTC));
                return Ok(); // Http status code 200
            }

            // Save the Transmission.
            TransmissionRepository.SaveTransmissionData(patientIdentifier, sensor1Info.SensorSerialNumber, sensor2Info.SensorSerialNumber, 
                                   DateTime.UtcNow, Convert.ToDateTime(package.TimeOfInterrogationUTC), uploadedPackage);

            // TODO: Will need to run alg. to convert from binary file uploaded
            // to presentation data saved in database.

            Transaction.Commit();

            telemetryClient.Flush();

            return Ok(); // Http status code 200
        }
    }
}
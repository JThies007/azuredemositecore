﻿// ************************************************************
// Copyright (C) 2018 Injectsense, Inc. - All Rights Reserved
// ************************************************************
using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using Injectsense.Helpers;
using Injectsense.DataAccess.Repositories;
using Injectsense.DataAccess.Entities;
using Injectsense.DataAccess;
using Injectsense.Models;
using TimeZoneConverter;
using Microsoft.AspNetCore.Authorization;
using Microsoft.Extensions.DependencyInjection;
using System.Security.Claims;

namespace Injectsense.Controllers
{
    [Authorize]
    [OutputCache(Duration = 0)]
    public class ManageMyPatientsController : Controller
    {
        IUnitOfWork Transaction { get; }
        IClinicRepository ClinicRepository { get; }
        IPatientRepository PatientRepository { get; }
        ITransmissionRepository TransmissionRepository { get; }
        IDeviceRepository DeviceRepository { get; }

        private readonly IServiceProvider Services;

        public ManageMyPatientsController(IUnitOfWork unitOfWork,
                                          IClinicRepository clinicRepository, 
                                          IPatientRepository patientRepository,
                                          ITransmissionRepository transmissionRepository, 
                                          IDeviceRepository deviceRepository,
                                          IServiceProvider services)
        {
            Transaction = unitOfWork;
            ClinicRepository = clinicRepository;
            PatientRepository = patientRepository;
            TransmissionRepository = transmissionRepository;
            DeviceRepository = deviceRepository;

            Services = services;
        }

        // GET: ManageMyPatients
        public ActionResult Index()
        {
            MainContext mainContext = Services.GetRequiredService<MainContext>();

            // We could put the clinic id in the html and get it back on this call.
            // But the goal is to leave as little info as possible in html.
            // So let's get user and map to their clinic, and from clinic, we'll get list of patients.
            ClaimsPrincipal currentUser = this.User;
            string currentUserName = currentUser.FindFirst(ClaimTypes.Name).Value;

            (Guid ClinicId, Guid ClinicStaffId, string ClinicName, string ClinicStaffFirstName, string ClinicStaffLastName) clinicInfo  = ClinicStaffHelper.GetUsersClinicIdFromRequest(currentUserName, mainContext);

            // Get the list of patients for the clinic.
            List<Patient> patientList = ClinicRepository.GetAllPatients(clinicInfo.ClinicId);

            // Get the TimeZone info for the clinic.
            string localTimeZoneInfo = ClinicRepository.GetClinicDotNetTimeZoneInfo(clinicInfo.ClinicId);
            TimeZoneInfo timeZoneInfo = TZConvert.GetTimeZoneInfo(localTimeZoneInfo);

            // Get the patients that have transmission information.
            // List<ClinicPatientAssociation> ClinicPatientInfoList = ClinicRepository.GetAllPatientsWithNewTransmissionsForClinic(clinicInfo.ClinicId);

            // Create a view model and populate it.
            ManageMyPatientsModel model = new ManageMyPatientsModel();
            model.Initialize(currentUserName, mainContext);

            foreach (Patient p in patientList)
            {
                ClinicPatientAssociation cpa = ClinicRepository.GetClinicPatientAssociation(clinicInfo.ClinicId, p.Id);
                (Guid DeviceId, string ImplantEye, DateTime? ImplantDate) leftEyeInfo = PatientRepository.GetImplantedDeviceInfo(cpa.MostRecentTransmissionLeftEyeDSN);
                (Guid DeviceId, string ImplantEye, DateTime? ImplantDate) rightEyeInfo = PatientRepository.GetImplantedDeviceInfo(cpa.MostRecentTransmissionRightEyeDSN);

                // TODO: Need to handle case where there are no devices or 1 device.
                // Need to set a flag in model that left and/or right devices are not available.
                bool hasSensorInLeftEye = !Guid.Empty.Equals(leftEyeInfo.DeviceId);
                bool hasSensorInRightEye = !Guid.Empty.Equals(rightEyeInfo.DeviceId);

                DateTime lastReceivedDateTimeLocalized;
                string lastReceivedDateTimeLocalizedString;
                if (cpa.MostRecentTransmissionReceivedDateUTC.HasValue)
                {
                    lastReceivedDateTimeLocalized = TimeZoneInfo.ConvertTimeFromUtc(cpa.MostRecentTransmissionReceivedDateUTC.Value, timeZoneInfo);
                    lastReceivedDateTimeLocalizedString = lastReceivedDateTimeLocalized.ToString("dd-MMM-yyyy h:mm tt");
                }
                else
                {
                    lastReceivedDateTimeLocalizedString = "";
                }

                // Take into account Last Office Visit may be null.
                DateTime? lastOfficeVisit = ClinicRepository.GetLastOfficeVisitDate(clinicInfo.ClinicId, p.Id);
                string lastOfficeVisitString;
                if (lastOfficeVisit.HasValue)
                {
                    lastOfficeVisitString = lastOfficeVisit.Value.ToString("dd-MMM-yyyy");
                }
                else
                {
                    lastOfficeVisitString = "";
                }

                model.PatientList.Add(new ManageMyPatientInfo
                {
                    MostRecentTransmissionId = cpa.MostRecentTransmissionId,
                    PatientName = p.NameLast + ", " + p.NameFirst,
                    PatientId = p.Id,
                    DateOfBirthString = p.DateOfBirthNotUTC.ToString("dd-MMM-yyyy"),
                    LastReceivedTimestampString = lastReceivedDateTimeLocalizedString,
                    IdNumber = p.Identifier,
                    LeftEyeSerialNumber = hasSensorInLeftEye ? cpa.MostRecentTransmissionLeftEyeDSN : "",
                    RightEyeSerialNumber = hasSensorInRightEye ? cpa.MostRecentTransmissionRightEyeDSN : "",
                    LastOfficeVisitString = lastOfficeVisitString,
                    TotalNumberOfTransmissions = cpa.ViewableTransmissionCount,
                });
            }

            return View(model);
        }

        public ActionResult LogOut()
        {
            // This action/method is a kludge because I couldn't get the "sign out" link to change form Home/Logout to Account/Logout.
            // So this method is located here to call the LogOut action on the AccountController.

            // TODO: Remove after successful port to ASP.NET core.
            //var controller = DependencyResolver.Current.GetService<AccountController>();
            //controller.ControllerContext = new ControllerContext(this.Request.RequestContext, controller);
            //ActionInvoker.InvokeAction(controller.ControllerContext, "LogOut");

            return RedirectToRoute(new
            {
                controller = "Account",
                action = "Logout",
            });
        }
    }
}
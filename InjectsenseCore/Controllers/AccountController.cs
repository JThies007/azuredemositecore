﻿// ************************************************************
// Copyright (C) 2018 Injectsense, Inc. - All Rights Reserved
// ************************************************************
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading.Tasks;
using Injectsense.DataAccess.Entities;
using Injectsense.Models;

namespace Injectsense.Controllers
{
    [AllowAnonymous]
    [OutputCache(Duration = 0)]
    public class AccountController : Controller
    {
        public readonly SignInManager<User> SignInManager;
        public readonly IServiceProvider Services;
        public readonly IHttpContextAccessor HttpContextAccessor;

        public AccountController(SignInManager<User> signInManager,
                                 IServiceProvider services,
                                 IHttpContextAccessor httpContextAccessor)
        {
            SignInManager = signInManager;
            Services = services;
            HttpContextAccessor = httpContextAccessor;
        }

        // GET: Account  
        // They hit the site - they get the login page (view).
        [AllowAnonymous]
        public ActionResult Index()
        {
            return View();
        }

        // They hit the site after filling in the login credentials
        [AllowAnonymous]
        [HttpGet]
        public ActionResult Login(string ReturnUrl = "")
        {
            ViewBag.ReturnUrl = ReturnUrl;
            return View();
        }

        [AllowAnonymous]
        [HttpPost]
        public async Task<ActionResult> Login(LoginModel loginModel, string ReturnUrl = "")
        {
            if (ModelState.IsValid)
            {
                //authenticate

                var result = await SignInManager.PasswordSignInAsync(loginModel.UserName, loginModel.Password, false, false);
                if (result.Succeeded)
                {
                    // Successful, onward to home page.
                    return RedirectToAction("Index", "Home");
                }
            }

            // Failed to login.
            ModelState.AddModelError("", "Invalid Username or Password.");
            return View(loginModel);
        }

        [AllowAnonymous]
        public ActionResult LogOut()
        {
            Response.Cookies.Append(
                "isCookie",
                "",
                new CookieOptions()
                {
                    Expires = DateTime.Now.AddYears(-1)
                }
            );

            SignOut();
            return RedirectToAction("Login", "Account", null);
        }
    }
}

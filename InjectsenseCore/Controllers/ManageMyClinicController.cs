﻿// ************************************************************
// Copyright (C) 2018 Injectsense, Inc. - All Rights Reserved
// ************************************************************
using System;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using Injectsense.Models;
using Injectsense.Helpers;
using Injectsense.DataAccess.Repositories;
using Injectsense.DataAccess.Entities;
using Injectsense.DataAccess;
using System.Text.RegularExpressions;
using Microsoft.AspNetCore.Authorization;
using Microsoft.Extensions.DependencyInjection;
using System.Security.Claims;

namespace Injectsense.Controllers
{
    [Authorize]
    [OutputCache(Duration = 0)]
    public class ManageMyClinicController : Controller
    {
        IUnitOfWork Transaction { get; }
        IClinicRepository ClinicRepository { get; }

        private readonly IServiceProvider Services;

        public ManageMyClinicController(IUnitOfWork unitOfWork, 
                                        IClinicRepository clinicRepository, 
                                        IServiceProvider services)
        {
            Transaction = unitOfWork;
            ClinicRepository = clinicRepository;

            Services = services;
        }

        // GET: ManageMyClinic
        public ActionResult Index()
        {
            MainContext mainContext = Services.GetRequiredService<MainContext>();

            ClaimsPrincipal currentUser = this.User;
            string currentUserName = currentUser.FindFirst(ClaimTypes.Name).Value;

            (Guid ClinicId, Guid ClinicStaffId, string ClinicName, string ClinicStaffFirstName, string ClinicStaffLastName) clinicInfo = ClinicStaffHelper.GetUsersClinicIdFromRequest(currentUserName, mainContext);

            ManageMyClinicModel model = new ManageMyClinicModel();
            model.Initialize(currentUserName, mainContext);

            PopulateModel(model);

            return View(model);
        }

        private void PopulateModel(ManageMyClinicModel model)
        {
            Clinic clinicInfo;

            try
            {
                clinicInfo = ClinicRepository.Find(x => x.Id == model.ClinicId).Single();
            }
            catch (Exception ex)
            {
                throw new Exception("Clinic Id not found.", ex);
            }
 
            model.ClinicNamePrimary = clinicInfo.NamePrimary;
            model.ClinicAddressLine1 = clinicInfo.AddressLine1;
            model.ClinicAddressLine2 = clinicInfo.AddressLine2;
            model.City = clinicInfo.City;
            model.State = clinicInfo.StateCode;
            model.Zipcode = clinicInfo.Zipcode;
            model.PhonePrimary = clinicInfo.PrimaryPhone;
            model.PrimaryContactFirstName = clinicInfo.PrimaryContactFirstName;
            model.PrimaryContactLastName = clinicInfo.PrimaryContactLastName;
            model.PrimaryContactEmailAddress = clinicInfo.PrimaryContactEmailAddress;
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Index(ManageMyClinicModel model)
        {
            // If the IsClinicProfile tab is true then the user has just posted
            // the Clinic Profile updates - so just focus on these fields for now
            // for validation and for saving to database.
            // If the IsClinicProfile tab is false then the user has just posted
            // the Primary Contact information.
            if (model.IsClinicProfileTab.ToLower().Equals("true"))
            {
                // The max string lengths are enforced in the view.
                if ((model.ClinicNamePrimary == null) || (model.ClinicNamePrimary.Length <= 0))
                {
                    ModelState.AddModelError("clinicNamePrimary", "Clinic Name is required.");
                }

                if ((model.ClinicAddressLine1 == null) || (model.ClinicAddressLine1.Length <= 0))
                {
                    ModelState.AddModelError("clinicAddressLine1", "Address is required.");
                }

                if ((model.City == null) || (model.City.Length <= 0))
                {
                    ModelState.AddModelError("city", "City is required.");
                }

                if ((model.State == null) || (model.State.Length <= 0))
                {
                    ModelState.AddModelError("state", "State is required.");
                }

                if ((model.Zipcode == null) || (model.Zipcode.Length <= 0))
                {
                    ModelState.AddModelError("zipcode", "Zipcode is required.");
                }
                else
                {
                    string pattern = @"^\d{5}$";
                    Regex zipcodeRegEx = new Regex(pattern, RegexOptions.IgnoreCase);
                    Match m = zipcodeRegEx.Match(model.Zipcode);
                    if (!m.Success)
                    {
                        ModelState.AddModelError("zipcode", "Zipcode is invalid.");
                    }
                }

                if ((model.PhonePrimary == null) || (model.PhonePrimary.Length <= 0))
                {
                    ModelState.AddModelError("phonePrimary", "Phone Number is required.");
                }
                else
                {
                    // Verify only digits have been entered for phone number.
                    string pattern = @"((\(\d{3}\) ?)|(\d{3}-))?\d{3}-\d{4}";
                    Regex phoneNumberRegEx = new Regex(pattern, RegexOptions.IgnoreCase);
                    Match m = phoneNumberRegEx.Match(model.PhonePrimary);
                    if (!m.Success)
                    {
                        ModelState.AddModelError("phonePrimary", "Phone Number is invalid.");
                    }
                }
            }
            else
            {
                // The submit is the primary contact tab.
                // Just validate fields on the primary contact tab.
                if ((model.PrimaryContactFirstName == null) || (model.PrimaryContactFirstName.Length <= 0))
                {
                    ModelState.AddModelError("primaryContactFirstName", "First Name is required.");
                }

                if ((model.PrimaryContactLastName == null) ||(model.PrimaryContactLastName.Length <= 0))
                {
                    ModelState.AddModelError("primaryContactLastName", "Last Name is required.");
                }

                if ((model.PrimaryContactEmailAddress == null) || (model.PrimaryContactEmailAddress.Length <= 0))
                {
                    ModelState.AddModelError("primaryContactEmailAddress", "Email Address is required.");
                }
                else
                {
                    string pattern = @"(\w+?@\w+?\x2E.+)";
                    Regex emailRegEx = new Regex(pattern, RegexOptions.IgnoreCase);
                    Match m = emailRegEx.Match(model.PrimaryContactEmailAddress);
                    if (!m.Success)
                    {
                        ModelState.AddModelError("primaryContactEmailAddress", "Email Address is invalid.");
                    }
                }

            }


            if (ModelState.IsValid)
            {
                // Save and then re-populate the model and return to view.
                // Need to check the tab that is being edited and save based on that.
                Guid clinicId = Guid.Parse(model.ClinicIdString);

                if (model.IsClinicProfileTab.ToLower().Equals("true"))
                {
                    // FInd the clinic entity and save the specific fields that were available on form.
                    Clinic clinicEntity = ClinicRepository.Find(x => x.Id == clinicId).Single();

                    clinicEntity.NamePrimary = model.ClinicNamePrimary;
                    clinicEntity.AddressLine1 = model.ClinicAddressLine1;
                    clinicEntity.AddressLine2 = model.ClinicAddressLine2;
                    clinicEntity.City = model.City;
                    clinicEntity.StateCode = model.State;
                    clinicEntity.Zipcode = model.Zipcode;
                    clinicEntity.PrimaryPhone = model.PhonePrimary;

                    Transaction.Commit();
                }
                else
                {
                    // FInd the clinic entity and save the specific fields that were available on form.
                    Clinic clinicEntity = ClinicRepository.Find(x => x.Id == clinicId).Single();

                    clinicEntity.PrimaryContactFirstName = model.PrimaryContactFirstName;
                    clinicEntity.PrimaryContactLastName = model.PrimaryContactLastName;
                    clinicEntity.PrimaryContactEmailAddress = model.PrimaryContactEmailAddress;

                    Transaction.Commit();
                }

                    return RedirectToAction("Index");
            }
            else
            {
                // The model is returned here which may have invalid values entered and error statuses.
                return View(model);
            }
        }

        public ActionResult LogOut()
        {
            // This action/method is a kludge because I couldn't get the "sign out" link to change form Home/Logout to Account/Logout.
            // So this method is located here to call the LogOut action on the AccountController.

            // TODO: Remove after successful port to ASP.NET core.
            //var controller = DependencyResolver.Current.GetService<AccountController>();
            //controller.ControllerContext = new ControllerContext(this.Request.RequestContext, controller);
            //ActionInvoker.InvokeAction(controller.ControllerContext, "LogOut");

            return RedirectToRoute(new
            {
                controller = "Account",
                action ="Logout",
            });
        }
    }
}
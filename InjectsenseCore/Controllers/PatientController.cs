﻿// ************************************************************
// Copyright (C) 2018 Injectsense, Inc. - All Rights Reserved
// ************************************************************
using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using Injectsense.DataAccess.Repositories;
using Injectsense.Models;
using Injectsense.Helpers;
using Injectsense.DataAccess.Entities;
using Injectsense.DataAccess;
using System.Text.RegularExpressions;
using TimeZoneConverter;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.Extensions.DependencyInjection;
using System.Security.Claims;

namespace Injectsense.Controllers
{
    enum DeviceUpdateType
    {
        NoChange,
        NewDevice,
        ReplaceDevice,
        DeleteDevice
    };

    [Authorize]
    [OutputCache(Duration = 0)]
    public class PatientController : Controller
    {
        IUnitOfWork Transaction { get; }
        IPatientRepository PatientRepository { get; }
        IDeviceRepository DeviceRepository { get; }
        IClinicRepository ClinicRepository { get; }

        private readonly IServiceProvider Services;

        public PatientController(IUnitOfWork unitOfWork,
                                 IPatientRepository patientRepository, 
                                 IDeviceRepository deviceRepository, 
                                 IClinicRepository clinicRepository,
                                 IServiceProvider services)
        {
            Transaction = unitOfWork;
            PatientRepository = patientRepository;
            DeviceRepository = deviceRepository;
            ClinicRepository = clinicRepository;

            Services = services;
        }

        // GET: Patient
        public ActionResult Index(string patientIdString)
        {
            PatientModel model = PopulatePatientModel(patientIdString);

            return View(model);
        }

        // GET: Create Patient
        [HttpGet]
        public ActionResult Create()
        {
            PatientModel model = new PatientModel();
            model.PatientInfoEntryOnly = true;

            int tempDeviceModelSelectId = 0;

            model.DeviceImplantLocationListRow1 = GetLocationListForDevice("");
            model.DeviceImplantLocationSelectIdRow1 = -1;

            model.DeviceModelListRow1 = GetDeviceModelListForDevice(Guid.Empty, out tempDeviceModelSelectId);
            model.DeviceModelSelectIdRow1 = -1;

            model.DeviceImplantLocationListRow2 = GetLocationListForDevice("");
            model.DeviceImplantLocationSelectIdRow2 = -1;

            model.DeviceModelListRow2 = GetDeviceModelListForDevice(Guid.Empty, out tempDeviceModelSelectId);
            model.DeviceModelSelectIdRow2 = -1;

            return View("Index", model);
        }

        // POST: Create Patient
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(PatientModel model)
        {
            // The max string lengths are enforced in the view.
            if ((model.NameFirst == null) || (model.NameFirst.Length <= 0))
            {
                ModelState.AddModelError("nameFirst", "First Name is required.");
            }

            // The max string lengths are enforced in the view.
            if ((model.NameLast == null) || (model.NameLast.Length <= 0))
            {
                ModelState.AddModelError("nameLast", "Last Name is required.");
            }

            if ((model.DateOfBirth == null) || (model.DateOfBirth.Length <= 10))
            {
                ModelState.AddModelError("dateOfBirth", "Date of Birth is required.");
            }
            else
            {
                string pattern = @"^(0[1-9]|[1-9]|[12][0-9]|3[01])-([Jj][Aa][Nn]|[Ff][Ee][Bb]|[Mm][Aa][Rr]|[Aa][Pp][Rr]|[Mm][Aa][Yy]|[Jj][Uu][Nn]|[Jj][Uu][Lj]|[Aa][Uu][Gg]|[Ss][Ee][Pp]|[Oo][Cc][Tt]|[Nn][Oo][Vv]|[Dd][Ee][Cc])-(19|20)\d\d$";
                Regex dateRegEx = new Regex(pattern, RegexOptions.IgnoreCase);
                Match m = dateRegEx.Match(model.DateOfBirth);
                if (!m.Success)
                {
                    ModelState.AddModelError("dateOfBirth", "Date of Birth is invalid.");
                }

                // We've passed the string-test, now verify we can convert to DateTime and valid date range.
                DateTime dateOfBirth = DateTime.MinValue;
                if (!DateTime.TryParse(model.DateOfBirth, out dateOfBirth))
                {
                    ModelState.AddModelError("DateOfBirth", "Invalid date entered.");
                }
                else if ((dateOfBirth.Year == 1) || (dateOfBirth.Year < 1900))
                {
                    ModelState.AddModelError("DateOfBirth", "Invalid date entered.");
                }
            }

            // for Patient Identifier allow:
            // letters(upper or lowercase)
            // numbers(0-9)
            // underscore(_)
            // dash(-)
            // point(.)
            //// no spaces! or other characters
            if (!(model.Identifier == null) && (model.Identifier.Length > 0))
            {
                string pattern = @"^[a-zA-Z0-9_.-]*$";
                Regex patientIdRegEx = new Regex(pattern, RegexOptions.IgnoreCase);
                Match m = patientIdRegEx.Match(model.Identifier);
                if (!m.Success)
                {
                    ModelState.AddModelError("identifier", "Patient ID is invalid.");
                }
            }

            if ((model.PhoneHome == null) || (model.PhoneHome.Length <= 0))
            {
                ModelState.AddModelError("phoneHome", "Home Phone is required.");
            }
            else
            {
                // This accepts spaces, no spaces, no dashes and dashes.
                // string pattern = @"^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$";
                // THis accepts only ###-###-####, where you can't have 000- or 123-
                string pattern = @"^[2-9]\d{2}-\d{3}-\d{4}$";
                Regex phoneRegEx = new Regex(pattern, RegexOptions.IgnoreCase);
                Match m = phoneRegEx.Match(model.PhoneHome);
                if (!m.Success)
                {
                    ModelState.AddModelError("phoneHome", "Home Phone is invalid (###-###-####).");
                }
            }

            if (model.Gender == null)
            {
                ModelState.AddModelError("gender", "Gender is required.");
            }

            // Need to re-populate the drop-down lists.
            int deviceModelSelectId = 0;
            model.DeviceImplantLocationListRow1 = GetLocationListForDevice("");
            model.DeviceModelListRow1 = GetDeviceModelListForDevice(Guid.Empty, out deviceModelSelectId);

            model.DeviceImplantLocationListRow2 = GetLocationListForDevice("");
            model.DeviceModelListRow2 = GetDeviceModelListForDevice(Guid.Empty, out deviceModelSelectId);

            if (ModelState.IsValid)
            {
                // Find the patient entity and save the specific fields that were available on form.
                Patient patientEntity = new Patient();

                // TODO: if not a demo, this should check if guid exists or catch exception.
                patientEntity.Id = Guid.NewGuid();

                patientEntity.NameFirst = model.NameFirst;
                patientEntity.NameLast = model.NameLast;
                patientEntity.NameMiddle = model.NameMiddle;
                patientEntity.DateOfBirthNotUTC = Convert.ToDateTime(model.DateOfBirth);
                patientEntity.Identifier = model.Identifier;
                patientEntity.PhoneHome = model.PhoneHome;
                patientEntity.Gender = model.Gender.ToLower().Equals("male") ? "M" : "F";

                // TODO: Should remove required state on this.
                patientEntity.ApplicationUsername = "NA";

                PatientRepository.Add(patientEntity);

                // Need to associate patients with clinic.
                ClaimsPrincipal currentUser = this.User;
                string currentUserName = currentUser.FindFirst(ClaimTypes.Name).Value;
                (Guid ClinicId, Guid ClinicStaffId, string ClinicName, string ClinicStaffFirstName, string ClinicStaffLastName) clinicInfo = ClinicStaffHelper.GetUsersClinicIdFromRequest(currentUserName, Services.GetRequiredService<MainContext>());

                ClinicRepository.AssociatePatient(clinicInfo.ClinicId, patientEntity.Id, clinicInfo.ClinicStaffId, clinicInfo.ClinicStaffId);

                Transaction.Commit();

                // Need to set the patient name now that it's been saved.
                model.NameCombined = model.NameLast + ", " + model.NameFirst;

                // We've passed the validation for patient info and saved to database.
                // Now make the equipment tab available.
                model.PatientInfoEntryOnly = false;
            }
            else
            {
                // We've failed validation of patient information.
                // Can not allow access to equipment tab until patient information created.
                model.PatientInfoEntryOnly = true;
            }

            // This will enable the equipment tab.
            return View("Index", model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Index(PatientModel model)
        {
            bool row1Validate = true;
            bool row2Validate = true;
            DeviceUpdateType row1DeviceSerialNumberUpdateType = DeviceUpdateType.NewDevice;
            DeviceUpdateType row2DeviceSerialNumberUpdateType = DeviceUpdateType.NewDevice;

            ClaimsPrincipal currentUser = this.User;
            string currentUserName = currentUser.FindFirst(ClaimTypes.Name).Value;

            (Guid ClinicId, Guid ClinicStaffId, string ClinicName, string ClinicStaffFirstName, string ClinicStaffLastName) clinicInfo = ClinicStaffHelper.GetUsersClinicIdFromRequest(currentUserName, Services.GetRequiredService<MainContext>());

            // If the IsPatientInfo tab is true then the user has just posted
            // the Patient Info updates - so just focus on these fields for now
            // for validation and for saving to database.
            // If the IsPaitentInfo tab is false then the user has just posted
            // the Equipment information.
            if (model.IsPatientInfoTab.ToLower().Equals("true"))
            {
                // The max string lengths are enforced in the view.
                if ((model.NameFirst == null) || (model.NameFirst.Length <= 0))
                {
                    ModelState.AddModelError("nameFirst", "First Name is required.");
                }

                // The max string lengths are enforced in the view.
                if ((model.NameLast == null) || (model.NameLast.Length <= 0))
                {
                    ModelState.AddModelError("nameLast", "Last Name is required.");
                }

                if ((model.DateOfBirth == null) || (model.DateOfBirth.Length <= 10))
                {
                    ModelState.AddModelError("dateOfBirth", "Date of Birth is required.");
                }
                else
                {
                    string pattern = @"^(0[1-9]|[1-9]|[12][0-9]|3[01])-([Jj][Aa][Nn]|[Ff][Ee][Bb]|[Mm][Aa][Rr]|[Aa][Pp][Rr]|[Mm][Aa][Yy]|[Jj][Uu][Nn]|[Jj][Uu][Lj]|[Aa][Uu][Gg]|[Ss][Ee][Pp]|[Oo][Cc][Tt]|[Nn][Oo][Vv]|[Dd][Ee][Cc])-(19|20)\d\d$";
                    Regex dateRegEx = new Regex(pattern, RegexOptions.IgnoreCase);
                    Match m = dateRegEx.Match(model.DateOfBirth);
                    if (!m.Success)
                    {
                        ModelState.AddModelError("dateOfBirth", "Date of Birth is invalid.");
                    }

                    // We've passed the string-test, now verify we can convert to DateTime and valid date range.
                    DateTime dateOfBirth = DateTime.MinValue;
                    if (!DateTime.TryParse(model.DateOfBirth, out dateOfBirth))
                    {
                        ModelState.AddModelError("DateOfBirth", "Invalid date entered.");
                    }
                    else if ((dateOfBirth.Year == 1) || (dateOfBirth.Year < 1900))
                    {
                        ModelState.AddModelError("DateOfBirth", "Invalid date entered.");
                    }
                }

                // for Patient Identifier allow:
                // letters(upper or lowercase)
                // numbers(0-9)
                // underscore(_)
                // dash(-)
                // point(.)
                //// no spaces! or other characters
                if (!(model.Identifier == null) && (model.Identifier.Length > 0))
                {
                    string pattern = @"^[a-zA-Z0-9_.-]*$";
                    Regex patientIdRegEx = new Regex(pattern, RegexOptions.IgnoreCase);
                    Match m = patientIdRegEx.Match(model.Identifier);
                    if (!m.Success)
                    {
                        ModelState.AddModelError("identifier", "Patient ID is invalid.");
                    }
                }

                if ((model.PhoneHome == null) || (model.PhoneHome.Length <= 0))
                {
                    ModelState.AddModelError("phoneHome", "Home Phone is required.");
                }
                else
                {
                    // This accepts spaces, no spaces, no dashes and dashes.
                    // string pattern = @"^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$";
                    // THis accepts only ###-###-####, where you can't have 000- or 123-
                    string pattern = @"^[2-9]\d{2}-\d{3}-\d{4}$";
                    Regex phoneRegEx = new Regex(pattern, RegexOptions.IgnoreCase);
                    Match m = phoneRegEx.Match(model.PhoneHome);
                    if (!m.Success)
                    {
                        ModelState.AddModelError("phoneHome", "Home Phone is invalid (###-###-####).");
                    }
                }

                if (model.Gender == null)
                {
                    ModelState.AddModelError("gender", "Gender is required.");
                }
            }
            else
            {
                // First check for equipment,
                // If there's no patient - then fail the validation and say so.
                // This should never be hit.
                if (model.PatientId.Equals(Guid.Empty))
                {
                    ModelState.AddModelError("", "Can not enter device data before creating patient.");

                    // An early exit.
                    // Whichever way we leave, we need to re-populate the drop-down lists.
                    int deviceModelSelectId = 0;
                    model.DeviceImplantLocationListRow1 = GetLocationListForDevice("");
                    model.DeviceModelListRow1 = GetDeviceModelListForDevice(Guid.Empty, out deviceModelSelectId);

                    model.DeviceImplantLocationListRow2 = GetLocationListForDevice("");
                    model.DeviceModelListRow2 = GetDeviceModelListForDevice(Guid.Empty, out deviceModelSelectId);

                    return View(model);
                }

                // Validate the Patient Equipment tab changes.

                // Validate the first row.

                // Determine row 1 device serial number update type.
                // This is key because change in device serial number triggers creation/update/deletion of device.
                row1DeviceSerialNumberUpdateType = GetDeviceUpdateStatus(model.DeviceSerialNumberRow1Hidden, model.DeviceSerialNumberRow1);
                row2DeviceSerialNumberUpdateType = GetDeviceUpdateStatus(model.DeviceSerialNumberRow2Hidden, model.DeviceSerialNumberRow2);


                // Validate Row 1

                if (!(row1DeviceSerialNumberUpdateType == DeviceUpdateType.DeleteDevice))
                {
                    // Validate the rest of the row1 fields.

                    // For the device model number verify the list index is valid given the list provided to user.
                    // This is not really possible to hit unless the site is hit outside of normal page.
                    if (!(model.DeviceModelListRow1.Any(x => x.Value == model.DeviceModelSelectIdRow1.ToString())))
                    {
                        ModelState.AddModelError("DeviceModelListRow1", "Model Number is invalid.");
                        row1Validate = false;
                    }

                    // Validate the device serial number.
                    // We've already checked and handled the dsn that has moved to deletion.
                    // This is a little forceful but if you post and dsn row 1 is empty -> then invalid.
                    if ((string.IsNullOrEmpty(model.DeviceSerialNumberRow1)) || (model.DeviceSerialNumberRow1.Length <= 0))
                    {
                        ModelState.AddModelError("deviceSerialNumberRow1", "Serial Number is invalid.");
                        row1Validate = false;
                    }
                    else
                    {
                        string pattern = @"^[a-zA-Z]{3}[0-9]{5}[a-zA-Z]{1}";
                        Regex serialNumberRegEx = new Regex(pattern, RegexOptions.IgnoreCase);
                        Match m = serialNumberRegEx.Match(model.DeviceSerialNumberRow1);
                        if (!m.Success)
                        {
                            ModelState.AddModelError("deviceSerialNumberRow1", "Serial Number is invalid.");
                            row1Validate = false;
                        }
                        else
                        {
                            // Verify the serial number prefix is valid for the device type.
                            string devicePrefixString = DeviceRepository.GetModelSerialNumberPrefix(model.DeviceModelListRow1.Find(x => x.Value == model.DeviceModelSelectIdRow1.ToString()).Text);
                            if (!devicePrefixString.Equals(model.DeviceSerialNumberRow1.Substring(0, 3)))
                            {
                                ModelState.AddModelError("deviceSerialNumberRow1", string.Format("Prefix invalid (Use {0}...)", devicePrefixString));
                                row1Validate = false;
                            }
                            // Do a final check to see if the Device Serial Number has already been used (is available).
                            else
                            {
                                if (((row1DeviceSerialNumberUpdateType == DeviceUpdateType.NewDevice) || (row1DeviceSerialNumberUpdateType == DeviceUpdateType.ReplaceDevice))
                                    && (!DeviceRepository.DeviceIsAvailable(model.DeviceSerialNumberRow1)))
                                {
                                    ModelState.AddModelError("deviceSerialNumberRow1", "Serial Number is not available.");
                                    row1Validate = false;
                                }
                            }
                        }
                    }

                    // Validate the row 1 date of implant.
                    if (row1Validate && ((model.DeviceImplantDateRow1 == null) || (model.DeviceImplantDateRow1.Length <= 0)))
                    {
                        ModelState.AddModelError("deviceImplantDateRow1", "Implant Date is invalid.");
                    }
                    else if (row1Validate)
                    {
                        string pattern = @"^(0[1-9]|[1-9]|[12][0-9]|3[01])-([Jj][Aa][Nn]|[Ff][Ee][Bb]|[Mm][Aa][Rr]|[Aa][Pp][Rr]|[Mm][Aa][Yy]|[Jj][Uu][Nn]|[Jj][Uu][Lj]|[Aa][Uu][Gg]|[Ss][Ee][Pp]|[Oo][Cc][Tt]|[Nn][Oo][Vv]|[Dd][Ee][Cc])-(19|20)\d\d$";
                        Regex dateRegEx = new Regex(pattern, RegexOptions.IgnoreCase);
                        Match m = dateRegEx.Match(model.DeviceImplantDateRow1);
                        if (!m.Success)
                        {
                            ModelState.AddModelError("deviceImplantDateRow1", "Implant Date is invalid.");
                        }

                        // We've passed the string-test, now verify we can convert to DateTime and valid date range.
                        DateTime dateOfImplant = DateTime.MinValue;
                        if (!DateTime.TryParse(model.DeviceImplantDateRow1, out dateOfImplant))
                        {
                            ModelState.AddModelError("deviceImplantDateRow1", "Implant Date is invalid.");
                        }
                        else if ((dateOfImplant.Year == 1) || (dateOfImplant.Year < 1900))
                        {
                            ModelState.AddModelError("deviceImplantDateRow1", "Implant Date is invalid.");
                        }
                    }

                    // Validate the row 1 implant location.
                    // For the device implant location verify the list index is valid given the list provided to user.
                    // This is not really possible to hit unless the site is hit outside of normal page.
                    if (row1Validate && !(model.DeviceImplantLocationListRow1.Any(x => x.Value == model.DeviceImplantLocationSelectIdRow1.ToString())))
                    {
                        ModelState.AddModelError("deviceImplantLocationListRow1", "Implant Location is invalid.");
                    }

                }

                // Validate Row 2
                if (!(row2DeviceSerialNumberUpdateType == DeviceUpdateType.DeleteDevice))
                {
                    // Validate the rest of the row2 fields.

                    // For the device model number verify the list index is valid given the list provided to user.
                    // This is not really possible to hit unless the site is hit outside of normal page.
                    // We only get the Row1 list back since row1 and row2 have the same list.
                    if (!(model.DeviceModelListRow1.Any(x => x.Value == model.DeviceModelSelectIdRow2.ToString())))
                    {
                        ModelState.AddModelError("DeviceModelListRow2", "Model Number is invalid.");
                    }

                    // Validate the device serial number.
                    // We've already checked and handled the dsn that has moved to deletion.
                    // If dsn row 2 is empty then don't validate any other fields.
                    if ((string.IsNullOrEmpty(model.DeviceSerialNumberRow2)) || (model.DeviceSerialNumberRow2.Length <= 0))
                    {
                        row2Validate = false;
                    }
                    else
                    {
                        string pattern = @"^[a-zA-Z]{3}[0-9]{5}[a-zA-Z]{1}";
                        Regex serialNumberRegEx = new Regex(pattern, RegexOptions.IgnoreCase);
                        Match m = serialNumberRegEx.Match(model.DeviceSerialNumberRow2);
                        if (!m.Success)
                        {
                            ModelState.AddModelError("deviceSerialNumberRow2", "Serial Number is invalid.");
                            row2Validate = false;
                        }
                        else
                        {
                            // Verify the serial number prefix is valid for the device type.
                            // Use the device model list from row 1 because it gets populated on the post back.
                            string devicePrefixString = DeviceRepository.GetModelSerialNumberPrefix(model.DeviceModelListRow1.Find(x => x.Value == model.DeviceModelSelectIdRow2.ToString()).Text);
                            if (!devicePrefixString.Equals(model.DeviceSerialNumberRow2.Substring(0, 3)))
                            {
                                ModelState.AddModelError("deviceSerialNumberRow2", string.Format("Prefix invalid (Use {0}...)", devicePrefixString));
                                row2Validate = false;
                            }
                            // Do a final check to see if the Device Serial Number has already been used (is available).
                            else
                            {
                                if (((row2DeviceSerialNumberUpdateType == DeviceUpdateType.NewDevice) || (row2DeviceSerialNumberUpdateType == DeviceUpdateType.ReplaceDevice))
                                    && (!DeviceRepository.DeviceIsAvailable(model.DeviceSerialNumberRow2)))
                                {
                                    ModelState.AddModelError("deviceSerialNumberRow2", "Serial Number is not available.");
                                    row2Validate = false;
                                }
                            }
                        }
                    }

                    // Validate the row 2 date of implant.
                    if (row2Validate && ((model.DeviceImplantDateRow2 == null) || (model.DeviceImplantDateRow2.Length <= 0)))
                    {
                        ModelState.AddModelError("deviceImplantDateRow2", "Implant Date is invalid.");
                    }
                    else if (row2Validate)
                    {
                        string pattern = @"^(0[1-9]|[1-9]|[12][0-9]|3[01])-([Jj][Aa][Nn]|[Ff][Ee][Bb]|[Mm][Aa][Rr]|[Aa][Pp][Rr]|[Mm][Aa][Yy]|[Jj][Uu][Nn]|[Jj][Uu][Lj]|[Aa][Uu][Gg]|[Ss][Ee][Pp]|[Oo][Cc][Tt]|[Nn][Oo][Vv]|[Dd][Ee][Cc])-(19|20)\d\d$";
                        Regex dateRegEx = new Regex(pattern, RegexOptions.IgnoreCase);
                        Match m = dateRegEx.Match(model.DeviceImplantDateRow2);
                        if (!m.Success)
                        {
                            ModelState.AddModelError("deviceImplantDateRow2", "Implant Date is invalid.");
                        }

                        // We've passed the string-test, now verify we can convert to DateTime and valid date range.
                        DateTime dateOfImplant = DateTime.MinValue;
                        if (!DateTime.TryParse(model.DeviceImplantDateRow2, out dateOfImplant))
                        {
                            ModelState.AddModelError("deviceImplantDateRow2", "Implant Date is invalid.");
                        }
                        else if ((dateOfImplant.Year == 1) || (dateOfImplant.Year < 1900))
                        {
                            ModelState.AddModelError("deviceImplantDateRow2", "Implant Date is invalid.");
                        }
                    }

                    // Validate the row 2 implant location.
                    // For the device implant location verify the list index is valid given the list provided to user.
                    // This is not really possible to hit unless the site is hit outside of normal page.
                    // We only ge the row1 implant location back since both lists are the same.
                    if (row2Validate && !(model.DeviceImplantLocationListRow1.Any(x => x.Value == model.DeviceImplantLocationSelectIdRow2.ToString())))
                    {
                        ModelState.AddModelError("deviceImplantLocationListRow2", "Implant Location is invalid.");
                    }
                }
            }

            // Whichever way we leave, we need to re-populate the drop-down lists.
            int tempDeviceModelSelectId = 0;
            model.DeviceImplantLocationListRow1 = GetLocationListForDevice("");
            model.DeviceModelListRow1 = GetDeviceModelListForDevice(Guid.Empty, out tempDeviceModelSelectId);

            model.DeviceImplantLocationListRow2 = GetLocationListForDevice("");
            model.DeviceModelListRow2 = GetDeviceModelListForDevice(Guid.Empty, out tempDeviceModelSelectId);

            if (ModelState.IsValid)
            {
                if (model.IsPatientInfoTab.ToLower().Equals("true"))
                {
                    PatientRepository.UpdatePatientDeviceAssociation(model.PatientId, model.NameFirst, model.NameLast, model.NameMiddle,
                                                                     Convert.ToDateTime(model.DateOfBirth), model.Identifier, model.PhoneHome,
                                                                     model.Gender.ToLower().Equals("male") ? "M" : "F");
                    
                    Transaction.Commit();
                }
                else
                {
                    // Would not have gotten this far if patient did not exist.
                    // Meaning get the patient entity.

                    // TODO: If in a real production system, there would be device serial number checking of:
                    // a.) Does the serial number prefix match the device model number.
                    // b.) Is the serial number already implant in a different/existing patient?
                    //     In other words, do not let a clinician accidentally "steal" a device from another patient.
                    // c.) If possible link the devices that have been shipped to clinic to available devices they could implant.
                    // A previous check is performed up in validatio to determine if 

                    // Row1: Determine what to save to database and save it to database.
                    UpdatePatientEquipmentRowInDb(model, row1DeviceSerialNumberUpdateType, model.DeviceModelSelectIdRow1, model.DeviceSerialNumberRow1, model.DeviceSerialNumberRow1Hidden, model.DeviceImplantLocationSelectIdRow1, model.DeviceImplantDateRow1);

                    // Row2: Determine what to save to database and save it to database.
                    UpdatePatientEquipmentRowInDb(model, row2DeviceSerialNumberUpdateType, model.DeviceModelSelectIdRow2, model.DeviceSerialNumberRow2, model.DeviceSerialNumberRow2Hidden, model.DeviceImplantLocationSelectIdRow2, model.DeviceImplantDateRow2);
                }

                return RedirectToAction("Index", new { patientIdString = model.PatientId });
            }
            else
            {
                // The model is returned here which may have invalid values entered and error statuses.
                return View(model);
            }
        }

        private void UpdatePatientEquipmentRowInDb(PatientModel model, DeviceUpdateType deviceSerialNumberUpdateType, int deviceModelSelectId, string serialNumber, string serialNumberHidden, int implantLocationSelectId, string implantDate)
        {
            ClaimsPrincipal currentUser = this.User;
            string currentUserName = currentUser.FindFirst(ClaimTypes.Name).Value;

            (Guid ClinicId, Guid ClinicStaffId, string ClinicName, string ClinicStaffFirstName, string ClinicStaffLastName) clinicInfo = ClinicStaffHelper.GetUsersClinicIdFromRequest(currentUserName, Services.GetRequiredService<MainContext>());

            // Scenarios to handle.
            // --------------------
            // Scenario #1:
            // Row is new device:
            // 1.) Create the device.
            // 2.) Add the device to the repository.
            // 3.) Associate the device to the patient.
            // TODO: device should already exist in database and you're just updating it's status but this is demo.
            if (deviceSerialNumberUpdateType == DeviceUpdateType.NewDevice)
            {
                Device device = new Device();
                device.DateAddedUTC = DateTime.UtcNow;
                device.DeviceTypeId = Convert.ToInt32(model.DeviceModelListRow1.Find(x => x.Value == deviceModelSelectId.ToString()).Value);
                device.IsAvailableForUse = true;
                device.ManufactureDate = DateTime.Now;
                device.SerialNumber = serialNumber;
                device.Id = Guid.NewGuid();
                device.HardwareVersion = 1.0m;
                device.CalCoeffA = 1.0m;
                device.CalCoeffB = 1.0m;
                device.CalCoeffC = 1.0m;

                DeviceRepository.Add(device);
                Transaction.Commit();

                string implantEye = model.DeviceImplantLocationListRow1.Find(x => x.Value == implantLocationSelectId.ToString()).Text.ToUpper().ElementAt(1).ToString();
                DateTime implantDateToSave = DateTime.Parse(implantDate);
                DeviceStatusType deviceStatusType = new DeviceStatusType();

                // The DeviceStatusType is set to "2" for implanted.
                PatientRepository.AssociateDeviceToPatient(model.PatientId, device.SerialNumber, implantEye, implantDateToSave, 2, clinicInfo.ClinicStaffId);
                Transaction.Commit();
            }
            // Scenario #2:
            // Row is updated device (serial number stays the same, other fields changed):
            // 1.) Find the serial number entry in the patient device association table.
            // 2.) Update the patient device association fields.
            // 3.) Save the patient device association entity.
            if (deviceSerialNumberUpdateType == DeviceUpdateType.NoChange)
            {
                // if there's no change and both entered and hidden have a value then update an entry.
                if (!string.IsNullOrEmpty(serialNumber))
                {
                    string implantEye = model.DeviceImplantLocationListRow1.Find(x => x.Value == implantLocationSelectId.ToString()).Text.ToUpper().ElementAt(0).ToString();
                    // The DeviceStatusType is set to "2" for implanted.
                    PatientRepository.UpdatePatientDeviceAssociation(model.PatientId, serialNumber, implantEye, DateTime.Parse(implantDate), 2, clinicInfo.ClinicStaffId);

                    Transaction.Commit();
                }
            }

            // Scenario #3:
            // Row is replaced device (serial number has changed, other fields may or may not have changed):
            // Row went from device serial number (A) to device serial number (B).
            // Need to delete device (A) and need to create device (B)
            if (deviceSerialNumberUpdateType == DeviceUpdateType.ReplaceDevice)
            {
                // Device has changed.
                // The original device for the row (hidden) is not empty, then we need to make that device unavailable (take it out of service).
                // status of 3 = Explanted.
                PatientRepository.RemoveDevice(model.PatientId, serialNumberHidden, 3, clinicInfo.ClinicStaffId);

                // We need to add the new device the user entered.
                Device device = new Device();
                device.DateAddedUTC = DateTime.UtcNow;
                device.DeviceTypeId = Convert.ToInt32(model.DeviceModelListRow1.Find(x => x.Value == deviceModelSelectId.ToString()).Value);
                device.IsAvailableForUse = true;
                device.ManufactureDate = DateTime.Now;
                device.SerialNumber = serialNumber;
                device.Id = Guid.NewGuid();
                device.HardwareVersion = 1.0m;
                device.CalCoeffA = 1.0m;
                device.CalCoeffB = 1.0m;
                device.CalCoeffC = 1.0m;

                DeviceRepository.Add(device);
                Transaction.Commit();

                string implantEye = model.DeviceImplantLocationListRow1.Find(x => x.Value == implantLocationSelectId.ToString()).Text.ToUpper().ElementAt(0).ToString();
                DateTime implantDateToSave = DateTime.Parse(implantDate);
                DeviceStatusType deviceStatusType = new DeviceStatusType();

                // The DeviceStatusType is set to "2" for implanted.
                PatientRepository.AssociateDeviceToPatient(model.PatientId, device.SerialNumber, implantEye, implantDateToSave, 2, clinicInfo.ClinicStaffId);

                // TODO: This update should only occur when a new transmission comes in and the most recent transmission left and right eye sn's get logged.
                // But because this is a demo, and once the user changes the serial numbers, they'll no longer show in the Manage My Patients list.
                // So we write the new device serial number in so it shows up.
                ClinicRepository.UpdateMostRecentTransmissionSerialNumberInformation(clinicInfo.ClinicId, model.PatientId, device.SerialNumber, implantEye);
                Transaction.Commit();
            }

            // Scenario #4:
            // For deleted device (a), serial number changed to empty string do the following:
            // 1.) Find existing patient device association.
            // 2.) Delete the existing device from the patient device association.
            if (deviceSerialNumberUpdateType == DeviceUpdateType.DeleteDevice)
            {
                // Indicate the device has been explaned and no longer available.
                // status of 3 = Explanted.
                PatientRepository.RemoveDevice(model.PatientId, serialNumberHidden, 3, clinicInfo.ClinicStaffId);

                Transaction.Commit();
            }
        }

        private DeviceUpdateType GetDeviceUpdateStatus(string originalDeviceSerialNumber, string newDeviceSerialNumber)
        {
            DeviceUpdateType updateType = DeviceUpdateType.NoChange;

            // If both old and new are null or empty then no change.
            if ((string.IsNullOrEmpty(originalDeviceSerialNumber)) && (string.IsNullOrEmpty(newDeviceSerialNumber)))
            {
                updateType = DeviceUpdateType.NoChange;
            }
            // if original was not empty and new is dempty then delete device.
            else if ((!string.IsNullOrEmpty(originalDeviceSerialNumber)) && (string.IsNullOrEmpty(newDeviceSerialNumber)))
            {
                updateType = DeviceUpdateType.DeleteDevice;
            }
            // if original was empty and new is not empty then we have a new device being added.
            else if ((string.IsNullOrEmpty(originalDeviceSerialNumber)) && (!string.IsNullOrEmpty(newDeviceSerialNumber)))
            {
                updateType = DeviceUpdateType.NewDevice;
            }
            // if original and new device serial number are the same then no change.
            else if (originalDeviceSerialNumber.Equals(newDeviceSerialNumber))
            {
                updateType = DeviceUpdateType.NoChange;
            }
            // the original and new device serial numbers are not empty and are different so replace the device.
            else
            {
                updateType = DeviceUpdateType.ReplaceDevice;
            }

            return (updateType);
        }

        private string ConvertGenderToString(string gender)
        {
            switch(gender.ToUpper())
            {
                case "M":
                    return ("Male");
                case "F":
                    return ("Female");
                case "O":
                    return ("Other");
                default:
                    throw new Exception(string.Format("An invalid Gender value was received: {0}", gender));
            }
        }

        private PatientModel PopulatePatientModel(string patientIdString)
        {
            MainContext mainContext = Services.GetRequiredService<MainContext>();

            ClaimsPrincipal currentUser = this.User;
            string currentUserName = currentUser.FindFirst(ClaimTypes.Name).Value;

            // Convert the incoming string parameters to a Guids.
            Guid patientId = ConvertStringToGuid(patientIdString, "patientIdString");

            (Guid ClinicId, Guid ClinicStaffId, string ClinicName, string ClinicStaffFirstName, string ClinicStaffLastName) clinicInfo = ClinicStaffHelper.GetUsersClinicIdFromRequest(currentUserName, mainContext);

            Patient patientInfo = PatientRepository.GetPatient(patientId);

            List<(Guid DeviceId, string ImplantEye, DateTime? ImplantDate)> deviceList = PatientRepository.GetImplantedDeviceInfo(patientId);

            // Get the TimeZone info for the clinic.
            string localTimeZoneInfo = ClinicRepository.GetClinicDotNetTimeZoneInfo(clinicInfo.ClinicId);
            TimeZoneInfo timeZoneInfo = TZConvert.GetTimeZoneInfo(localTimeZoneInfo);

            DateTime currentDate = DateTime.UtcNow;
            DateTime.SpecifyKind(currentDate, DateTimeKind.Utc);

            // A patient id has been entered - so fill the model and present the populated form.
            PatientModel model = new PatientModel();
            model.Initialize(currentUserName, mainContext);

            // Patient Information
            model.PatientId = patientId;
            model.NamePrefix = patientInfo.NamePrefix;
            model.NameFirst = patientInfo.NameFirst;
            model.NameMiddle = patientInfo.NameMiddle;
            model.NameLast = patientInfo.NameLast;
            model.NameSuffix = patientInfo.NameSuffix;
            model.NameCombined = patientInfo.NameLast + ", " + patientInfo.NameFirst;
            model.HasReader = patientInfo.HasReader;
            model.Gender = ConvertGenderToString(patientInfo.Gender);
            model.Identifier = patientInfo.Identifier;
            model.ApplicationUsername = patientInfo.ApplicationUsername;
            model.DateOfBirth = patientInfo.DateOfBirthNotUTC.ToString("dd-MMM-yyyy");
            model.AddressLine1 = patientInfo.AddressLine1;
            model.AddressLine2 = patientInfo.AddressLine2;
            model.AddressLine3 = patientInfo.AddressLine3;
            model.AddressLine4 = patientInfo.AddressLine4;
            model.City = patientInfo.City;
            model.StateCode = patientInfo.StateCode;
            model.Zipcode = patientInfo.Zipcode;
            model.PostOfficeNumber = patientInfo.PostOfficeNumber;
            model.CountryCodeId = patientInfo.CountryCodeId;
            model.PhoneHome = patientInfo.PhoneHome;
            model.PhoneMobile = patientInfo.PhoneMobile;
            model.PhoneWorkNumber = patientInfo.PhoneWorkNumber;
            model.PhoneWorkExtension = patientInfo.PhoneWorkExtension;
            model.PhoneOtherNumber = patientInfo.PhoneOtherNumber;
            model.PhoneOtherDescription = patientInfo.PhoneOtherDescription;
            model.PrimaryEmail = patientInfo.PrimaryEmail;
            model.SecondaryEmail = patientInfo.SecondaryEmail;

            // Given the list of implanted devices, display the information with left eye first on list for consistancy.

            // I had these rows in a list of selectable items but because the deviceInfo object was complex,
            // the post-back model binding lost the complete DeviceInfo list - so all information lost.
            // Rather than create a complex custom model binder, just broke this up into two rows and seperate records for each item.

            // The items in the device list are pre-sotred such that the left-eye-location item will always be listed first if it exists.

            // If device list is empty then both rows have empty fields.
            int tempDeviceModelSelectId = -1;
            if (deviceList.Count == 0)
            {
                // No devices, create an empty row #1
                model.DeviceSerialNumberRow1 = "";
                model.DeviceSerialNumberRow1Hidden = "";
                model.DeviceImplantDateRow1 = "";

                model.DeviceImplantLocationListRow1 = GetLocationListForDevice("");
                model.DeviceImplantLocationSelectIdRow1 = -1;

                model.DeviceModelListRow1 = GetDeviceModelListForDevice(Guid.Empty, out tempDeviceModelSelectId);
                model.DeviceModelSelectIdRow1 = -1;

                // No devices, create an empty row #2
                model.DeviceSerialNumberRow2 = "";
                model.DeviceSerialNumberRow2Hidden = "";
                model.DeviceImplantDateRow2 = "";

                model.DeviceImplantLocationListRow2 = GetLocationListForDevice("");
                model.DeviceImplantLocationSelectIdRow2 = -1;

                model.DeviceModelListRow2 = GetDeviceModelListForDevice(Guid.Empty, out tempDeviceModelSelectId);
                model.DeviceModelSelectIdRow2 = -1;
            }
            else if (deviceList.Count == 1)
            {
                Guid deviceId0 = deviceList[0].DeviceId;
                Injectsense.DataAccess.Entities.Device device = DeviceRepository.Find(x => x.Id == deviceId0).Single();

                // 1 row gets the device info, second row is empty.
                model.DeviceSerialNumberRow1 = device.SerialNumber;
                model.DeviceSerialNumberRow1Hidden = device.SerialNumber;
                model.DeviceImplantDateRow1 = deviceList[0].ImplantDate.HasValue ? deviceList[0].ImplantDate.Value.ToString("dd-MMM-yyyy") : "";

                model.DeviceImplantLocationListRow1 = GetLocationListForDevice("");
                model.DeviceImplantLocationSelectIdRow1 = (deviceList[0].ImplantEye.Equals("l", StringComparison.CurrentCultureIgnoreCase)) ? 1 : 2;

                model.DeviceModelListRow1 = GetDeviceModelListForDevice(deviceList[0].DeviceId, out tempDeviceModelSelectId);
                model.DeviceModelSelectIdRow1 = tempDeviceModelSelectId;

                // No devices, create an empty row #2
                model.DeviceSerialNumberRow2 = "";
                model.DeviceSerialNumberRow2Hidden = "";
                model.DeviceImplantDateRow2 = "";

                model.DeviceImplantLocationListRow2 = GetLocationListForDevice("");
                model.DeviceImplantLocationSelectIdRow2 = 0;

                model.DeviceModelListRow2 = GetDeviceModelListForDevice(Guid.Empty, out tempDeviceModelSelectId);
                model.DeviceModelSelectIdRow2 = -1;
            }
            else if (deviceList.Count == 2)
            {
                Guid deviceId0 = deviceList[0].DeviceId;
                Injectsense.DataAccess.Entities.Device device0 = DeviceRepository.Find(x => x.Id == deviceId0).Single();
                Guid deviceId1 = deviceList[1].DeviceId;
                Injectsense.DataAccess.Entities.Device device1 = DeviceRepository.Find(x => x.Id == deviceId1).Single();

                // 1 row gets the first device info, row 2 gets the second.
                model.DeviceSerialNumberRow1 = device0.SerialNumber;
                model.DeviceSerialNumberRow1Hidden = device0.SerialNumber;
                model.DeviceImplantDateRow1 = deviceList[0].ImplantDate.HasValue ? deviceList[0].ImplantDate.Value.ToString("dd-MMM-yyyy") : "";

                model.DeviceImplantLocationListRow1 = GetLocationListForDevice("");
                model.DeviceImplantLocationSelectIdRow1 = (deviceList[0].ImplantEye.Equals("l", StringComparison.CurrentCultureIgnoreCase)) ? 1 : 2;

                model.DeviceModelListRow1 = GetDeviceModelListForDevice(deviceList[0].DeviceId, out tempDeviceModelSelectId);
                model.DeviceModelSelectIdRow1 = tempDeviceModelSelectId;

                // No devices, create an empty row #2
                model.DeviceSerialNumberRow2 = device1.SerialNumber;
                model.DeviceSerialNumberRow2Hidden = device1.SerialNumber;
                model.DeviceImplantDateRow2 = deviceList[1].ImplantDate.HasValue ? deviceList[1].ImplantDate.Value.ToString("dd-MMM-yyyy") : "";

                model.DeviceImplantLocationListRow2 = GetLocationListForDevice("");
                model.DeviceImplantLocationSelectIdRow2 = (deviceList[1].ImplantEye.Equals("l", StringComparison.CurrentCultureIgnoreCase)) ? 1 : 2;

                model.DeviceModelListRow2 = GetDeviceModelListForDevice(deviceList[1].DeviceId, out tempDeviceModelSelectId);
                model.DeviceModelSelectIdRow2 = tempDeviceModelSelectId;
            }

            return (model);
        }

        private List<SelectListItem> GetDeviceModelListForDevice(Guid deviceId, out int tempDeviceModelSelectId)
        {
            tempDeviceModelSelectId = -1;

            // Get the list of sensor models.
            List<(int Id, string Name)> modelList = DeviceRepository.GetAvailableDeviceModelList();
            List<SelectListItem> selectableListOfModels = modelList.Select(x => new SelectListItem { Value = x.Id.ToString(), Text = x.Name }).ToList();

            // If the device id entered is not Guid-empty then find the device model in the list and set it "selected".
            if (!Guid.Empty.Equals(deviceId))
            {
                // Create a list of device models that DO have one selected.

                // Find the type-id of the current device to flag it as selected in list of available models.
                string deviceModelName = PatientRepository.GetDeviceModelName(deviceId);

                // Given the selectable list, find the current implanted device model and make it "selected".
                foreach (SelectListItem item in selectableListOfModels)
                {
                    if (item.Text.ToLower().Equals(deviceModelName.ToLower()))
                    {
                        item.Selected = true;
                        tempDeviceModelSelectId = Convert.ToInt32(item.Value);
                    }
                }
            }

            return (selectableListOfModels);
        }

        private List<SelectListItem> GetLocationListForDevice(string implantEye)
        {
            List<SelectListItem> implantLocationList = new List<SelectListItem>()
            {
                new SelectListItem() { Text = "Left", Value = "1" },
                new SelectListItem() { Text = "Right", Value = "2" }
            };

            foreach(SelectListItem sli in implantLocationList)
            {
                if (sli.Text.ToLower().StartsWith(implantEye.ToLower()))
                {
                    sli.Selected = true;
                }
            }

            return (implantLocationList);
        }

        private Guid ConvertStringToGuid(string guidString, string source)
        {
            Guid newGuid = Guid.Empty;
            if (!Guid.TryParse(guidString, out newGuid))
            {
                throw new Exception(string.Format("PatientController - Index has received an invalid {0} GUID: {1}", source, guidString));
            }

            return (newGuid);
        }

        public ActionResult LogOut()
        {
            // This action/method is a kludge because I couldn't get the "sign out" link to change form Home/Logout to Account/Logout.
            // So this method is located here to call the LogOut action on the AccountController.

            // TODO: Remove after successful port to ASP.NET core.
            //var controller = DependencyResolver.Current.GetService<AccountController>();
            //controller.ControllerContext = new ControllerContext(this.Request.RequestContext, controller);
            //ActionInvoker.InvokeAction(controller.ControllerContext, "LogOut");

            return RedirectToRoute(new
            {
                controller = "Account",
                action = "Logout",
            });
        }
    }
}
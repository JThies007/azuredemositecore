﻿// ************************************************************
// Copyright (C) 2018 Injectsense, Inc. - All Rights Reserved
// ************************************************************
using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using TimeZoneConverter;
using Microsoft.AspNetCore.Authorization;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.AspNetCore.Identity;
using System.Security.Claims;
using Injectsense.Models;
using Injectsense.DataAccess.Repositories;
using Injectsense.DataAccess.Entities;
using Injectsense.DataAccess;
using Injectsense.Helpers;

namespace Injectsense.Controllers
{
    [Authorize]
    [OutputCache(Duration = 0)]
    public class HomeController : Controller
    {
        private IUnitOfWork Transaction { get; }

        private SignInManager<User> SignInManager { get; }

        private IServiceProvider Services { get; }

        public HomeController(IUnitOfWork unitOfWork,
                              SignInManager<User> signInManager,
                              IServiceProvider services)
        {
            Transaction = unitOfWork;

            SignInManager = signInManager;
            Services = services;
        }

        public ActionResult Index()
        {
            MainContext mainContext = Services.GetRequiredService<MainContext>();

            // We could put the clinic id in the html and get it back on this call.
            // But the goal is to leave as little info as possible in html.
            // So let's get user and map to their clinic, and from clinic, we'll get list of patients.
            ClaimsPrincipal currentUser = this.User;
            string currentUserName = currentUser.FindFirst(ClaimTypes.Name).Value;

            (Guid ClinicId, Guid ClinicStaffId, string ClinicName, string ClinicStaffFirstName, string ClinicStaffLastName) clinicInfo = ClinicStaffHelper.GetUsersClinicIdFromRequest(currentUserName, mainContext);

            // Get the TimeZone info for the clinic.
            IClinicRepository clinicRepository = Services.GetRequiredService<IClinicRepository>();
            string localTimeZoneInfo = clinicRepository.GetClinicDotNetTimeZoneInfo(clinicInfo.ClinicId);
            TimeZoneInfo timeZoneInfo = TZConvert.GetTimeZoneInfo(localTimeZoneInfo);

            // Get all patients for clinic that have new transmissions for home page list.
            // List returned is orded by Most Recent transmission received (newest == first in list).
            List<ClinicPatientAssociation> ClinicPatientInfoList = clinicRepository.GetAllPatientsWithNewTransmissionsForClinic(clinicInfo.ClinicId);

            //// Now populate the view model that will be passed to view.
            LatestPatientActivityModel model = new LatestPatientActivityModel();
            model.Initialize(currentUserName, mainContext);

            // A temp list to add to then sort before returning.
            List<LatestPatientActivity> latestActivityList = new List<LatestPatientActivity>();

            // Get the clinic profile image displayed at the top of the home page.
            model.ClinicProfileImage = GetClinicProfileImage(clinicInfo.ClinicId);

            IPatientRepository patientRepository = Services.GetRequiredService<IPatientRepository>();

            foreach (ClinicPatientAssociation cpa in ClinicPatientInfoList)
            {
                Patient patient = patientRepository.GetPatient(cpa.PatientId);

                // NOTE: Have to be careful here...
                // What the patient has implanted and enabled at this moment in time may NOT BE EQUAL
                // to what the patient had implanted and enabled at time of transmission.
                // Meaning do not pull device information from latest patient info.
                // -----------------------------------------------------------------------------------------------------------------
                // Pull device information from clinic patient association where DSN's are lined up with most recent transmission.
                // -----------------------------------------------------------------------------------------------------------------

                // This may not be used but need it to be initialized.
                DateTime receivedDateTime = DateTime.MinValue;

                string receivedDateString;
                if (cpa.MostRecentTransmissionReceivedDateUTC.HasValue)
                {
                    receivedDateTime = TimeZoneInfo.ConvertTimeFromUtc(cpa.MostRecentTransmissionReceivedDateUTC.Value, timeZoneInfo);
                    receivedDateString = receivedDateTime.ToString("dd-MMM-yyyy h:mm tt");
                }
                else
                {
                    receivedDateString = "";
                }

                // Take into account Last Office Visit may be null.
                DateTime? lastOfficeVisit = clinicRepository.GetLastOfficeVisitDate(clinicInfo.ClinicId, patient.Id);
                string lastOfficeVisitString;
                if (lastOfficeVisit.HasValue)
                {
                    lastOfficeVisitString = lastOfficeVisit.Value.ToString("dd-MMM-yyyy");
                }
                else
                {
                    lastOfficeVisitString = "";
                }

                latestActivityList.Add(new LatestPatientActivity()
                {
                    PatientId = cpa.PatientId,
                    PatientName = patient.NameLast + ", " + patient.NameFirst,
                    TransmissionId = cpa.MostRecentTransmissionId,
                    ReceivedDateString = receivedDateString,
                    ReceivedDate = receivedDateTime,
                    IdNumber = patient.Identifier,
                    LeftEyeSerialNumber = cpa.MostRecentTransmissionLeftEyeDSN,
                    RightEyeSerialNumber = cpa.MostRecentTransmissionRightEyeDSN,
                    LastOfficeVisitString = lastOfficeVisitString
                });

                model.PatientActivityList = latestActivityList.OrderByDescending(x => x.ReceivedDate).ToList();
            }

            return View(model);
        }

        private string GetClinicProfileImage(Guid clinicId)
        {
            string imageReference = "";

            // TODO: This is needs to move to image-names saved in database and images on a central location in file system.
            switch (clinicId.ToString().ToUpper())
            {
                case "169381CF-4B6D-4FA9-911A-01C0EC31E78D":
                    // Load the mountain pic.
                    imageReference = @"~/images/ClinicImages/mountainPic.png";
                    break;
                case "FBE0CF6A-EEA1-49C7-80F0-3E005B988EA2":
                    imageReference = @"~/images/ClinicImages/eyePic.png";
                    break;
                default:
                    // The generic clinic image has been removed.
                    // imageReference = @"~/images/ClinicImages/genericClinicLogo.png";
                    break;
            }

            return (imageReference);
        }

        public ActionResult LogOut()
        {
            // This action/method is a kludge because I couldn't get the "sign out" link to change form Home/Logout to Account/Logout.
            // So this method is located here to call the LogOut action on the AccountController.

            return RedirectToRoute(new
            {
                controller = "Account",
                action = "Logout",
            });
        }
    }
}


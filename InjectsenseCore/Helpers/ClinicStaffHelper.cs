﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Injectsense.DataAccess;
using Injectsense.DataAccess.Repositories;
using Injectsense.DataAccess.Entities;
using Microsoft.AspNetCore.Http;
using System.Security.Claims;

namespace Injectsense.Helpers
{
    public static class ClinicStaffHelper
    {
        public static (Guid ClinicId, Guid ClinicStaffId, string ClinicName, string ClinicStaffFirstName, string ClinicStaffLastName) GetUsersClinicIdFromRequest(string userName, MainContext mainContext)
        {
            Guid clinicId = Guid.Empty;
            Guid clinicStaffId = Guid.Empty;
            (Guid ClinicId, Guid ClinicStaffId, string ClinicName, string ClinicStaffFirstName, string ClinicStaffLastName) returnInfo;

            if (!string.IsNullOrEmpty(userName))
            {
                // TODO: There are the ClinicStaff roles and the UserRoles - these need to be cleaned up and determine where source of truth is.
                // for now everyone is an admin.

                string[] roles = null;
                string[] roleArray = new string[1] { "Admin" };  // satisfy what is being passed on as role info.

                IClinicStaffRepository clinicStaffRepository = new ClinicStaffRepository(mainContext);
                if (clinicStaffRepository.IsValidUser(userName, out roles, out clinicId, out clinicStaffId))
                {
                    ClinicRepository clinicRepository = new ClinicRepository(mainContext);
                    Clinic clinic = clinicRepository.Find(x => x.Id == clinicId).Single();

                    ClinicStaff clinicStaff = clinicStaffRepository.Find(x => x.Id == clinicStaffId).Single();

                    returnInfo.ClinicId = clinicId;
                    returnInfo.ClinicStaffId = clinicStaffId;
                    returnInfo.ClinicName = clinic.NamePrimary;
                    returnInfo.ClinicStaffFirstName = clinicStaff.NameFirst;
                    returnInfo.ClinicStaffLastName = clinicStaff.NameLast;

                    return (returnInfo);
                }
                else
                {
                    throw new Exception(String.Format("In GetUsersClinicIdFromRequest() and unable to map username to clinic. Username: {0}", userName));
                }
            }
            else
            {
                throw new Exception("User Name has not been supplied.");
            }
        }
    }
}